<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
  //  return redirect(route('home'));
//    return view('empty');
//});
Route::get('/', function () {
    return redirect(route('users.index'));
//    return view('empty');
});

Route::get('/admin', function () {
    return redirect(route('users.index'));
});
Route::get('storage/{filename}', function ($filename)
{
    return Image::make(storage_path('public/' . $filename))->response();
});
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/products', 'HomeController@productsHome')->name('product');
Route::get('/faqs', 'FaqsFrontController@indexHome')->name('faqs');
Route::get('/products', 'HomeController@productsHome')->name('product');
Route::get('/how-to-order', 'HomeController@orderHome')->name('how-to-order');
Route::get('/get-in-touch', 'HomeController@contactHome')->name('get-in-touch');
Route::get('/our-team', 'TeamsFrontController@indexHome')->name('our-team');
Route::get('/design-lab', 'ConfiguratorController@index')->name('configurator');
Route::get('/step1-addColor', 'step1Controller@index')->name('step1-addColor');
Route::post('/step1-addColor/uploade', 'step1Controller@uploade')->name('step1-addColor.uploade');
Route::post('/step1-addColor/download-pdf','step1Controller@downloadPDF')->name('step1-addColor.download-pdf');
/*Route::group(['prefix'=>'configurator'],function (){
    Route::resource('step1addColor', 'step1Controller');
});*/
Route::group(['prefix'=>'admin'],function (){
    Route::resource('colors', 'ColorController');
    Route::resource('clothes', 'ClothesController');
    Route::post('/clothes/uploade', 'ClothesController@uploade')->name('clothes.uploade');
    Route::resource('users', 'UsersController');
    Route::resource('prints', 'PrintsController');
    Route::resource('product_cat','CategoryController');
    Route::resource('designs','DesignController');
	Route::post('/designs/addLayers', 'DesignController@addLayers')->name('designs.addLayers');
    Route::resource('clip_arts','ClipArtController');
    Route::resource('fonts','FontController');
    Route::resource('embs','EmbellishmentController');
    Route::resource('sizes','SizeController');
    Route::post('/uploade','PrintsController@uploade')->name('uploade');
    Route::resource('homepage_banners', 'HomepageBannersController');
	Route::resource('text', 'TextController');
	Route::resource('teams', 'TeamsController');
	Route::resource('faqs', 'FaqsController');
	Route::resource('editable_logos', 'EditableLogosController');
	Route::post('/editable_logos/uploade', 'EditableLogosController@uploade')->name('editable_logos.uploade');
	Route::resource('symbols', 'SymbolsController');
	Route::post('/symbols/uploade', 'SymbolsController@uploade')->name('symbols.uploade');
	Route::resource('alphabets', 'AlphabetsController');
	Route::post('/alphabets/uploade', 'AlphabetsController@uploade')->name('alphabets.uploade');
	Route::resource('collars', 'CollarsController');
	Route::post('/collars/uploade', 'CollarsController@uploade')->name('collars.uploade');
	Route::resource('coming_soon', 'ComingSoonController');
	// new design routes
    Route::get('category', 'NewCategoryController@index')->name('category.index');
    Route::post('category-save','NewCategoryController@create')->name('category.save');

    Route::get('company', 'CompanyController@index')->name('company.index');
    Route::get('customer', 'CustomerFileController@index')->name('customer.index');

    Route::get('data-source', 'DataSourceController@index')->name('dataSource.index');
    Route::post('data-source-save', 'DataSourceController@store')->name('dataSource.store');
    Route::get('data-source-edit/{id}', 'DataSourceController@edit')->name('dataSource.edit');
    Route::post('data-source-update/{id}', 'DataSourceController@update')->name('dataSource.update');
    //datasource colours routes
    Route::post('data-source-color', 'DataSourceController@storeColour')->name('DataSource.colours');

    Route::get('distributor', 'DistributorController@index')->name('distributor.index');
    Route::get('e-commerce', 'ECommerceController@index')->name('ecommerce.index');

    Route::get('file', 'FileController@index')->name('file.index');
    Route::post('file-save', 'FileController@create')->name('file.save');

    Route::get('form', 'FormController@index')->name('form.index');
    Route::get('language', 'LanguageController@index')->name('language.index');

    Route::get('product', 'ProductController@index')->name('product.index');
    Route::post('product-store', 'ProductController@store')->name('product.store');
    Route::get('product-edit/{id}','ProductController@edit')->name('product.edit');
    Route::post('product-edit/{id}','ProductController@update')->name('product.update');
    Route::post('product-cat-store','ProductController@cat_store')->name('product_cat.store');
    Route::get('product-cat-store/{id}','ProductController@cat_edit')->name('product_cat.edit');
    Route::post('product-cat-update/{id}','ProductController@cat_update')->name('product_cat_edit.store');
    Route::post('related-product','ProductController@product_related')->name('product_related.store');

    //product step
    Route::post('store-product-step', 'ProductStepController@store')->name('product.step.create');
    Route::get('edit-product-step/{id}', 'ProductStepController@edit')->name('product.step.edit');
    Route::post('update-product-step/{id}', 'ProductStepController@update')->name('product.step.update');

    Route::get('show-product-field/{id}','ProductStepController@showProductField')->name('product.show.step.field');
    Route::post('add-product-field','ProductStepController@storeProductField')->name('product.store.step.field');
    Route::get('edit-product-step-field/{id}','ProductStepController@editProductField')->name('product.edit.step.field');
    Route::post('update-product-step-field/{id}', 'ProductStepController@updateProductField')->name('product.update.step.field');

    Route::get('setting', 'SettingController@index')->name('setting.index');
});