<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    //return redirect(route('home'));
  return view('empty');
});
Route::get('/admin', function () {
    return redirect(route('users.index'));
});

Auth::routes();

//Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin'],function (){

    Route::resource('colors', 'ColorController');

    Route::resource('clothes', 'ClothesController');

    Route::post('/clothes/uploade', 'ClothesController@uploade')->name('clothes.uploade');

    Route::resource('users', 'UsersController');

    Route::resource('prints', 'PrintsController');

    Route::resource('product_cat','CategoryController');
    Route::resource('designs','DesignController');
    Route::resource('clip_arts','ClipArtController');
    Route::resource('fonts','FontController');
    Route::resource('embs','EmbellishmentController');
    Route::resource('sizes','SizeController');
    Route::post('/uploade','PrintsController@uploade')->name('uploade');

});
