// JavaScript Document

var defaultBaseColor;
var defaultLayer1Color;
var defaultLayer2Color;
var defaultAccentColor;

function getDefaultColors() {
	var svgRect = document.getElementById("Layer_1");
	var svgBody = svgRect.getElementById("Body");
	defaultBaseColor = svgBody.getAttribute("fill");
	var svgLayer1 = svgRect.getElementById("Layer-01");
	defaultLayer1Color = svgLayer1.getAttribute("fill");
	var svgLayer2 = svgRect.getElementById("Layer-02");
	defaultLayer2Color = svgLayer2.getAttribute("fill");
    var svgAccents = svgRect.getElementById("Accents");
	defaultAccentColor = svgAccents.getAttribute("fill");
	console.log("getDefaultColors Successful...");
	
}

function changeBaseColor() {
	var baseColor = document.getElementById("baseColor").value;
	var svgRect = document.getElementById("Layer_1");
    var svgBody = svgRect.getElementById("Body");
	svgBody.setAttribute("fill",baseColor);
	console.log("changeBaseColor Successful...");
}

function changeLayer1Color() {
	var layer1Color = document.getElementById("layer1Color").value;
	var svgRect = document.getElementById("Layer_1");
    var svgLayer1 = svgRect.getElementById("Layer-01");
	svgLayer1.setAttribute("fill",layer1Color);
	console.log("changeLayer1Color Successful...");
}

function changeLayer2Color() {
	var layer2Color = document.getElementById("layer2Color").value;
	var svgRect = document.getElementById("Layer_1");
    var svgLayer2 = svgRect.getElementById("Layer-02");
	svgLayer2.setAttribute("fill",layer2Color);
	console.log("changeLayer2Color Successful...");
}

function changeLayer3Color() {
	var layer3Color = document.getElementById("layer3Color").value;
	var svgRect = document.getElementById("Layer_1");
    var svgAccents = svgRect.getElementById("Accents");
	svgAccents.setAttribute("fill",layer3Color);
	console.log("changeLayer3Color Successful...");
}

function colorReset() {
	var svgRect = document.getElementById("Layer_1");
	var svgBody = svgRect.getElementById("Body");
	svgBody.setAttribute("fill",defaultBaseColor);
	var svgLayer1 = svgRect.getElementById("Layer-01");
	svgLayer1.setAttribute("fill",defaultLayer1Color);
	var svgLayer2 = svgRect.getElementById("Layer-02");
	svgLayer2.setAttribute("fill",defaultLayer2Color);
    var svgAccents = svgRect.getElementById("Accents");
	svgAccents.setAttribute("fill",defaultAccentColor);
	console.log("colorReset Successful...");
}