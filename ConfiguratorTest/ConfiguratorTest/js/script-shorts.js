// JavaScript Document

var defaultBaseColor;
var defaultLayer1Color;
var defaultLayer2Color;
var defaultAccentColor;
var gradientLayer;

function getDefaultColors() {
	gradientLayer = document.getElementById('gradientLayer').value;
	var svgRect = document.getElementById("Layer_1");
	var svgBody = svgRect.getElementsByClassName("st0");
	//defaultBaseColor = svgBody.getAttribute("fill");
	//defaultBaseColor = svgBody[0].style.fill;
	for(var i=0; i<svgBody.length; i++)
	{
		console.log(svgBody[i].className);
	}
	/**/
	var svgBody1 = svgRect.getElementsByTagName("style");
	for(var i=0; i<svgBody1.length; i++)
	{
		console.log(svgBody1[i].innerHTML.getAttributes());
	}
	/*
	var svgLayer1 = svgRect.getElementById("Layer-01");
	defaultLayer1Color = svgLayer1.getAttribute("fill");
	var svgLayer2 = svgRect.getElementById("Layer-02");
	defaultLayer2Color = svgLayer2.getAttribute("fill");
    var svgAccents = svgRect.getElementById("Accents");
	defaultAccentColor = svgAccents.getAttribute("fill");*/
	console.log("getDefaultColors Successful...");
	//console.log(defaultBaseColor);
}

function changeBaseColor() {
	var baseColor = document.getElementById("baseColor").value;
	var svgRect = document.getElementById("Layer_1");
    var svgBody = svgRect.getElementsByClassName("st0");
	for(var i=0; i<svgBody.length; i++)
	{
		svgBody[i].style.fill = baseColor;
	}
	console.log("changeBaseColor Successful...");
}

function changeGradientColor() {
	var gradientLayerColor = document.getElementById("gradientLayerColor").value;
	var svgRect = document.getElementById("Layer_1");
	var svgGradientLayer = svgRect.getElementById("_1_1_");
	var stopTag = svgGradientLayer.getElementsByTagName("stop");
	stopTag[1].style = "stop-color:"+gradientLayerColor;
	console.log("changeGradientColor Successful...");
}

function changeLayer1Color() {
	var layer1Color = document.getElementById("layer1Color").value;
	var svgRect = document.getElementById("Layer_1");
	var svgLayer1;
	svgLayer1 = svgRect.getElementsByClassName("st1");
	svgLayer1[0].style.fill = layer1Color;
	console.log("changeLayer1Color Successful...");
}

function changeLayer2Color() {
	var layer2Color = document.getElementById("layer2Color").value;
	var svgRect = document.getElementById("Layer_1");
    var svgLayer2;
	svgLayer2 = svgRect.getElementsByClassName("st2");
	svgLayer2[0].style.fill = layer2Color;
	console.log("changeLayer2Color Successful...");
}

function changeLayer3Color() {
	var layer3Color = document.getElementById("layer3Color").value;
	var svgRect = document.getElementById("Layer_1");
    var svgAccents;
	svgAccents = svgRect.getElementsByClassName("st3");
	svgAccents[0].style.fill = layer3Color;
	console.log("changeLayer3Color Successful...");
}

function colorReset() {
	var svgRect = document.getElementById("Layer_1");
	var svgBody = svgRect.getElementById("Body");
	svgBody.setAttribute("fill",defaultBaseColor);
	var svgLayer1 = svgRect.getElementById("Layer-01");
	svgLayer1.setAttribute("fill",defaultLayer1Color);
	var svgLayer2 = svgRect.getElementById("Layer-02");
	svgLayer2.setAttribute("fill",defaultLayer2Color);
    var svgAccents = svgRect.getElementById("Accents");
	svgAccents.setAttribute("fill",defaultAccentColor);
	console.log("colorReset Successful...");
}