<?php
$type = $_GET['type'];
$route = 'images/'.$type.'/';
$gradient = $_GET['gradient']==''?0:$_GET['gradient'];
$backImage = $_GET['backImage']==''?0:$_GET['backImage'];
$frontImage = $_GET['frontImage']==''?0:$_GET['frontImage'];
$svg_file = file_get_contents($route.$_GET['fileName']);

$find_string   = '<svg';
$position = strpos($svg_file, $find_string);

$svg_file_new = substr($svg_file, $position);
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Configurator Test</title>
<link href="style/style.css" rel="stylesheet" type="text/css" />
<script language="javascript" src="js/script-<?php echo $type;?>.js"></script>

</head>

<body>
<div class="clear">
	<input type="text" name="baseColor" id="baseColor" size="7" />
	<input type="button" value="Change Base Color" onclick="changeBaseColor();"/>
    <?php
	if($gradient == 1)
	{
	?>
	<input type="text" name="gradientLayerColor" id="gradientLayerColor" size="7" />
    <input type="button" value="Change Gradient Layer Color" onclick="changeGradientColor();"/>
	<?php
	}
	?>
    <input type="text" name="layer1Color" id="layer1Color" size="7" />
    <input type="button" value="Change Layer1 Color" onclick="changeLayer1Color();"/>
    <input type="text" name="layer2Color" id="layer2Color" size="7" />
    <input type="button" value="Change Layer2 Color" onclick="changeLayer2Color();"/>
    <input type="text" name="layer3Color" id="layer3Color" size="7" />
    <input type="button" value="Change Layer3 Color" onclick="changeLayer3Color();"/>
    <input type="hidden" name="gradientLayer" id="gradientLayer" value="<?php echo $gradient;?>" />
    <input type="button" value="Reset" onclick="colorReset();"/>
</div>

<div class="shadowImageDiv" name="shirt_shadow" id="shirt_shadow"><img name="shirt_shadow_img" id="shirt_shadow_img" src="<?php echo $route?>shadow.png" width="850" height="417" class="rmarg" /></div>
<div class="baseImageDiv" name="shirt_base" id="shirt_base"><img name="shirt_base_img" id="shirt_base_img" src="<?php echo $route?>overlay.png" width="850" height="417" class="rmarg" /></div>
<div class="level1ImageDiv" name="shirt_base" id="shirt_base">
	<?php
	echo $svg_file_new;
	?>
</div>
<?php
if($backImage == 1)
{
?>
<div class="level2ImageDiv" name="custom_logo" id="custom_logo">
	<img src="images/logo1.png" height="66" width="100" />
</div>
<?php
}
if($frontImage == 1)
{
?>
<div class="level3ImageDiv" name="custom_logo" id="custom_logo">
	<img src="images/logo1.png" height="33" width="50" />
</div>
<?php
}
?>
</body>
<script language="javascript">getDefaultColors();</script>
</html>
