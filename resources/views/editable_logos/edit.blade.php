@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Edit an Editable Logo
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($prints, ['route' => ['editable_logos.update', $prints->id], 'method' => 'patch']) !!}

                        @include('editable_logos.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection