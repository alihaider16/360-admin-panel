<style>
    .tt{
        width: 100px;
        max-width: 100px;
        max-height: 100px;
    }
</style>
<table class="table table-responsive"  id="dataTable">
    <thead>
        <tr>
			<th>Logo</th>
            <th>Name</th>
            <th>Logo CSS</th>
            <th>Text CSS</th>
			<th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($prints as $prints)
        <tr>
            <td>
                <img class="tt" src="{{ asset('img/editable_logos/') }}/{!! $prints->logo !!}">
            </td>
			<td>
				{!! $prints->name !!}
			</td>
			<td>
				{!! $prints->logoCSS !!}
			</td>
			<td>
				{!! $prints->textCSS !!}
			</td>
            <td>
                {!! Form::open(['route' => ['editable_logos.destroy', $prints->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('editable_logos.show', [$prints->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
					<?php /*?>
					<a href="{!! route('editable_logos.edit', [$prints->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
					<?php */?>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>