<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $prints->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $prints->name !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('logoCSS', 'logoCSS:') !!}
    <p>{!! $prints->logoCSS !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('textCSS', 'Text CSS:') !!}
    <p>{!! $prints->textCSS !!}</p>
</div>


<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created:') !!}
    <p>{!! $prints->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated:') !!}
    <p>{!! $prints->updated_at !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Photo:') !!}
    <p>{!! $prints->logo !!}</p>
    <img class="tt" src="{{ asset('img/editable_logos/') }}/{!! $prints->logo !!}">
</div>