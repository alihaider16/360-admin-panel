<?php
$homeNav = '';
$productNav = '';
$designLabNav = '';
$teamNav = '';
$orderNav = '';
$faqsNav = ' active';
$contactNav = '';
?>
<!DOCTYPE html>
<html lang="en">
   <head>
      @include('layouts.front_header')
	
   </head>
   <style media="screen">
	.search-container{
		width:25%;
		float:right;
	}
	.search-container  input[type=text] {
		padding: 6px;
		margin-top: 8px;
		font-size: 17px;
		border: 1px solid #bababa;
	}
	.search-container button {
		float: right;
		padding: 7px 10px;
		margin-top: 8px;
		margin-right: 30px;
		background: #ddd;
		font-size: 17px;
		border: none;
		cursor: pointer;
	}
	.accordion {
		background-color: #eee;
		color: #444;
		cursor: pointer;
		padding: 18px;
		width: 100%;
		border: none;
		text-align: left;
		outline: none;
		font-size: 15px;
		transition: 0.4s;
	}
	
	.active-accor, .accordion:hover {
		background-color: #ccc;
	}
	
	.accordion:after {
		content: '\002B';
		color: #000;
		font-weight: bold;
		float: right;
		margin-left: 5px;
		font-size: 18px;
	}
	.accordion:focus{
		outline:none !important;
	}
	
	.active-accor:after {
		content: "\2212";
	}
	
	.panel {
		padding: 0 18px;
		background-color: white;
		max-height: 0;
		overflow: hidden;
		transition: max-height 0.2s ease-out;
	}
	@media screen and (max-width: 600px) {
		.topnav .search-container {
			float: none;
		}
	}
	</style>
   <body>
      <!-- eyebrow banner -->
      @include('layouts.front_nav')
      <!-- end navbar container -->

      <div class="container">
    <!--our promise -->
    <div class="row">
      <div class="col-md-12">
        <div class="search-container">
          <form action="/action_page.php">
            <input type="text" placeholder="Search.." name="search">
            <button type="submit"><i class="fas fa-search"></i></button>
          </form>
        </div>
      </div>
      <div class="col-md-12">
        <div class="our-promise text-center">
          <h2>FREQUENTLY ASKED QUESTIONS</h2>
        </div>
      </div>
    </div>
  </div>
  <div class="container" style="padding-bottom:15%">
  @foreach($faq as $q)
    <div class="row">
      <button class="accordion mb-4">{!! $q->question !!}</button>
      <div class="panel">
        <p>{!! $q->answer !!}</p>
      </div>
    </div>
  @endforeach
  </div>
  @include('layouts.front_footer') 
   </body>
   <!-- Scripts -->
   <script>
var acc = document.getElementsByClassName("accordion");
var i;

for (i = 0; i < acc.length; i++) {
  acc[i].addEventListener("click", function() {
    this.classList.toggle("active-accor");
    var panel = this.nextElementSibling;
    if (panel.style.maxHeight) {
      panel.style.maxHeight = null;
    } else {
      panel.style.maxHeight = panel.scrollHeight + "px";
    }
  });
}
</script>
   @include('layouts.front_footerScript')
</html>