@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Customer File</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container">
            <h5>Showing latest 400 files.</h5>
            <hr>
            <div class="row">
                <div class="col-md-4" style="float: left;"> <input id="name" type="text" name="name" class="form-control" placeholder="Enter Id For Download"
                required="required" data-error="Setting Required"><button class="btn btn-success m-t-b">Download</button></div>
            </div>
            <!--Table-->
            @include('customer_file.table')
            <!--End table-->

        </div>

    </div>
@endsection