@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Homepage Banners
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('homepage_banners.show_fields')
                    <a href="{!! route('homepage_banners.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
