@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Homepage Banners
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($homepageBanners, ['route' => ['homepage_banners.update', $homepageBanners->id], 'method' => 'patch']) !!}

                        @include('homepage_banners.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection