<style>
    .tt{
        width: 100px;
        max-width: 100px;
        max-height: 100px;
    }
</style>
<table class="table table-responsive" id="dataTable">
    <thead>
        <tr>
        <th>Photo</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($homepage_banners as $homeBanner)
        <tr>
            <td>
                <img class="tt" src="{{ asset('img/banners/') }}/{!! $homeBanner->image !!}">
            </td>
            <td>
                {!! Form::open(['route' => ['homepage_banners.destroy', $homeBanner->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('homepage_banners.show', [$homeBanner->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>