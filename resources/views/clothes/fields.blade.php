<!-- Name Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('base', 'Base:') !!}
    {!! Form::text('base', null, ['class' => 'form-control image']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('overlay', 'Overlay:') !!}
    {!! Form::text('overlay', null, ['class' => 'form-control image']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('thumb', 'Thumb:') !!}
    {!! Form::text('thumb', null, ['class' => 'form-control image']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('clothes.index') !!}" class="btn btn-default">Cancel</a>
</div>
