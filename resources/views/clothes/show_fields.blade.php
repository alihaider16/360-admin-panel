<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $clothes->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $clothes->name !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('thumb', 'thumb:') !!}
    <p><img class="img-fluid col-sm-12" src="{{ asset('img/t-shirts/') }}/{!! $clothes->thumb !!}_thumb.png"></p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Images:') !!}
    <p><img class="img-fluid col-sm-6" src="{{ asset('img/t-shirts/') }}/{!! $clothes->base !!}_base.png"><img class="img-fluid col-sm-6" src="{{ asset('img/t-shirts/') }}/{!! $clothes->overlay !!}_overlay.png"></p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created:') !!}
    <p>{!! $clothes->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated:') !!}
    <p>{!! $clothes->updated_at !!}</p>
</div>

