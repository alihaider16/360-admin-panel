<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', $clothes['name'], ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::hidden('id', $clothes['id'], ['class' => 'form-control']) !!}
    {!! Form::hidden('base', null, ['class' => 'form-control image']) !!}
    {!! Form::hidden('overlay', null, ['class' => 'form-control image']) !!}
    {!! Form::hidden('thumb', null, ['class' => 'form-control image']) !!}
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('clothes.index') !!}" class="btn btn-default">Cancel</a>
</div>
