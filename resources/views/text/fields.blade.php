<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('heading', 'Heading:') !!}
    {!! Form::hidden('id', null) !!}
	{!! Form::text('heading', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12">
    {!! Form::label('text', 'Text:') !!}
    {!! Form::text('text', null, ['class' => 'form-control']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('text.index') !!}" class="btn btn-default">Cancel</a>
</div>
