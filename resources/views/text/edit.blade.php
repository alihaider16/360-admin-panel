@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Homepage Text
        </h1>
   </section>
   <div class="content">
       @include('adminlte-templates::common.errors')
       <div class="box box-primary">
           <div class="box-body">
               <div class="row">
                   {!! Form::model($homeText, ['route' => ['text.update', $homeText->id], 'method' => 'patch']) !!}

                        @include('text.fields')

                   {!! Form::close() !!}
               </div>
           </div>
       </div>
   </div>
@endsection