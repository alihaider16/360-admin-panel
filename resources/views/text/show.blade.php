@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Homepage Text
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('text.show_fields')
                    <a href="{!! route('text.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
