<?php
$homeNav = '';
$productNav = '';
$designLabNav = ' active';
$teamNav = '';
$orderNav = '';
$faqsNav = '';
$contactNav = '';
?>
<!doctype html>
<html lang="en">
	<head>
		@include('layouts.front_header')
	</head>
	<body>
		@include('layouts.front_nav')
		<!-- Content -->
		@include('layouts.front_footer')
	</body>
	@include('layouts.front_footerScript')
</html>