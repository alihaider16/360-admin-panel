<?php
$homeNav = '';
$productNav = '';
$designLabNav = '';
$teamNav = '';
$orderNav = '';
$faqsNav = '';
$contactNav = ' active';
?>
<!doctype html>
<html lang="en">
	<head>
		@include('layouts.front_header')
	</head>
	<style media="screen">
         .our-team-goal p{
         margin-left: 72px;
         }
         .how-to-order-button{
         background:#7da7d9;
         color:#000;
         padding: 7px 12px;
         font-size: 17px;
         }
      </style>
	<body>
		@include('layouts.front_nav')
		<!-- Content -->
		<div class="container">
         <!--Place your Order -->
         <div class="row">
            <div class="our-team">
               <div class="col-md-12">
                  <h2 class="text-center">Contact Us</h2>
                  <hr style="margin-left: 28%; margin-right: 25%;">
               </div>
            </div>
            <div class="container">
               <div class="row">
                  <div class="col-md-6 offset-md-3">
                     <div class="card card-outline-secondary mb-4">
                        <div class="card-body" style="background:#eaedf6;">
                           <form autocomplete="off" class="form" role="form">
                              <fieldset>
                                 <label class="mb-0" for="name2">Name</label>
                                 <div class="row mb-1">
                                    <div class="col-lg-12">
                                       <input class="form-control" id="name2" name="name2" required="" type="text">
                                    </div>
                                 </div>
                                 <label class="mb-0" for="email2">Email</label>
                                 <div class="row mb-1">
                                    <div class="col-lg-12">
                                       <input class="form-control" id="email2" name="email2" required="" type="text">
                                    </div>
                                 </div>
                                 <label class="mb-0" for="product">Product</label>
                                 <div class="row mb-1">
                                    <div class="col-lg-12">
                                       <input class="form-control" id="product" name="email2" required="" type="text">
                                    </div>
                                 </div>
                                 <label class="mb-0" for="message2">Message</label>
                                 <div class="row mb-1">
                                    <div class="col-lg-12">
                                       <textarea class="form-control" id="message2" name="message2" required="" rows="6"></textarea>
                                    </div>
                                 </div>
                                 <button class="btn  btn-lg d-block mx-auto how-to-order-button mt-4" type="submit">Place Order</button>
                              </fieldset>
                           </form>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-6">
                     <div class="how-to-order-around">
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </div>
		<!--End---->
		@include('layouts.front_footer')
	</body>
	@include('layouts.front_footerScript')
</html>