<form id="product-form" class="m-t-b" method="post" action="" role="form">
    <div class="messages"></div>
    <div class="controls">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" name="name" class="form-control" placeholder="e.g English"
                           required="required" data-error="Language Required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!-- input Fields-->
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Save">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>
</form>