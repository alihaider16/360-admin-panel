@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Languages</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container">
            <h5>Here You can Overrride default Languages strige.Once you've created a language you can set the "default
                language" on any distributers or company.</h5>
            <hr>
            <div class="m-t-b"><button id="p-step-btn" class="btn btn-primary pushme with-color"
                                       onclick="divToggle('product-form-toggle')">Add Language</button></div>
            <!--Form Start-->
            <div class="d-none" id="product-form-toggle">
                @include('language.form')
            </div>
            <!--Form End-->
            <!--Table-->
            @include('language.table')
            <!--End table-->

        </div>

    </div>
@endsection