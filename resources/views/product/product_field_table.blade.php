<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%"
       role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Name: activate to sort column ascending" style="width: 137px;">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 215px;">Key</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Name</th>
        <th class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Age: activate to sort column ascending" aria-sort="descending" style="width: 44px;">
            Step</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">type</th>
        <th class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Age: activate to sort column ascending" aria-sort="descending" style="width: 44px;">
            View Action</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Pos</th>
        <th class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Age: activate to sort column ascending" aria-sort="descending" style="width: 44px;">
            SVG</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Price</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th rowspan="1" colspan="1">ID</th>
        <th rowspan="1" colspan="1">Category ID</th>
        <th rowspan="1" colspan="1">Category Name</th>
        <th rowspan="1" colspan="1">Position</th>

    </tr>
    </tfoot>
    <tbody>


    <tr role="row" class="odd">
        <td class="">Ashton Cox</td>
        <td>Junior Technical Author</td>
        <td>San Francisco</td>
        <td class="sorting_1">66</td>
        <td class="">Ashton Cox</td>
        <td>Junior Technical Author</td>
        <td>San Francisco</td>
        <td class="sorting_1">66</td>
        <td class="sorting_1">66</td>

    </tr>
    <tr role="row" class="even">
        <td class="">Michael Silva</td>
        <td>Marketing Designer</td>
        <td>London</td>
        <td class="sorting_1">66</td>
        <td class="">Ashton Cox</td>
        <td>Junior Technical Author</td>
        <td>San Francisco</td>
        <td class="sorting_1">66</td>
        <td class="sorting_1">66</td>

    </tr>

    </tbody>
</table>