<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%"
       role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Name: activate to sort column ascending" style="width: 70px;">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 215px;">Name</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Position</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 100px;"></th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 100px;"></th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 100px;"></th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th rowspan="1" colspan="1">ID</th>
        <th rowspan="1" colspan="1">Name</th>
        <th rowspan="1" colspan="1">Position</th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
        <th rowspan="1" colspan="1"></th>
    </tr>
    </tfoot>
    <tbody>
    @php($i = 1)
    @foreach($product_steps as $item)
        @if($item->parent_id == '')
    <tr role="row" class="odd">
        <td class="">{{$i++}}</td>
        <td>{{$item->name}}</td>
        <td>{{$item->position}}</td>
        <td>
            <a href="{{route('product.step.edit',$item->id)}}" class="btn btn-primary btn-sm">Edit Step</a>
        </td>
        <td>
            <a href="{{route('product.show.step.field',$item->id)}}" class="btn btn-primary btn-sm">Add Field Group</a>
        </td>
        <td>
            <a href="#" class="btn btn-danger btn-sm">Delete</a>
        </td>
    </tr>
        @else
            <tr role="row" class="odd">
                <td class="">{{$i++}}</td>
                <td><span style="margin-right: 10px;"></span>{{$item->name}}</td>
                <td>{{$item->position}}</td>
                <td>
                    <a href="{{route('product.edit.step.field',$item->id)}}" class="btn btn-primary btn-sm">Edit Field Group</a>
                </td>
                <td>
                    <a href="#" class="btn btn-danger btn-sm">Delete</a>
                </td>
            </tr>
        @endif
    @endforeach
    </tbody>
</table>