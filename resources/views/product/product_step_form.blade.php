<form class="m-t-b" method="post" action="{{route('product.step.create')}}" role="form">
    @csrf
    <div class="messages"></div>
    <div class="controls">
        <h3>Add Step</h3>
        <hr>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" name="name" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <input type="hidden" name="product_id" value="{{$editProduct->id}}">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="position">Position</label>
                    <input id="position" type="number" name="position" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="paretan_step">Parent Step</label>
                    <select class="form-control" name="parent_step">
                        <option value=""></option>
                        @foreach($product_steps as $row)
                            @if($row->parent_id == '')
                                <option value="{{$row->id}}">{{$row->name}}</option>
                            @endif
                        @endforeach
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>

        </div>
        <!---End-->
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Save Step">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>