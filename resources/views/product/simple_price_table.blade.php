<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%"
       role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Name: activate to sort column ascending" style="width: 137px;">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 215px;">Minimum Quantity
        </th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Unit Price</th>


        <th class="Fixed Discoun" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Discount: activate to sort column ascending" style="width: 100px;">Fixed Discount</th>

        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Percent Discount: activate to sort column ascending" style="width: 100px;">Percent
            Discount</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th rowspan="1" colspan="1">ID</th>
        <th rowspan="1" colspan="1">Minimum Quantity</th>
        <th rowspan="1" colspan="1">Unit Price</th>
        <th rowspan="1" colspan="1">Fixed Discoun</th>
        <th rowspan="1" colspan="1">Percent Discount</th>
    </tr>
    </tfoot>
    <tbody>
    <tr role="row" class="odd">
        <td class="">Ashton Cox</td>
        <td>Junior Technical Author</td>
        <td>San Francisco</td>
        <th rowspan="1" colspan="1">Product/Design Name</th>
        <th rowspan="1" colspan="1">Position</th>
    </tr>
    <tr role="row" class="even">
        <td class="">Michael Silva</td>
        <td>Marketing Designer</td>
        <td>London</td>
        <th rowspan="1" colspan="1">Product/Design Name</th>
        <th rowspan="1" colspan="1">Position</th>
    </tr>
    <tr role="row" class="odd">
        <td class="">Jackson Bradshaw</td>
        <td>Director</td>
        <td>New York</td>
        <th rowspan="1" colspan="1">Product/Design Name</th>
        <th rowspan="1" colspan="1">Position</th>
    </tr>
    </tbody>
</table>