<form class="m-t-b" method="post" action="" role="form">
    <div class="messages"></div>
    <div class="controls">
        <h3>Add Product Field</h3>
        <hr>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" name="name" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="position">Position</label>
                    <input id="position" type="number" name="position" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="step">Step</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" is="step" name="step">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="type">type</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="type" name="type">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                    <p>
                        Checkbox: show/hide an SVG snippet.
                        Colour Picker/Colour Tile Picker: pick a colour from a data source.
                        Custom: any custom SVG.
                        Description: text on the form.
                        Dropdown: select from a data source.
                        Horizontal Rule: a horizontal line on the form.
                        Image: image file upload.
                        Pattern Transform: position and scale a pattern's content.
                        Tile Picker (image): select from a list of data source item images.
                        Tile Picker (description): select from a data source item descriptions.
                        Transform: position and scale SVG elements.
                        Text Box: enter text (e.g. for Set Text view action).
                        Text Area: multiline text for notes/comments. Does not make multi-line SVG text.
                        Slider: set a number value e.g. font size.
                        Team Details: enter each team with each players' details: size, name and number. Must use
                        'teamdetails' key.
                        Editor Units: Add multiple images and text. Must use 'editor' key.
                        Color List: Generate a list of colours from an image and allow customer to enter pantones
                        next to each major colour. Key must be same as image key + list. e.g. key = "imagelist" if
                        the image key was "image".
                    </p>
                </div>
            </div>
        </div>
        <!---End-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="discription">Description</label>
                    <input id="discription" type="text" name="discription" class="form-control" placeholder=""
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Save Step">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>