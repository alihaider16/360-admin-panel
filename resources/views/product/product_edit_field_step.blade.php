@extends('layouts.admin')
@section('content')
    <form action="{{route('product.step.update',$editField->id)}}" method="post">
        @csrf
        <div class="container">
            <div class="row" style="margin-top: 20px;">
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="">Name</label>
                        <input type="text" name="name" value="{{$editField->name}}" class="form-control">
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="">Position</label>
                        <input type="number" name="position" value="{{$editField->position}}" class="form-control">
                    </div>
                    <div class="form-group col-lg-6 col-sm-12">
                        <label for="">Parent Step</label>
                        <select name="parent_step" class="form-control">
                            <option value="{{$editField->parent_id}}"></option>
                        </select>
                    </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-4">
                    <button type="submit" class="btn btn-success">Update</button>
                </div>
            </div>
        </div>
    </form>
@endsection