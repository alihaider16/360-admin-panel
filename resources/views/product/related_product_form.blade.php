<form id="product-form" class="m-t-b" method="post" action="{{route('product_related.store')}}" role="form">
@csrf
    <div class="messages"></div>

    <div class="controls">
        <h3>Related Products</h3>
        <hr>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="category">Product</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" name="parent_product" required>
                        <option value="">Select Product</option>
                        @forelse($products as $product)
                        <option value="{{$product->id}}">{{$product->name}}</option>
                        @endforeach
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="category">Related Product</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" name="related_product" required>
                        <option value="">Select Related Product</option>
                        @forelse($products as $product)
                            <option value="{{$product->id}}">{{$product->name}}</option>
                            @endforeach
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Submit">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>
</form>