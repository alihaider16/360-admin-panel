<form class="m-t-b" method="post" action="" role="form">
    <div class="messages"></div>
    <div class="controls">
        <h3>Add Design</h3>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cart_p">Name</label>
                    <input id="cart_p" type="text" name="fname" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cart_p">SKU</label>
                    <input id="cart_p" type="number" name="cart_p" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cart_p">Cart Price</label>
                    <input id="cart_p" type="number" name="cart_p" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="category">Override Image</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cart_p">Front SVG Snippet</label>
                    <input id="cart_p" type="text" name="fname" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cart_p">Back SVG Snippet</label>
                    <input id="cart_p" type="number" name="cart_p" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <input type="checkbox"><label>Show SVG Defs</label>
                <!-- <div class="form-group">
<label for="cart_p">Front SVG Snippet</label>
<input id="cart_p" type="text" name="fname" class="form-control" placeholder="e.g 10"
required="required" data-error="Position required">
<div class="help-block with-errors"></div>
</div> -->
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cart_p">Position</label>
                    <input id="cart_p" type="number" name="cart_p" class="form-control" placeholder="e.g 10"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <input type="checkbox"><label>Active</label>
                <!-- <div class="form-group">
<label for="cart_p">Front SVG Snippet</label>
<input id="cart_p" type="text" name="fname" class="form-control" placeholder="e.g 10"
required="required" data-error="Position required">
<div class="help-block with-errors"></div>
</div> -->
            </div>
        </div>
        <!---End-->
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Save Step">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>