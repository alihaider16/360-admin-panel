<form id="product-form" class="m-t-b" method="post" action="" role="form">

    <div class="messages"></div>

    <div class="controls">
        <h3>Add Price Tires</h3>
        <hr>
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="url_slug">Minimum Quantity</label>
                    <input id="url_slug" type="text" name="url_slug" class="form-control"
                           placeholder="Please Valid URL *" required="required" data-error="url_slug is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="url_slug">Unit Price</label>
                    <input id="url_slug" type="text" name="url_slug" class="form-control"
                           placeholder="Please Valid URL *" required="required" data-error="url_slug is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cart_p">Fixed Discount</label>
                    <input id="cart_p" type="number" name="cart_p" class="form-control" placeholder="e.g 29.99"
                           required="required" data-error="Cart Price required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cart_p">Percent Discount</label>
                    <input id="cart_p" type="number" name="cart_p" class="form-control" placeholder="e.g 29.99"
                           required="required" data-error="Cart Price required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Send message">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>
</form>