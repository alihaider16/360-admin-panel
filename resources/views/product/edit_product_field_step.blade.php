@extends('layouts.admin')
@section('content')
    <div class="clearfix"></div>

    @include('flash::message')

    <div class="clearfix"></div>

    <form action="{{route('product.update.step.field',$editStepField->id)}}" method="post">
        @csrf
        <div class="container">
            <div class="row" style="margin-top: 20px;">
                <div class="form-group col-lg-6 col-sm-12">
                    <label for="">Name</label>
                    <input type="text" name="name" value="{{$editStepField->name}}" class="form-control">
                </div>
                <div class="form-group col-lg-6 col-sm-12">
                    <label for="">Position</label>
                    <input type="number" name="position" value="{{$editStepField->position}}" class="form-control">
                </div>
                <div class="form-group col-lg-6 col-sm-12">
                    <label for="">Parent Step</label>
                    <select name="parent_step" class="form-control">
                        <option value="{{$editStepField->parent_id}}">
                            {{\App\Http\Controllers\ProductStepController::getParentName($editStepField->parent_id)}}
                        </option>
                    </select>
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-4">
                    <button type="submit" class="btn btn-success">Submit</button>
                </div>
            </div>
        </div>
    </form>
@endsection