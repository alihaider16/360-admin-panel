<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%"
       role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Name: activate to sort column ascending" style="width: 137px;">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 215px;">Product Name</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Image</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 215px;"> RelatedProduct Name</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Related Image</th>

    </tr>
    </thead>
    <tfoot>
    <tr>
        <th rowspan="1" colspan="1">ID</th>
        <th rowspan="1" colspan="1">Product Name</th>
        <th rowspan="1" colspan="1">Image</th>
        <th rowspan="1" colspan="1">Related Product Name</th>
        <th rowspan="1" colspan="1">Related Image</th>

    </tr>
    </tfoot>
    <tbody>
    @php($i = 1)
    @foreach($related_products as $product)
    <tr role="row" class="odd">
        <td class="">{{$i++}}</td>
        <td>{{$product->product->name}}</td>
        <td>{!! $product->product->threeD_model !!}</td>
        <td>
            {{\App\Http\Controllers\ProductController::getRelatedName($product->related_product)}}
        </td>
        <td>
            {!! \App\Http\Controllers\ProductController::getRelatedImage($product->related_product) !!}
        </td>
    </tr>
    @endforeach
    </tbody>
</table>