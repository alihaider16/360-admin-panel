<!--Table-->
<table id="tbl-Product" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%"
       role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Name: activate to sort column ascending" style="width: 20px;">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 100px;">Category Image
        </th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Category Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 70px;">Slug
        </th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 50px;">URL Override
        </th>
        <th class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Age: activate to sort column ascending" aria-sort="descending"
            style="width: 44px;">
            Position</th>
        <th class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Age: activate to sort column ascending" aria-sort="descending"
            style="width: 44px;">
            Action</th>

    </tr>
    </thead>
    <tfoot>
    <tr>
        <th rowspan="1" colspan="1">ID</th>
        <th rowspan="1" colspan="1">Category Image</th>
        <th rowspan="1" colspan="1">Category Name</th>
        <th rowspan="1" colspan="1">Slug</th>
        <th rowspan="1" colspan="1">URL Override</th>
        <th rowspan="1" colspan="1">Position</th>
        <th rowspan="1" colspan="1">Action</th>
    </tr>
    </tfoot>
    <tbody>
    @php($i =1)
    @foreach($categories as $category)
        <tr role="row" class="odd">
            <td>{{$i++}}</td>
            <td>
                <img src="{{asset('/').'images/categories/'.$category->image}}" alt="image" width="100" height="100" class="img-fluid">
            </td>
            <td>{{$category->name}}</td>
            <td>{{$category->slug}}</td>
            <td>
                <span style="background-color: {{$category->url_override}}; padding: 5px 12px 5px 12px;"></span>
            </td>
            <td class="sorting_1">{{$category->position}}</td>
            <td>
                <a href="{{route('product_cat.edit',$category->id)}}" class="btn btn-danger">Edit</a>
            </td>
        </tr>
    @endforeach
    </tbody>
</table>
<!--End table-->