@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Edit Product</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
<form class="m-t-b" method="post" action="{{route('product_cat_edit.store',$cat_edit->id)}}" enctype="multipart/form-data">
    @csrf
    <div class="messages"></div>

    <div class="controls">
        <hr>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="parent">Parent</label>
                    <select class="form-control" id="parent" name="parent">
                        @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" name="name" class="form-control" placeholder="e.g 10"
                          value="{{$cat_edit->name}}" required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="url_slug">URL Slug</label>
                    <input id="url_slug" type="text" name="url_slug" class="form-control"
                          value="{{$cat_edit->slug}}" placeholder="Please Valid URL *" required="required"
                           data-error="url_slug is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="url_override">URL Overrride</label>
                    <div class="input-group">
                        <span class="input-group-addon" style="background: {{$cat_edit->url_override}}"></span>
                        <input id="url_override" value="{{$cat_edit->url_override}}" type="text" name="url_override" class="form-control" placeholder="e.g #ffffff" required>
                    </div>
                    <p>Set inside material color.Leave emplty to leave the to mirror the front material
                    </p>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <img src="{{asset('images/categories/'.$cat_edit->image)}}" height="100" width="100" class="img-fluid" alt="img">
                    <label for="image">image</label>
                    <input type="file" name="image">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="position">Position</label>
                    <input id="position" type="number" name="position" class="form-control"
                         value="{{$cat_edit->position}}"  placeholder="e.g 0.5" required="required" data-error="required">
                    <p>Use the same as your ecommerece unit</p>

                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <div class="ar_display"><input type="checkbox" value="">
                        <lable>Disable Back View</lable>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
            </div>

        </div>
        <!---End-->
        <!--input Fields-->

        <div class="row">

            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Save">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>
    </div>
    @endsection
