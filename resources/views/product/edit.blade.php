@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Edit Product</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#product" role="tab" data-toggle="tab" aria-controls=" product">product</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#cat" role="tab" data-toggle="tab" aria-controls="Category">Category</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#product_step" role="tab" data-toggle="tab" aria-controls="Product Step">Product
                                Step</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Product_field" role="tab" data-toggle="tab" aria-controls="Product field">Product
                                field</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Product_design" role="tab" data-toggle="tab"
                               aria-controls="Product Design">Product Design</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#size" role="tab" data-toggle="tab" aria-controls="Size">Size</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Group_Item" role="tab" data-toggle="tab" aria-controls="Group Item">Group
                                Item</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Related_Product" role="tab" data-toggle="tab"
                               aria-controls="Related Product">Related Product</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Cross_Sell" role="tab" data-toggle="tab" aria-controls="Cross Sell">Cross
                                Sell</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Simple_Price_Tires" role="tab" data-toggle="tab"
                               aria-controls="Simple Price Tires">Simple Price Tires</a>
                        </li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="product">
                            @include('product.product_edit_form')
                        </div>

                        <div role="tabpanel" class="tab-pane fade" id="cat">
                            @include('product.product_edit_product_cat')
                        </div>

                        <!--product_step tab-contant-->
                        <div role="tabpanel" class="tab-pane fade" id="product_step">
                            <div class="m-t-b"><button id="p-step-btn" class="btn btn-primary pushme with-color"
                                                       onclick="divToggle('p-step-form')">Add Step</button></div>
                            <!--Form Start-->
                            <div class="d-none" id="p-step-form">
                                @include('product.product_step_form')
                            </div>
                            <!--Form End-->
                            @include('product.product_step_table')
                        </div>
                        <!--end-->
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection