<form class="m-t-b" method="post" action="{{route('product.store')}}" enctype="multipart/form-data">
@csrf
    <div class="messages"></div>

    <div class="controls">
        <h3>Add Product</h3>
        <hr>
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name *</label>
                    <input id="name" type="text" name="name" class="form-control"
                           placeholder="Please enter your firstname *" required="required"
                           data-error="Firstname is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="url_slug">URL Slug *</label>
                    <input id="url_slug" type="text" name="url_slug" class="form-control"
                           placeholder="Please Valid URL *" required="required" data-error="url_slug is required.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="category">Category *</label>
                    <select class="form-control" id="category" name="category">
                        @foreach($categories as $category)
                        <option value="{{$category->id}}">{{$category->name}}</option>
                        @endforeach
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="cart_price">Cart Price*</label>
                    <input id="cart_price" type="number" name="cart_price" class="form-control" placeholder="e.g 29.99"
                           required="required" data-error="Cart Price required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="minimum_quantity">Minimum Quantity*</label>
                    <input id="minimum_quantity" type="number" name="minimum_quantity" class="form-control" placeholder="e.g 5"
                           required="required" data-error="">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="product_weight">Product Weight*</label>
                    <input id="product_weight" type="number" name="product_weight" class="form-control" placeholder="e.g 0.5"
                           required="required" data-error="required">
                    <p>Use the same as your ecommerece unit</p>
                    <div class="ar_display"><input type="checkbox" value="">
                        <lable>Hide Price Breakdown</lable>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="front_overly">Front Overlay*</label>
                    <select name="font_overlay" class="form-control">
                        <option value="">Select Overlay</option>
                        @foreach($files as $file)
                            @if(pathinfo($file->name, PATHINFO_EXTENSION) == 'png' ||
                                pathinfo($file->name, PATHINFO_EXTENSION) == 'jpg' ||
                                pathinfo($file->name, PATHINFO_EXTENSION) == 'jpeg')
                                <option value="{{$file->image}}" style="background-image: url('{{asset('images/Files/'.$file->image)}}');">{{$file->name}}</option>
                            @endif
                        @endforeach
                    </select>
                    <p>Image Overlay with shading.Only required for products with static overlay e.g without
                        selectable collar style.</p>
                    <div class="ar_display"><input type="checkbox" value="">
                        <lable>Disable Back View</lable>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="front_view_name">Front View Name*</label>
                    <input id="front_view_name" type="text" name="front_view_name" class="form-control" placeholder="e.g Front"
                           required="required" data-error="required">
                    <p>Text entered here override the view Front/Back button text.Entering "Main" here would
                        change
                        "view front" to "view main".</p>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="back_overlay">Back Overlay*</label>
                    <select name="back_overlay" class="form-control">
                        <option value="">Select Overlay</option>
                        @foreach($files as $file)
                            @if(pathinfo($file->name, PATHINFO_EXTENSION) == 'png' ||
                                pathinfo($file->name, PATHINFO_EXTENSION) == 'jpg' ||
                                pathinfo($file->name, PATHINFO_EXTENSION) == 'jpeg')
                                <option value="{{$file->image}}" style="background-image: url('{{asset('images/Files/'.$file->image)}}');">{{$file->name}}</option>
                            @endif
                        @endforeach
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="back_view">Back View Name*</label>
                    <input id="back_view" type="text" name="back_view" class="form-control" placeholder="e.g Back"
                           required="required" data-error="required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="list_image">List image</label>
                    <select name="list_image" class="form-control">
                        <option value="">Select Overlay</option>
                        @foreach($files as $file)
                            @if(pathinfo($file->name, PATHINFO_EXTENSION) == 'png' ||
                                pathinfo($file->name, PATHINFO_EXTENSION) == 'jpg' ||
                                pathinfo($file->name, PATHINFO_EXTENSION) == 'jpeg')
                                <option value="{{$file->image}}" style="background-image: url('{{asset('images/Files/'.$file->image)}}');">{{$file->name}}</option>
                            @endif
                        @endforeach
                    </select>
                    <p>Image that display on product listing page</p>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="3d_modal">3D Model</label>
                    <select name="threeD_modal" class="form-control">
                        <option value="">Select 3D Model</option>
                        @foreach($files as $file)
                            @if(pathinfo($file->name, PATHINFO_EXTENSION) == 'svg')
                                <option value="{{$file->image}}" style="background-image: url('{{asset('images/Files/'.$file->image)}}');">{{$file->name}}</option>
                            @endif
                        @endforeach
                    </select>
                    <p>Select .obj, or .zip file</p>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="back_material">Back Material colours </label>
                    <!-- <input id="b_overlay" type="text" name="b_overlay" class="form-control" placeholder=""  required="required" data-error="">-->
                    <div class="input-group">
                        <span class="input-group-addon"></span><input id="back_material" type="text" name="back_material"
                                                                      class="form-control" placeholder="e.g #ffffff" required="required"
                                                                      data-error="Valid email is required.">
                    </div>
                    <p>Set inside material color.Leave emplty to leave the to mirror the front material </p>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="camera">Cameras</label>
                    <!-- <input id="b_overlay" type="text" name="b_overlay" class="form-control" placeholder=""  required="required" data-error="">-->
                    <select class="form-control" id="camera" name="camera">
                        <option value="1">Test</option>
                        <option value="2">Test</option>
                        <option value="3">Test</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="camera_override">Form Reply camera override</label>
                    <!-- <input id="b_overlay" type="text" name="b_overlay" class="form-control" placeholder=""  required="required" data-error="">-->
                    <select class="form-control" id="camera_override" name="camera_override">
                        <option value="1">Test</option>
                        <option value="2">Test</option>
                        <option value="3">Test</option>
                    </select>
                    <p>Alternative camera to use in form replies</p>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="position_offset">Position offset</label>
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <input id="position_offset_x" type="text" name="position_offset_x" class="form-control" placeholder="X"
                                   required="required" data-error="">
                        </div>
                        <div class="col-md-4">
                            <input id="position_offset_y" type="text" name="position_offset_y" class="form-control" placeholder="Y"
                                   required="required" data-error="">
                        </div>
                        <div class="col-md-4">
                            <input id="position_offset_z" type="text" name="position_offset_z" class="form-control" placeholder="Z"
                                   required="required" data-error="">
                        </div>
                    </div>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="scale">Scale</label>
                    <input id="scale" type="text" name="scale" class="form-control" placeholder="e.g 100"
                           required="required" data-error="Valid email is required.">
                    <div class="help-block with-errors"></div>
                    <p>Default scale= 1.1 Unit= 1 meter</p>
                    <div class="ar_display"><input type="checkbox" value="">
                        <lable>Override form reply model Position/scale</lable>
                    </div>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="light_brightness">Light Brightness</label>
                    <input id="light_brightness" type="tel" name="light_brightness" class="form-control" placeholder="e.g 1.5">
                    <div class="help-block with-errors"></div>
                    <p>Default 1.Twice as bright=2</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="svg_dimention">SVG Dimentsions</label>
                    <div class="row no-gutters">
                        <div class="col-sm-6 ar_display">
                            <input id="svg_dimention_1" type="text" name="svg_dimention_1" class="form-control"
                                   placeholder="e.g 400" required="required" data-error="">
                        </div>
                        <div class="col-sm-6 ar_display">
                            <input id="svg_dimention_2" type="text" name="svg_dimention_2" class="form-control"
                                   placeholder="e.g 400" required="required" data-error="">
                        </div>

                    </div>
                    <div class="help-block with-errors"></div>
                    <p>Example:400x400</p>
                    <p>The width of SVG that that was exported from the illustrator</p>
                    <div class="ar_display"><input type="checkbox" value="">
                        <lable> Show Print view buttton</lable>

                    </div>
                    <p>Enable the button to Toggle between 2D and 3D print view</p>
                </div>

            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="height_baskit">Height on baskit page</label>
                    <input id="height_baskit" type="tel" name="height_baskit" class="form-control" placeholder="e.g 200">
                    <div class="help-block with-errors"></div>
                    <p>The height of the product on the fianl baskit page</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="befault_colour">Default Toggle colours</label>
                    <!-- <input id="b_overlay" type="text" name="b_overlay" class="form-control" placeholder=""  required="required" data-error="">-->
                    <div class="input-group">
                        <span class="input-group-addon"></span><input id="befault_colour" type="text" name="befault_colour"
                                                                      class="form-control" placeholder="e.g #ffffff" required="required"
                                                                      data-error="Valid email is required.">
                    </div>
                    <p>Set inside material color.Leave emplty to leave the to mirror the front material </p>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="pdf_overlay_image">PDF overlay image</label>
                    <select name="b_overlay" class="form-control">
                        <option value="">Select Overlay</option>
                        @foreach($files as $file)
                            @if(pathinfo($file->name, PATHINFO_EXTENSION) == 'png' ||
                                pathinfo($file->name, PATHINFO_EXTENSION) == 'jpg' ||
                                pathinfo($file->name, PATHINFO_EXTENSION) == 'jpeg')
                                <option value="{{$file->image}}" style="background-image: url('{{asset('images/Files/'.$file->image)}}');">{{$file->name}}</option>
                            @endif
                        @endforeach
                    </select>
                    <div class="help-block with-errors"></div>
                    <p>Image that overlayed on the top of select pdfs(Require PDF Customization)</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="position">Position</label>
                    <input id="position" type="text" name="position" class="form-control" placeholder="e.g 10"
                           required="required">
                    <div class="help-block with-errors"></div>
                    <p>Position in the product list</p>
                    <div class="ar_display"><input type="checkbox" value="">
                        <lable>Active</lable>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Save">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>