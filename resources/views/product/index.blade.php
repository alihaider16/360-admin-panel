@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Product</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!--Test Drive-->

                    <!--End-->
                    <!--Nav Tab-->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#product" role="tab" data-toggle="tab" aria-controls=" product">product</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#cat" role="tab" data-toggle="tab" aria-controls="Category">Category</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#product_step" role="tab" data-toggle="tab" aria-controls="Product Step">Product
                                Step</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Product_field" role="tab" data-toggle="tab" aria-controls="Product field">Product
                                field</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Product_design" role="tab" data-toggle="tab"
                               aria-controls="Product Design">Product Design</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#size" role="tab" data-toggle="tab" aria-controls="Size">Size</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Group_Item" role="tab" data-toggle="tab" aria-controls="Group Item">Group
                                Item</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Related_Product" role="tab" data-toggle="tab"
                               aria-controls="Related Product">Related Product</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Cross_Sell" role="tab" data-toggle="tab" aria-controls="Cross Sell">Cross
                                Sell</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Simple_Price_Tires" role="tab" data-toggle="tab"
                               aria-controls="Simple Price Tires">Simple Price Tires</a>
                        </li>
                    </ul>
                    <!--end-->
                    <!-- Tab panes -->
                    <div class="tab-content ">
                        <!--Product Tab Content-->
                        <div role="tabpanel" class="tab-pane fade in active" id="product">
                            <div class="m-t-b"><button id="product-btn" onclick="divToggle('product-form-toggle')"
                                                       class="btn btn-primary pushme with-color">Add Product</button></div>

                            <!--Form Start-->
                            <div class="d-none" id="product-form-toggle">
                                @include('product.product_form')
                            </div>
                            <!--Form End-->
                            <!--Table-->
                            @include('product.product_table')
                            <!--End table-->

                        </div>
                        <!--End-->

                        <!--Category tab-contant-->
                        <div role="tabpanel" class="tab-pane fade" id="cat">
                            <div class="m-t-b"><button id="p-catagory-btn" onclick="divToggle('category-form-toggle')"
                                                       class="btn btn-primary pushme with-color">Add Category</button></div>

                            <!--Form Start-->
                            <div class="d-none" id="category-form-toggle">
                                @include('product.category_forms')
                            </div>
                            <!--Form End-->
                            <!--Table-->
                            @include('product.category_table')
                            <!--End table-->

                        </div>
                        <!--end-->

                        <!--product_step tab-contant-->
{{--                        <div role="tabpanel" class="tab-pane fade" id="product_step">--}}
{{--                            <div class="m-t-b"><button id="p-step-btn" class="btn btn-primary pushme with-color"--}}
{{--                                                       onclick="divToggle('p-step-form')">Add Step</button></div>--}}
{{--                            <!--Form Start-->--}}
{{--                            <div class="d-none" id="p-step-form">--}}
{{--                                @include('product.product_step_form')--}}
{{--                            </div>--}}
{{--                            <!--Form End-->--}}
{{--                            @include('product.product_step_table')--}}
{{--                        </div>--}}
                        <!--end-->

                        <!--product_field tab-contant-->
                        <div role="tabpanel" class="tab-pane fade" id="Product_field">

                            <div class="m-t-b"><button id="p-step-btn" class="btn btn-primary pushme with-color"
                                                       onclick="divToggle('p-field-form-toggle')">Add Product Field</button>
                                <button id="p-step-btn" class="btn btn-primary pushme with-color"
                                        onclick="divToggle('p-field-quantity-toggle')">Add Quantity Field</button>
                                <button id="p-step-btn" class="btn btn-primary pushme with-color"
                                        onclick="divToggle('p-field-team-toggle')">Team Details Field</button>
                                <button id="p-step-btn" class="btn btn-primary pushme with-color"
                                        onclick="divToggle('p-field-editer-toggle')">Editor Field</button>
                            </div>
                            <div class="m-t-b"></div>
                            <!--Form Start-->
                            <div class="d-none" id="p-field-form-toggle">
                                @include('product.add_product_field')
                            </div>
                            <!--Form End-->
                            <!--Form Start-->
                            <div class="d-none" id="p-field-quantity-toggle">
                                @include('product.add_quantity_field')
                            </div>
                            <!--Form End-->
                            <!--Form Start-->
                            <div class="d-none" id="p-field-team-toggle">
                                @include('product.team_detail_field')
                            </div>
                            <!--Form End-->
                            <!--Form Start-->
                            <div class="d-none" id="p-field-editer-toggle">
                                @include('product.editor_field')
                            </div>
                            <!--Form End-->
                            @include('product.product_field_table')
                        </div>
                        <!--end-->

                        <!--product_Design tab-contant-->
                        <div role="tabpanel" class="tab-pane fade" id="Product_design">
                            <h3 class="m-t-b">Product Design</h3>
                            <hr>
                            <div class="m-t-b"><button id="p-step-btn" class="btn btn-primary pushme with-color"
                                                       onclick="divToggle('add-design-inner-tab')">Add Design</button>
                            </div>
                            <div class="d-none" id="add-design-inner-tab">
                                <ul class="nav nav-tabs" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link in active" href="#Design" role="tab" data-toggle="tab">Design</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#Grouped_Items" role="tab" data-toggle="tab">Grouped Items</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#Cross_sells" role="tab" data-toggle="tab">Cross-sells</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane fade in active" id="Design">
                                        <div class="m-t-b"><button id="p-step-btn" class="btn btn-primary pushme with-color"
                                                                   onclick="divToggle('p-design')">Add Design</button>
                                        </div>
                                        <!--Form Start-->
                                        <div class="d-none" id="p-design">
                                            @include('product.add_design_form')
                                        </div>
                                        <!--Form End-->
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="Grouped_Items">
                                        <div class="m-t-b"><button id="p-step-btn" class="btn btn-primary pushme with-color"
                                                                   onclick="divToggle('p-group-items')">Add Grouped Items</button>
                                        </div>
                                        <!--Form Start-->
                                        <div class="d-none" id="p-group-items">
                                            @include('product.add_group_items_form')
                                        </div>
                                        <!--Form End-->
                                    </div>
                                    <div role="tabpanel" class="tab-pane fade" id="Cross_sells">
                                        <div class="m-t-b"><button id="p-step-btn" class="btn btn-primary pushme with-color"
                                                                   onclick="divToggle('p-cross-sells')">Add Cross Sells</button>
                                        </div>
                                        <!--Form Start-->
                                        <div class="d-none" id="p-cross-sells">
                                            @include('product.add_cross_sells')
                                        </div>
                                        <!--Form End-->
                                    </div>
                                </div>
                                <!-- partial -->
                            </div>
                            <div class="m-t-b"></div>
                            @include('product.product_design_table')
                        </div>
                        <!--end-->

                        <!--Size Tab Content-->
                        <div role="tabpanel" class="tab-pane fade" id="size">
                            <div class="m-t-b"><button id="product-btn" onclick="divToggle('size-form-toggle')"
                                                       class="btn btn-primary pushme with-color">Add Size</button></div>

                            <!--Form Start-->
                            <div class="d-none" id="size-form-toggle">
                                @include('product.size_form')
                            </div>
                            <!--Form End-->
                            <!--Table-->
                            @include('product.size_table')
                            <!--End table-->

                        </div>
                        <!--End -->

            <!--Group_Item Tab-->
                        <div role="tabpanel" class="tab-pane fade" id="Group_Item">
                            <div class="m-t-b"><button id="product-btn" onclick="divToggle('group-item-form-toggle')"
                                                       class="btn btn-primary pushme with-color">Add Group Items</button></div>

                            <!--Form Start-->
                            <div class="d-none" id="group-item-form-toggle">
                                @include('product.group_items_form')
                            </div>
                            <!--Form End-->
                            <!--Table-->
                            @include('product.group_items_table')
                            <!--End table-->
                        </div>
                        <!--end-->

                        <!--Related_Product Tab-->
                        <div role="tabpanel" class="tab-pane fade" id="Related_Product">
                            <div class="m-t-b"><button id="product-btn" onclick="divToggle('related-form-toggle')"
                                                       class="btn btn-primary pushme with-color">Add Related Product</button></div>

                            <!--Form Start-->
                            <div class="d-none" id="related-form-toggle">
                                @include('product.related_product_form')
                            </div>
                            <!--Form End-->
                            <!--Table-->
                        @include('product.related_product_table')
                            <!--End table-->

                        </div>
                        <!--end-->

                        <!--Cross_Sell Tab-->
                        <div role="tabpanel" class="tab-pane fade" id="Cross_Sell">
                            <div class="m-t-b"><button id="product-btn" onclick="divToggle('cross-sell-form-toggle')"
                                                       class="btn btn-primary pushme with-color">Add Cross Sells</button></div>

                            <!--Form Start-->
                            <div class="d-none" id="cross-sell-form-toggle">
                                @include('product.cross_sell_form')
                            </div>
                            <!--Form End-->
                            <!--Table-->
                        @include('product.cross_sell_table')
                            <!--End table-->
                        </div>
                        <!--end-->

                        <!--Simple_Price_Tires Tab-->
                        <div role="tabpanel" class="tab-pane fade" id="Simple_Price_Tires">
                            <div class="m-t-b"><button id="product-btn" onclick="divToggle('Simple_Price_Tires-form-toggle')"
                                                       class="btn btn-primary pushme with-color">Add Price Tires</button></div>

                            <!--Form Start-->
                            <div class="d-none" id="Simple_Price_Tires-form-toggle">
                                @include('product.simple_price_form')
                            </div>
                            <!--Form End-->
                            <!--Table-->
                            @include('product.simple_price_table')
                            <!--End table-->
                        </div>
                        <!--end-->

                        <!-- End Main Tab Content-->
                    </div>
                </div>
            </div>
        </div>

    </div>
@endsection