<form class="m-t-b" method="post" action="contact.php" role="form">
    <div class="messages"></div>
    <div class="controls">
        <h3>Add Product Field Quantity</h3>
        <hr>
        <!--input Fields-->

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="step">Step</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="step" name="step">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="type">Type</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="type" name="type">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                    <p>

                        How this field affects the SVG.

                        Fill: fills the entire selected region the selected colour.
                        Outline Colour: changes the outline colour on the selected region.
                        Show Selected: shows the current data source item and hides the rest.
                        Toggle: turn on/off the selected region.
                        Pattern Fill: fills the entire selected region the pattern selected from the data source.
                        Pattern Transform: move and scale pattern.
                        Set Image: changes an SVG image to display the image currently selected by the customer. SVG
                        image required e.g.
                        <img x="40" y="40" width="50" height="50" transform="skewY(5)" />
                        Image Filter: Remove White Background: use with checkbox, apply to image to automatically
                        remove the white colour. Useful if customer has uploaded a JPEG image with a white
                        background.
                        Set Gradient Stop Colour: change the colour of the selected gradient colour. Use a CSS
                        selector in "IDs" fields e.g. #grad > stop:eq(0), #grad2 > stop:eq(0)
                        Set Font: used with a TextBox field, sets text in <text> elements in the selected region. To
                            center the text update the textPath like: <textPath text-anchor="middle" startOffset="50%">
                                Font Size: used with Dropdown or Tile Picker. Use data source item values such as 12pt,
                                or 12px.
                                Bold: used with Checkbox, sets selected text bold.
                                Set Text: used with a TextBox field, sets font in <text> elements in the selected region
                                    Transform: move and scale the selected region. Text elements need text-anchor="middle"
                                    so they scale from the center.
                                    Set Attribute: (advanced) change any SVG attribute to the selected value (e.g. use
                                    with a slider, data source item value).
                                    Set Overlay: changes the overlay to the selected dropdown item.
                                    Set 3D Model: Used like Set Overlay. Shows the selected 3D model from the data source.
                                    Put SVG in a Custom field.
                                </text></textPath></text></p>
                </div>
            </div>

        </div>
        <!---End-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" name="name" class="form-control" placeholder="Quantity"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="override_lable">Override Lable Text</label>
                    <input id="override_lable" type="number" name="override_lable" class="form-control"
                           placeholder="e.g Choose material" required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                    <p>Override field name text for client. If Name has a value use [[blank]] to hide it.</p>
                </div>
            </div>
        </div>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="discription">Description</label>
                    <input id="discription" type="text" name="discription" class="form-control" placeholder="Quantity"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="key">Key</label>
                    <input id="key" type="number" name="key" class="form-control"
                           placeholder="e.g Choose material" required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                    <p>This is used to link the customer's chosen option to other product designs e.g. shirt base colour ('c1') to shorts base colour ('c1').
                        This field is required as it provides a unique identifier in the basket for the design option.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="required">Required</label>
                    <!-- <input id="cart_p" type="text" name="cart_p" class="form-control" placeholder="Quantity"
required="required" data-error="Position required"> -->
                    <input type="checkbox" value="">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <!-- <div class="col-md-6">
<div class="form-group">
<label for="">Override Lable Text</label>
<input id="cart_p" type="number" name="cart_p" class="form-control"
placeholder="e.g Choose material" required="required" data-error="Position required">
<div class="help-block with-errors"></div>
</div>
</div> -->
        </div>
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="view_action">View Action</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="view_action" name="view_action">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                    <p>

                        How this field affects the SVG.

                        Fill: fills the entire selected region the selected colour.
                        Outline Colour: changes the outline colour on the selected region.
                        Show Selected: shows the current data source item and hides the rest.
                        Toggle: turn on/off the selected region.
                        Pattern Fill: fills the entire selected region the pattern selected from the data source.
                        Pattern Transform: move and scale pattern.
                        Set Image: changes an SVG image to display the image currently selected by the customer. SVG
                        image required e.g.
                        <img x="40" y="40" width="50" height="50" transform="skewY(5)" />
                        Image Filter: Remove White Background: use with checkbox, apply to image to automatically
                        remove the white colour. Useful if customer has uploaded a JPEG image with a white
                        background.
                        Set Gradient Stop Colour: change the colour of the selected gradient colour. Use a CSS
                        selector in "IDs" fields e.g. #grad > stop:eq(0), #grad2 > stop:eq(0)
                        Set Font: used with a TextBox field, sets text in <text> elements in the selected region. To
                            center the text update the textPath like: <textPath text-anchor="middle" startOffset="50%">
                                Font Size: used with Dropdown or Tile Picker. Use data source item values such as 12pt,
                                or 12px.
                                Bold: used with Checkbox, sets selected text bold.
                                Set Text: used with a TextBox field, sets font in <text> elements in the selected region
                                    Transform: move and scale the selected region. Text elements need text-anchor="middle"
                                    so they scale from the center.
                                    Set Attribute: (advanced) change any SVG attribute to the selected value (e.g. use
                                    with a slider, data source item value).
                                    Set Overlay: changes the overlay to the selected dropdown item.
                                    Set 3D Model: Used like Set Overlay. Shows the selected 3D model from the data source.
                                    Put SVG in a Custom field.
                                </text></textPath></text></p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="change_action">Change Action</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="change_action" name="change_action">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                    <p>What happens when the customer changes this field.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="step">Step</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="step" name="step">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="Dafault_value">Dafault Value</label>
                    <input id="Dafault_value" type="text" name="Dafault_value" class="form-control" placeholder="Quantity"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="min_value">Min Value</label>
                    <input id="min_value" type="number" name="min_value" class="form-control"
                           placeholder="e.g Choose material" required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="max_value">Max Value</label>
                    <input id="max_value" type="text" name="max_value" class="form-control" placeholder="Quantity"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="act_field">Act On Field</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="act_field" name="act_field">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                    <p>The view action will be applied to another field instead of this field.</p>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <!--Nav Tab-->
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active show" href="#SvgSnippet" role="tab" data-toggle="tab">product</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#IdToSvg" role="tab" data-toggle="tab">Category</a>
                    </li>
                </ul>
                <!--end-->
                <!--product_step tab-contant-->
                <div role="tabpanel" class="tab-pane fade" id="SvgSnippet">
                    ascascas
                    <!--Form Start-->
                    <div class="d-none" id="p-step-form">
                        <form class="m-t-b" method="post" action="contact.php" role="form">
                            <div class="messages"></div>
                            <div class="controls">
                                <h3>Add Step</h3>
                                <hr>
                                <!--input Fields-->

                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="text-muted"><strong>*</strong> These fields are required.</p>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!--Form End-->

                </div>
                <!--end-->
                <!--product_step tab-contant-->
                <div role="tabpanel" class="tab-pane fade" id="IdToSvg">
                    ascascas
                    <!--Form Start-->
                    <div class="d-none" id="p-step-form">
                        <form class="m-t-b" method="post" action="contact.php" role="form">
                            <div class="messages"></div>
                            <div class="controls">
                                <h3>Add Step</h3>
                                <hr>
                                <!--input Fields-->

                                <div class="row">
                                    <div class="col-md-12">
                                        <p class="text-muted"><strong>*</strong> These fields are required.</p>
                                    </div>
                                </div>
                            </div>

                        </form>
                    </div>
                    <!--Form End-->

                </div>
                <!--end-->
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="svg_render">SVG Render Order</label>
                    <input id="svg_render" type="text" name="svg_render" class="form-control" placeholder="Quantity"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                    <p>Controls the order the SVG snippets are output onto the final SVG in case one is covering another. The SVG is rendered in this order: Design, Fields, Data Source Items. A higher position means it is rendered later and is layered on top of the previous layers.</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="position">Position</label>
                    <input id="position" type="text" name="position" class="form-control"
                           placeholder="e.g Choose material" required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Save Step">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>