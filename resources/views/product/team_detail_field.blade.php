<form class="m-t-b" method="post" action="" role="form">
    <div class="messages"></div>
    <div class="controls">
        <h3>Add Team Detail Field</h3>
        <hr>
        <!--input Fields-->

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="step">Step</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="step" name="step">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="type">Type</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="type" name="type">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                    <p>

                        How this field affects the SVG.

                        Fill: fills the entire selected region the selected colour.
                        Outline Colour: changes the outline colour on the selected region.
                        Show Selected: shows the current data source item and hides the rest.
                        Toggle: turn on/off the selected region.
                        Pattern Fill: fills the entire selected region the pattern selected from the data source.
                        Pattern Transform: move and scale pattern.
                        Set Image: changes an SVG image to display the image currently selected by the customer. SVG
                        image required e.g.
                        <img x="40" y="40" width="50" height="50" transform="skewY(5)" />
                        Image Filter: Remove White Background: use with checkbox, apply to image to automatically
                        remove the white colour. Useful if customer has uploaded a JPEG image with a white
                        background.
                        Set Gradient Stop Colour: change the colour of the selected gradient colour. Use a CSS
                        selector in "IDs" fields e.g. #grad > stop:eq(0), #grad2 > stop:eq(0)
                        Set Font: used with a TextBox field, sets text in <text> elements in the selected region. To
                            center the text update the textPath like: <textPath text-anchor="middle" startOffset="50%">
                                Font Size: used with Dropdown or Tile Picker. Use data source item values such as 12pt,
                                or 12px.
                                Bold: used with Checkbox, sets selected text bold.
                                Set Text: used with a TextBox field, sets font in <text> elements in the selected region
                                    Transform: move and scale the selected region. Text elements need text-anchor="middle"
                                    so they scale from the center.
                                    Set Attribute: (advanced) change any SVG attribute to the selected value (e.g. use
                                    with a slider, data source item value).
                                    Set Overlay: changes the overlay to the selected dropdown item.
                                    Set 3D Model: Used like Set Overlay. Shows the selected 3D model from the data source.
                                    Put SVG in a Custom field.
                                </text></textPath></text></p>
                </div>
            </div>

        </div>
        <!---End-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" name="name" class="form-control" placeholder="Quantity"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="override_lable">Override Lable Text</label>
                    <input id="override_lable" type="number" name="override_lable" class="form-control"
                           placeholder="e.g Choose material" required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="description">Description</label>
                    <input id="description" type="text" name="description" class="form-control" placeholder="Quantity"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="key">Key</label>
                    <input id="key" type="number" name="key" class="form-control"
                           placeholder="e.g Choose material" required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="data_source">Data Source</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="data_source" name="data_source">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>

                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="data_source">Default</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="data_source" name="data_source">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="max_number_team">Max Number of Teams</label>
                    <input id="max_number_team" type="text" name="max_number_team" class="form-control" placeholder="Quantity"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                    <p>Default To 5</p>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="max_number_player">Max Number of Players Per Team</label>
                    <input id="max_number_player" type="number" name="max_number_player" class="form-control"
                           placeholder="e.g Choose material" required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                    <p>Default To 30</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="hide_quantities">Hide Quantities</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <input type="checkbox" id="hide_quantities" name="hide_quantities">
                    <div class="help-block with-errors"></div>

                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="hide_file">Hide File Upload</label>
                    <input type="checkbox" id="hide_file" name="hide_file">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="hide_term_player">Hide terms and Player Details Entry</label>
                    <input type="checkbox" id="hide_term_player" name="hide_term_player">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="show_spreadsheet">Show Spreadsheet Importer</label>
                    <input type="checkbox" id="show_spreadsheet" name="show_spreadsheet">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="show_slector">Show Slector</label>
                    <input type="checkbox" id="show_slector" name="show_slector">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="position">Position</label>
                    <input id="position" type="text" name="position" class="form-control" placeholder="Quantity"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Save Step">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>