<table id="tbl-Product" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%"
       role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Name: activate to sort column ascending" style="width: 30px;">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 150px;">Photo</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Product Name</th>
        <th class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Age: activate to sort column ascending" aria-sort="descending" style="width: 44px;">
            Category Name</th>
        <th class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Age: activate to sort column ascending" aria-sort="descending" style="width: 44px;">
            Position</th>
        <th class="sorting_desc" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Age: activate to sort column ascending" aria-sort="descending" style="width: 44px;">
            Action</th>

    </tr>
    </thead>
    <tfoot>
    <tr>
        <th rowspan="1" colspan="1">ID</th>
        <th rowspan="1" colspan="1">Photo</th>
        <th rowspan="1" colspan="1">Product Name</th>
        <th rowspan="1" colspan="1">Category Name</th>
        <th rowspan="1" colspan="1">Position</th>
        <th rowspan="1" colspan="1">Action</th>

    </tr>
    </tfoot>
    <tbody>
    @php($i = 1)
    @foreach($products as $product)
    <tr role="row" class="odd">
        <td class="">{{$i++}}</td>
        <td>
            {!! $product->threeD_model !!}
        </td>
        <td>{{ucfirst($product->name)}}</td>
        <td>{{$product->category->name}}</td>
        <td class="sorting_1">{{$product->position}}</td>
        <td>
            <a href="{{route('product.edit',$product->id)}}" class="btn btn-danger">Edit</a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>