<style>
    .tt{
        width: 100px;
        max-width: 100px;
        max-height: 100px;
    }
</style>
<table class="table table-responsive"  id="dataTable">
    <thead>
        <tr>
			<th>Symbol</th>
            <th>Name</th>
			<th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($prints as $prints)
        <tr>
            <td>
                <img class="tt" src="{{ asset('img/symbols/') }}/{!! $prints->symbol !!}">
            </td>
			<td>
				{!! $prints->name !!}
			</td>
            <td>
                {!! Form::open(['route' => ['symbols.destroy', $prints->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('symbols.show', [$prints->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
					<?php /*?>
					<a href="{!! route('symbols.edit', [$prints->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
					<?php */?>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>