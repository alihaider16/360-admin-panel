<!DOCTYPE html>
<html>
<head>
	<title>KIT DESIGN - PDF</title>
</head>
<body>
<h2 align="center">KIT DESIGN</h2>
<hr>
<div class="img-fluid" style="z-index: 3; cursor:pointer; position:absolute;"><img src="{!! $input['base'] !!}" class='col-12'></div>
<div class="img-fluid" style="z-index: 4; cursor:pointer; position:absolute;"><img src="{!! $input['overlay'] !!}" class='col-12'></div>
@if($input['frontTLFile']!='')
<div style="z-index:2; position:absolute; top:125px; left:110px; height:80px; width:80px;" id='frontTL'><img src="{!! $input['frontTLFile'] !!}" class='col-12'></div></div>
@endif
@if($input['frontTRFile']!='')
<div style="z-index:2; position:absolute; top:125px; left:245px; height:80px; width:80px;" id='frontTR'><img src="{!! $input['frontTRFile'] !!}" class='col-12'></div>
@endif
@if($input['frontBLFile']!='')
<div style="z-index:2; position:absolute; top:340px; left:100px; height:80px; width:80px;" id='frontBL'><img src="{!! $input['frontBLFile'] !!}" class='col-12'></div>
@endif
@if($input['frontBRFile']!='')
<div style="z-index:2; position:absolute; top:340px; left:257px; height:80px; width:80px;" id='frontBR'><img src="{!! $input['frontBRFile'] !!}" class='col-12'></div>
@endif
@if($input['backCenterFile']!='')
<div style="z-index:2; position:absolute; top:100px; left:536px; height:150px; width:150px;" id='backCenter'><img src="{!! $input['backCenterFile'] !!}" class='col-12'></div>
@endif
<div class="col-sm-12 col-12" style="z-index: 1; cursor:pointer;">{!! $product[0]->image !!}</div>
</body>
<script type="text/javascript">
var svgRect = document.getElementById("Layer_2");
<?php
if($input['baseColor']!='')
{
?>
var svgBody = svgRect.getElementById("layer-1");
svgBody.setAttribute("fill",{!! $input['baseColor'] !!});
<?php
}
if($input['layer1Color']!='')
{
?>
var svgLayer1 = svgRect.getElementById("layer-2");
svgLayer1.setAttribute("fill",{!! $input['layer1Color'] !!});
<?php
}
if($input['layer2Color']!='')
{
?>
var svgLayer2 = svgRect.getElementById("layer-3");
svgLayer2.setAttribute("fill",{!! $input['layer2Color'] !!});
<?php
}
if($input['accentColor']!='')
{
?>
var svgAccents = svgRect.getElementById("layer-4");
svgAccents.setAttribute("fill",{!! $input['accentColor'] !!});
<?php
}
?>
</script>
</html>