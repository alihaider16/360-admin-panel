   <!-- Scripts -->
   <script src="{{asset('js/scripts.js')}}"></script> <!-- Custom scripts -->
   <script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/owl.carousel.min.js"></script>
   <script src="https://kit.fontawesome.com/dd4338fa1b.js"></script>
   <script type="text/javascript">
      $('.owl-carousel').owlCarousel({
      loop: true,
      margin: 10,
      nav: false,
      navText: [
       "<img src='{{asset('images/owl-lef-arrow.png')}}' class='owl-arrow-1'>",
       "<img src='{{asset('images/owl-right-arrow.png')}}' class='owl-arrow-1'>"
      ],
      autoplay: true,
      autoplayHoverPause: true,
      responsive: {
       0: {
         items: 1
       },
       600: {
         items: 3
       },
       1000: {
         items: 5
       },
       1400: {
         items: 7
       }
      }
      })
   </script>