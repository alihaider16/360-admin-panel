		<title>Company</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<!-- Styles -->
		<link href="{{asset('css/bootstrap.css')}}" rel="stylesheet">
		<link href="{{asset('css/fontawesome-all.css')}}" rel="stylesheet">
		<link href="{{asset('css/front-styles.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="{{asset('frontend/inter-style/bootstrap-4.0.0/dist/css/bootstrap.min.css')}}">
		<link rel="stylesheet" href="{{asset('frontend/inter-style/style.css')}}">
		<link href="{{asset('frontend/css/home.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="{{asset('frontend/inter-style/responsive.css')}}">
		<!--<link href="{{ asset('/') }}/css/front-styles.css" rel="stylesheet">-->

		<!-- Favicon  -->
		<link rel="icon" href="{{asset('public/images/company.png')}}">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.1.3/assets/owl.carousel.min.css"/>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>