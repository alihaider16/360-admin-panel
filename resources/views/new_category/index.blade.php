@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Category</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container">

            <div class="m-t-b"><button id="product-btn" onclick="divToggle('product-form-toggle')"
                                       class="btn btn-primary pushme with-color">Add Category</button></div>

            <!--FormController Start-->
            <div class="d-none" id="product-form-toggle">
                @include('new_category.form')
            </div>
            <!--FormController End-->
            @include('new_category.table')

        </div>
    </div>
@endsection