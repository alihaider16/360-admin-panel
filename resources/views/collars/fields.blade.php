<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('name', 'Name:') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>

<!-- Name Field -->
<div class="form-group col-sm-12">
    {!! Form::label('category', 'Category:') !!}
    {!! Form::select('category', $cat, null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-12 image">
    {!! Form::label('collar', 'Collar:') !!}
    {!! Form::text('collar', null, ['class' => 'form-control image']) !!}
</div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
    <a href="{!! route('collars.index') !!}" class="btn btn-default">Cancel</a>
</div>
