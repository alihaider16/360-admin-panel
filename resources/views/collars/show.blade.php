@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            Collar Details
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    @include('collars.show_fields')
                    <a href="{!! route('collars.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
