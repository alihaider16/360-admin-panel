<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'Id:') !!}
    <p>{!! $prints->id !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $prints->name !!}</p>
</div>

<!-- Name Field -->
<div class="form-group">
    {!! Form::label('css', 'Logo CSS:') !!}
    <p>{!! $prints->css !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('image', 'Photo:') !!}
    <p>{!! $prints->image !!}</p>
    <img class="tt" src="{{ asset('img/templates/') }}/{!! $prints->image !!}">
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', 'Created:') !!}
    <p>{!! $prints->created_at !!}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated:') !!}
    <p>{!! $prints->updated_at !!}</p>
</div>

