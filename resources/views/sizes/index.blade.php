@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Sizes</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('sizes.create') !!}">Add new</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-responsive" id="dataTable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Shorthand</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($sizes as $size)
                        <tr>
                            <td>{{$size->name}}</td>
                            <td>
                                {{$size->shorthand}}
                            </td>
                            <td>
                                {!! Form::open(['route' => ['sizes.destroy', $size->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('sizes.show', [$size->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a href="{!! route('sizes.edit', [$size->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @empty
                        <td>No sizes added yet.</td>

                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

