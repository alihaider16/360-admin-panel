{!! Form::open(['route' => 'DataSource.colours']) !!}
<div class="row">
    <div class="form-group">
        <div class="col-md-6">
            <label for="">Text</label>
            <input type="text" class="form-control" name="text">
        </div>
        <div class="col-md-6">
            <label for="">Value</label>
            <input type="text" class="form-control" name="value">
            <input type="hidden" value="{{$dataSrc->id}}" name="id">
        </div>
    </div>
    <div class="col-md-6">
        <button type="submit" class="btn btn-success" style="margin-top: 10px;">Submit</button>
    </div>
</div>
{!! Form::close() !!}