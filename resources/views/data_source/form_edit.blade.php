<form class="m-t-b" method="post" action="{{route('dataSource.update',$dataSrc->id)}}" role="form">
    @csrf
    <div class="messages"></div>

    <div class="controls">
        <h3>Add Data Source</h3>
        <hr>
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" name="name" class="form-control" value="{{$dataSrc->name}}"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="type">type</label>
                    <select class="form-control" id="type" name="type">
                        <option value="{{$dataSrc->type}}">{{$dataSrc->type}}</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!--input Fields-->

        <div class="row">

            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Update">
                <a href="{{route('dataSource.index')}}" class="btn btn-danger">Back</a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>