<table id="tbl-Product" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%"
       role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Name: activate to sort column ascending" style="width: 30px;">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 100px;">DataSource Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Type
        </th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Action
        </th>

    </tr>
    </thead>
    <tfoot>
    <tr>
        <th rowspan="1" colspan="1">ID</th>
        <th rowspan="1" colspan="1">DataSource Name</th>
        <th rowspan="1" colspan="1">Type</th>
        <th rowspan="1" colspan="1">Action</th>

    </tr>
    </tfoot>
    <tbody>
    @php($i = 1)
    @foreach($dataSrc as $data)
    <tr role="row" class="odd">
        <td>{{$i++}}</td>
        <td>{{$data->name}}</td>
        <td>{{$data->type}}</td>
        <td>
            <a href="{{route('dataSource.edit',$data->id)}}" class="btn btn-danger btn-sm">Edit</a>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>