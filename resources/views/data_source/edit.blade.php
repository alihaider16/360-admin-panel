@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">DataSource</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="container">

            <!--FormController Start-->
            <div class="d-show" id="product-form-toggle">
                @include('data_source.form_edit')
            </div>
            <!--FormController End-->
            <div class="m-t-b"><button id="product-btn" onclick="divToggle('product-form-edit-item')"
                                       class="btn btn-primary pushmee with-color">Add Item</button>
            </div>
            <div class="d-none" id="product-form-edit-item">
                @include('data_source.form_edit_item')
            </div>
            <!--Table-->
        @include('data_source.edit_item_table')
        <!--End table-->

        </div>

    </div>

@endsection