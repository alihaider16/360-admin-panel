<table class="table table-responsive" id="dataTable">
    <thead>
    <tr>
        <th>ID</th>
        <th>Text</th>
        <th>Value</th>
        <th>Thumb</th>
    </tr>
    </thead>
    <tbody>
    @php($i = 1)
    @foreach($colours as $colour)
    <tr>
        <td>{{$i++}}</td>
        <td>{{$colour->name}}</td>
        <td>{{$colour->color}}</td>
        <td>
            <span style="background-color: {{$colour->color}}; padding: 10px 10px 5px 20px;"></span>
        </td>
    </tr>
    @endforeach
    </tbody>
</table>