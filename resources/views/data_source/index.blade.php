@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">DataSource</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="container">

            <div class="m-t-b"><button id="product-btn" onclick="divToggle('product-form-toggle')"
                                       class="btn btn-primary pushme with-color">Add Data Source</button></div>

            <!--FormController Start-->
            <div class="d-none" id="product-form-toggle">
                @include('data_source.form')
            </div>
            <!--FormController End-->
            <!--Table-->
            @include('data_source.table')
            <!--End table-->

        </div>

    </div>

@endsection