<?php
$homeNav = '';
$productNav = '';
$designLabNav = ' active';
$teamNav = '';
$orderNav = '';
$faqsNav = '';
$contactNav = '';
?>
<!doctype html>
<html lang="en">
	<head>
		@include('layouts.front_header')
		<!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">-->
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"> 
		<link rel="stylesheet" href="{{asset('frontend/inter-style/style.css')}}">
		<link href="{{asset('frontend/css/home.css')}}" rel="stylesheet">
		<link rel="stylesheet" href="{{asset('frontend/inter-style/responsive.css')}}">

		<script src="{{asset('js/configuratorScript.js')}}"></script>
		<script src="{{asset('js/jquery.form.js')}}"></script>
    
    <style>
       
        svg {
            width: 102%;
            height: auto;
        }

        .form {
            display: block;
            margin: 20px auto;
            background: #eee;
            border-radius: 10px;
            padding: 15px;
        }

         .progress {
            padding: 30px;
            position: relative;
            width: 100%;
            border: 1px solid #ddd;
            padding: 1px;
            border-radius: 3px;
        } 

        .bar {
            background-color: #B4F5B4;
            width: 0%;
            height: 50px;
            border-radius: 3px;
        }

        .percent {
            font-weight: bolder;
            font-size: 1.5em;
            position: absolute;
            display: inline-block;
            top: 25%;
            left: 48%;
        }

        .card {
            background: #fff;
            padding: 3em;
            line-height: 1.5em;
            box-shadow: 1px 1px 3px rgba(0, 0, 0, 0.1);
        }   

        [hidden] {
            display: none !important;
        }
        .current-bg-1 {
            background-image:url(../images/StepBox-light-gray.png) !important;
            background-repeat: no-repeat;
            color:white;
            padding-top: 10px;
            padding-bottom: 20px; 
            height: 40px;
            display: inline-block;
        }
        .current-bg-2 {
            background-image:url(../images/StepBox-light-gray.png) !important;
            background-repeat: no-repeat;
            color:white;
            padding-top: 10px;
            padding-bottom: 20px; 
            height: 40px;
            display: inline-block;
        }
        .current-bg-3 {
            background-image:url(../images/StepBox-light-gray-End.png) !important;
            background-repeat: no-repeat;
            color:white;
            padding-top: 10px;
            padding-bottom: 20px; 
            height: 40px;
            display: inline-block;
        }
        li.selected {
            background-image:url(../images/akumaglass.png);
            color:white;
            transition: .5;
        }

        .on {
            stroke: red;
            stroke-width: 2;
        }
        /*STYLE*/
        ol {
          /* border-right: 1px solid rgba(78, 68, 50, 0.05); */
          float: left;
          list-style-type: decimal;
          margin: 1em;
          margin-left: 0;
          padding: 1em;
          padding-right: 2em;
        }
         ol.styled {
          counter-reset: item;
          list-style: none !important;
        }
        ol.styled li {
          background: rgba(78, 68, 50, 0.1);
          border-radius: 4px;
          /* font-family: "Helvetica Neue", Helvetica, Arial, sans-serif; */
          font-size:16px;
          line-height: 2;
          margin-bottom: 10px;
          width: 182px;
          cursor: pointer;
        }
        ol.styled li:before {
          /* content: '&#x25CB;' counter(item, decimal) ''; */
          /* content: '';
          /* background: #4e443c;
          border-radius: 4px; */
          color: #f7f7ef;
          font-size: 15px;
          margin: 0 5px 0 8px;
          padding: 4px 7px; 
        }
        ol.styled li span{
          padding: 10px;
        }
        span.add-color{
          background-color: rgb(123,193,67);
            /* padding: 9px !important; */
            /* padding-left: 98px !important; */
            margin-left: -22px;
            /* padding-top: 14px !important;
            padding-bottom: 10px !important;
            padding-right: 56px !important;
            padding-left: 55px !important; */
            padding: 10px 99px 10px 10px!important;
            color: white;
        }
        span.add-design{
          background-color: rgb(123,193,67);
            /* padding: 9px !important; */
            /* padding-left: 98px !important; */
            margin-left: -22px;
            /* padding-top: 14px !important;
            padding-bottom: 10px !important;
            padding-right: 56px !important;
            padding-left: 55px !important; */
            padding: 10px 89px 10px 10px!important;
            color: white;
        }
        label.menu-label {
         
          color: #666;
          font-size: 18px;
          cursor: pointer;
          padding: 0.8em 1em 0.8em 0;
          padding-top: 0px !important;
            margin-bottom: 0px !important;
            padding-bottom: 0px !important;
        }
        label.menu-label:hover {
          color:rgb(83,83,83);
        }
        label.menu-label:hover:before {
          /* background: rgb(83,83,83); */
          background-color: lightgray;
        }
        label.menu-label:before {
          font-family: 'Impact';
          text-align: center;
          content: counter(li);
          border: 10px solid rgb(123,193,67);
          border-radius: 40px;
          display: inline-block;
          width: 60px;
          height: 60px;
          line-height: 40px;
          font-size: 24px;
          margin: 0 0.5em 0 0;
          background: white;
          color: rgb(83,83,83);
        }
        .css-accordion {
          list-style-type: none;
          counter-reset: li;
          
          /*padding: 20px;
          margin: 20px;*/
         /* width: 250px;*/
          
          /*---- End .accordion-item ----*/
        }
        .css-accordion .accordion-item {
          counter-increment: li;
          padding: 0;
          margin: 0;
        }
        .css-accordion .accordion-item .item-content-container {
          border-left: 10px solid rgb(123,193,67);
          padding: 6px 0;
          margin: -2px 0 -2px 25px;
        }
        .css-accordion .accordion-item .item-content-container .item-content {
          /* background: #eee; */
          padding:5px auto;
          overflow: hidden;
          margin: 0 0 0 0px;
          /* border-radius: 2px;
          box-shadow: inset 0 2px 8px rgba(0, 0, 0, 0.5), 0 1px 2px rgba(255, 255, 255, 0.9); */
        }
        .css-accordion .accordion-item .item-content-container .item-content p {
          margin: 0.5em 0;
          font-size: 14px;
          text-shadow: 0 1px 1px rgba(255, 255, 255, 0.9);
        }
        .css-accordion .accordion-item input[type=radio] {
          display: none;
          /*---- End &:checked ----*/
        }
        .css-accordion .accordion-item input[type=radio] ~ .item-content-container {
          overflow: hidden;
        }
        .css-accordion .accordion-item input[type=radio] ~ .item-content-container .item-content {
          height: 0;
          transition: all 0.3s linear;
        }
        .css-accordion .accordion-item input[type=radio]:checked ~ .item-content-container {
          height: auto;
          overflow: visible;
        }
        .css-accordion .accordion-item input[type=radio]:checked ~ .item-content-container .item-content {
          height:auto;
          /* overflow-y: auto; */
          transition: all 0.5s linear;
        }
        .css-accordion .accordion-item input[type=radio]:checked + label {
          /* color: purple; */
          
        }
        .css-accordion .accordion-item input[type=radio]:checked + label:before {
          background:rgb(83,83,83);
          color:white;
        }
        .current-step-1{
        	background-image:url(../../images/StepBox-Green.png) !important;
        	/* background-color:red; */
        }
        .step-container{
          height:100vh;
           overflow:hidden;
        }

        .info{
          display: none
        }
        .model-content-7 {
            padding: 0px !important;
            width: 100% !important;
            border: none;
            border-radius: 0px;
            -webkit-box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.75);
            box-shadow: 0px 2px 6px 0px rgba(0, 0, 0, 0.75);
        }
        .mymodal7 {
          position: absolute;
          z-index: 9999;
          max-width: 23%;
          left: -140px;
          top: 60px;
        }
        @media(max-width: 800px){
          .info-acord{
            display: none
          }
          .info{
            display: block
          }
        }

    </style>
	</head>
	<body>
		@include('layouts.front_nav')
		<!-- end navbar container -->
      	<div class="container px-0" style="overflow:hidden;">
         <!-- Haeder -->
         <header class="header" id="header">
            <div class="row no-gutters">
                <div class="col-12 col-sm-5">
                    <h1 class="mrg" style="margin-bottom: 0px;">KIT DESIGNER</h1>
                </div>
                <div class="col-7 d-none d-sm-block">
                    <ul class="process-step">
                        <li style="z-index:3;" class="complete" id="step-1">Step 1<span>Select Design</span></li>
                        <li style="z-index:2;" class="" id="step-2">Step 2<span>Edit Design</span></li>
                        <li style="z-index:1;" class="" id="step-3">Step 3<span>Completed Design</span></li>
                    </ul>
                </div>
            </div>
         </header>
         </div>
         
         <section  style="width: 100%;">
            <div class="container step-container"  style="overflow-y: hidden; height: auto; margin-bottom: 10px">
                <div class="row" style="overflow-y: hidden; height: auto; margin-bottom: 10px">

                      <!--Left Column (Accordian, Price, & Desription)-->
                      <div class="col-md-3 info-acord">
                        <div class="row ">

                          <!---LEFT MENU START-->

                            <ol class="css-accordion user-journey">
                                <li class="accordion-item stage-1">
                                  <input type="radio" name="accordion-control" id="stage-1-control" checked>
                                  <label class="menu-label" for="stage-1-control"><span class="add-color">Add Color</span></label>
                                  <div class="item-content-container">
                                    <div class="item-content">
                                        <ol id="layers" class="prefixed styled">
                                            <?php $i=1;?>
                                            @foreach($layers as $layer)
                                            <li class="add-color-<?=$i?>"><span>&#9675</span>{!! $layer->layer_title !!}</li><?php $i++;?>
                                            @endforeach
                                            <?php if($embs != '')
                                            {
                                            ?>
                                            <li class="add-color--1"><span>&#9675</span>Embelishment</li>
                                            <?php
                                            }?>
                                        </ol>
                                    </div>
                                  </div>
                                </li>
                                <li class="accordion-item stage-2">
                                  <input type="radio" name="accordion-control" id="stage-2-control">
                                  <label class="menu-label" for="stage-2-control"><span class="add-design">Add Design</span></label>
                                  <div class="item-content-container">
                                    <div class="item-content">
                                        <ol class="prefixed styled">
                                            <li id="one-my-design"><span>&#9675</span>Front Top Right</li>
                                            <li id="two-my-design"><span>&#9675</span>Front Top Left</li>
                                            <li id="three-my-design"><span>&#9675</span>Front Bottom Right</li>
                                            <li id="four-my-design"><span>&#9675</span>Front Bottom Left</li>
                                            <li id="five-my-design"><span>&#9675</span>Back Center</li>
                                        </ol>
                                    </div>
                                  </div>
                                </li>
                                <li class="accordion-item stage-3">
                                  <input type="radio" name="accordion-control" id="stage-4-control">
                                  <label class="menu-label" for="stage-4-control"><span class="add-design" id="saveDesign">Save Design</span></label>
                                </li>
                            </ol>

                          <!-- -LEFT MENU END -->
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input id="base" name="base" type="hidden" value="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->base !!}_base.png">
                            <input id="overlay" name="overlay" type="hidden" value="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->overlay !!}_overlay.png">
                            <input id="svgData" name="svgData" type="hidden" value="{!! $_REQUEST['designID'] !!}">

                            <input id="baseColor" name="baseColor" type="hidden">
                            <input id="layer1Color" name="layer1Color" type="hidden">
                            <input id="layer2Color" name="layer2Color" type="hidden">
                            <input id="accentColor" name="accentColor" type="hidden">

                            <input id="frontTRFile" name="frontTRFile" type="hidden">
                            <input id="frontTLFile" name="frontTLFile" type="hidden">
                            <input id="frontBRFile" name="frontBRFile" type="hidden">
                            <input id="frontBLFile" name="frontBLFile" type="hidden">
                            <input id="backCenterFile" name="backCenterFile" type="hidden">
                        </div>

                        <div class="row">
                          <hr class="col-12">
                          <div class="col-12" style="text-align: justify;"><span><strong>Price:</strong> ${!! $products[0]->price !!}</span><br><span><strong>Description:</strong> {!! $products[0]->description !!}</span></div>
                        </div>
                      </div>

                      <!-- Right Column Shirt Canvas -->
                      <div class="col-md-9">
                          <div class="container">
                          <br>
                          <!---------------------------Front Top Right--------------------------->
                          <div class="row">
                            <div class="col-sm-6 col-6" style="cursor:pointer;">
                                <div class="modal1" id="myModal1-1" style="display:none; z-index: 9999;">
                                  <div class="modal-dialog">
                                    <div class="modal-content model-content-1">
                                    <!-- Modal Header -->
                                    <div class="modal-header">
                                      <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                      <button type="button" id="close1-1" data-dismiss="modal">×</button>
                                    </div>
                                    <!-- Modal body -->
                                    <div class="modal-body">
                                      <div class="row no-gutters" style="margin-bottom: 5px;">
                                        <div class="col-6"><button type="button" class="btn-model-design btn-5 btn-light float-left mr-1">0-9</button></div>
                                        <div class="col-6">
                                            <button type="button" class="btn-model-design btn-6 btn-light float-left">Text</button>
                                        </div>
                                      </div>
                                        <div class="row no-gutters">
                                            <div class="col">
                                                <button type="button" class="btn-model-design btn-1 btn-light float-left mr-1">A-Z</button> 
                                            </div>
                                            <div class="col">
                                                <button type="button" class="btn-model-design btn-2 btn-light float-left">Symbol</button>
                                            </div>                           
                                        </div>
                                      <div class="row no-gutters" style="margin-top: 5px;margin-bottom: 5px;">
                                        <div class="col-6"><button type="button" class="btn-model-design btn-3 btn-light float-left mr-1">Editable</button></div>
                                        <div class="col-6">
                                            <button type="button" class="btn-model-design btn-4 btn-light float-left">Upload</button>
                                        </div>
                                      </div>
                                      <hr>
                                        <div class="one-one" style="display:none;">
                                          <div class="container">
                                            <div class="row mt-3">
                                            <?php $i=0;?>
                                            @foreach($alphabets as $alphabet)
                                            <div class="col-sm-3 col-3"><a onclick="addFrontTR('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontTR')"><img src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a></div>
                                            @endforeach
                                            </div>
                                          </div>
                                          <hr>
                                            <ul class="nav nav-tabs" role="tablist">
                                              <li class="nav-item">
                                                <a class="nav-link active" href="#profile1" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#buzz1" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#div1" role="tab" data-toggle="tab" id="border2"><span style="color: #454545;"><i class="fas fa-circle"></i><span></a>
                                              </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                              <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile1">
                                                @foreach($colors as $color)
                                                  <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha1','frontTR')">
                                                  <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                  <?php $i++?>
                                                @endforeach                   
                                              </div>
                                              <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1">
                                                @foreach($colors as $color)
                                                  <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha2','frontTR')">
                                                  <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                  <?php $i++?>
                                                @endforeach
                                              </div>
                                              <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="div1">
                                                @foreach($colors as $color)
                                                  <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha3','frontTR')">
                                                  <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                  <?php $i++?>
                                                @endforeach
                                              </div>
                                            </div>
                                        </div>
                                        <div class="two-one" style="display:none;">
                                        <div class="row">
                                        @foreach($symbols as $symbol)
                                          <div class="col-sm-4 col-4"><a onclick="addFrontTR('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','frontTR')"><img src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a></div>
                                        @endforeach
                                        </div>
                                        <hr>
                                            <ul class="nav nav-tabs" role="tablist">
                                              <li class="nav-item">
                                                <a class="nav-link active" href="#profile1-1" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#buzz1-1" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                              </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                              <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile1-1">
                                                  @foreach($colors as $color)
                                                    <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','frontTR')">
                                                    <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                    <?php $i++?>
                                                  @endforeach                   
                                              </div>
                                              <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1-1">
                                                @foreach($colors as $color)
                                                  <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','frontTR')">
                                                  <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                  <?php $i++?>
                                                @endforeach
                                              </div>
                                            </div>
                                        </div>
                                        <div class="three-one" style="display:none;">
                                          <div class="row" style="padding-left: 10px; padding-right: 10px;">Text (Max 12 characters)</div>
                                          <div class="row" style="padding-left: 10px; padding-right: 10px;"><input type="text" id="editable_text1" class="col-12" onchange="addText(this.value,'frontTR','text-align: center;')" maxlength="12"></div>
                                          <div class="row" style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                            <select id="fonts" class="col-12">
                                              <option>Select Font</option>
                                              @foreach($fonts as $font)
                                              <option value="{!! $font->id !!}">{!! $font->name !!}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div class="row">
                                        @foreach($elogos as $elogo)
                                          <div class="col-sm-4 col-4"><a onclick="addFrontTR('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','frontTR')"><img src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a></div>
                                        @endforeach
                                        </div>
                                        <hr>
                                            <ul class="nav nav-tabs" role="tablist">
                                              <li class="nav-item">
                                                <a class="nav-link active" href="#profile1-2" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#buzz1-2" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                              </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                              <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile1-2">
                                                  @foreach($colors as $color)
                                                    <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','frontTR')">
                                                    <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                    <?php $i++?>
                                                  @endforeach
                                              </div>
                                              <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1-2">
                                                @foreach($colors as $color)
                                                  <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','frontTR')">
                                                  <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                  <?php $i++?>
                                                @endforeach
                                              </div>
                                            </div>
                                        </div>
                                        <div class="four-one" style="display:none;">
                                        <h3>Select an image file</h3>
                                        <form class="form form-horizontal" id="custom1" method="post" action="{{ route('step1-addColor.uploade') }}" enctype="multipart/form-data">
                                          @csrf
                                          <div style="position:relative;">
                                            <a class='btn btn-primary' href='javascript:;'>
                                              Select file ...
                                              <input required id="apkfile" name="myfile" accept="image/x-svg" type="file"
                                                  style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                                  name="file_source" size="40">
                                            </a>
                                          </div>
                                          <br>
                                          <input class="form-control btn btn-warning" id="upload" type="submit" value="Upload file to server">
                                        </form>
    
                                        <div style="height: 50px;" class="progress">
                                          <div class="bar"></div>
                                          <div class="percent">0%</div>
                                        </div>
                                        </div>
                                        <div class="three-one" style="display:none;">
                                          <div class="row" style="padding-left: 10px; padding-right: 10px;">Text (Max 12 characters)</div>
                                          <div class="row" style="padding-left: 10px; padding-right: 10px;"><input type="text" id="editable_text1" class="col-12" onchange="addText(this.value,'frontTR','text-align: center;')" maxlength="12"></div>
                                          <div class="row" style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                            <select id="fonts" class="col-12">
                                              <option>Select Font</option>
                                              @foreach($fonts as $font)
                                              <option value="{!! $font->id !!}">{!! $font->name !!}</option>
                                              @endforeach
                                            </select>
                                          </div>
                                          <div class="row">
                                        @foreach($elogos as $elogo)
                                          <div class="col-sm-4 col-4"><a onclick="addFrontTR('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','frontTR')"><img src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a></div>
                                        @endforeach
                                        </div>
                                        <hr>
                                            <ul class="nav nav-tabs" role="tablist">
                                              <li class="nav-item">
                                                <a class="nav-link active" href="#profile1-2" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                              </li>
                                              <li class="nav-item">
                                                <a class="nav-link" href="#buzz1-2" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                              </li>
                                            </ul>
                                            <!-- Tab panes -->
                                            <div class="tab-content">
                                              <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile1-2">
                                                  @foreach($colors as $color)
                                                    <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','frontTR')">
                                                    <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                    <?php $i++?>
                                                  @endforeach
                                              </div>
                                              <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz1-2">
                                                @foreach($colors as $color)
                                                  <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','frontTR')">
                                                  <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                  <?php $i++?>
                                                @endforeach
                                              </div>
                                            </div>
                                        </div>
                                      </div>
                                    </div>
                            	</div>
                            </div>
                            <?php /**/?>
                            <div class="col-sm-6 col-6" style="cursor:pointer;">
                                <div class="modal1" id="myModal1-1-1" style="display:none; z-index: 9999;">
                                	<div class="modal-dialog">
                                		<div class="modal-content model-content-1">
                                            <!-- Modal Header -->
                                            <div class="modal-header">
                                                <h6 class="modal-title modal-color">Preview</h6>
                                                <button type="button" id="close1-1" data-dismiss="modal">×</button>
                                            </div>
                                            <!-- Modal body -->
                                            <div class="modal-body" id="preview-1-1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php /**/?>
                          </div>
                          <!---------------------------Front Top Left--------------------------->
                          <div class="row">
                            <div class="col-sm-6 col-12" style="cursor:pointer;">
                            <div class="modal1" id="myModal1-2" style="display:none; z-index: 9999;">
                              <div class="modal-dialog">
                                <div class="modal-content model-content-1">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                  <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                  <button type="button" id="close1-2" data-dismiss="modal">×</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body">
                                  <div class="row no-gutters" > 
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-1 btn-light float-left mr-1">A-Z</button>
                                    
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-2 btn-light float-left">Symbol</button>
                                    </div>
                                  </div>
                                  <div class="row no-gutters" style="margin-top:10px;">
                                  <div class="col-6">
                                  <button type="button" class="btn-model-design btn-3 btn-light float-left mr-1">Editable</button>
                                  </div>
                                  <div class="col-6">
                                  <button type="button" class="btn-model-design btn-4 btn-light float-left">Upload</button>
                                  </div>
                                  </div>
                                  <hr>
                                    <div class="one-one" style="display:none;">
                                      <div class="container">
                                        <div class="row mt-3">
                                        @foreach($alphabets as $alphabet)
                                        <div class="col-sm-3 col-3"><a onclick="addFrontTL('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontTL')"><img src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a></div>
                                        @endforeach
                                        </div>
                                      </div>
                                      <hr>
                                      <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile2" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz2" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span><span></a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile2">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha1','frontTL')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz2">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha2','frontTL')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="two-one" style="display:none;">
                                    <div class="row">
                                    @foreach($symbols as $symbol)
                                      <div class="col-sm-4 col-4"><a onclick="addFrontTL('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','frontTL')"><img src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a></div>
                                    @endforeach
                                    </div>
                                    <hr>
                                      <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile2-1" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz2-1" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span><span></a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile2-1">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','frontTL')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz2-1">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','frontTL')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="three-one" style="display:none;">
                                      <div class="row" style="padding-left: 10px; padding-right: 10px;">Text (Max 12 characters)</div>
                                      <div class="row" style="padding-left: 10px; padding-right: 10px;"><input type="text" id="editable_text1" class="col-12" onchange="addText(this.value,'frontTL','text-align: center;')" maxlength="12"></div>
                                      <div class="row" style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                        <select id="fonts" class="col-12">
                                          <option>Select Font</option>
                                          @foreach($fonts as $font)
                                          <option value="{!! $font->id !!}">{!! $font->name !!}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                      <div class="row">
                                    @foreach($elogos as $elogo)
                                      <div class="col-sm-4 col-4"><a onclick="addFrontTL('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','frontTL')"><img src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a></div>
                                    @endforeach
                                    </div>
                                    <hr>
                                      <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile2-2" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz2-2" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span><span></a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile2-2">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','frontTL')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz2-2">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','frontTL')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="four-one" style="display:none;">
                                      <h3>Select an image file</h3>
                                    <form class="form form-horizontal" id="custom2" method="post" action="{{ route('step1-addColor.uploade') }}" enctype="multipart/form-data">
                                      @csrf
                                      <div style="position:relative;">
                                        <a class='btn btn-primary' href='javascript:;'>
                                          Select file ...
                                          <input required id="apkfile" name="myfile" accept="image/x-svg" type="file"
                                              style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                              name="file_source" size="40">
                                        </a>
                                      </div>
                                      <br>
                                      <input class="form-control btn btn-warning" id="upload" type="submit" value="Upload file to server">
                                    </form>

                                    <div style="height: 50px;" class="progress">
                                      <div class="bar"></div>
                                      <div class="percent">0%</div>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <!---------------------------Front Bottom Right--------------------------->
                          <div class="row">
                            <div class="col-sm-6 col-12" style="cursor:pointer;">
                            <div class="modal1" id="myModal1-3" style="display:none; z-index: 9999;">
                              <div class="modal-dialog">
                                <div class="modal-content model-content-1">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                  <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                  <button type="button" id="close1-3" data-dismiss="modal">×</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body">
                                  <div class="row no-gutters">
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-1 btn-light float-left mr-1">A-Z</button>
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-2 btn-light float-left">Symbol</button>
                                    </div>
                                  </div>
                                  <div class="row no-gutters" style="margin-top:10px;">
                                  <div class="col-6">
                                  <button type="button" class="btn-model-design btn-3 btn-light float-left mr-1">Editable</button>									   
                                  </div>
                                  <div class="col-6">
                                  <button type="button" class="btn-model-design btn-4 btn-light float-left">Upload</button>
                                  </div>
                                  </div>
                                  <hr>
                                    <div class="one-one" style="display:none;">
                                      <div class="container">
                                        <div class="row mt-3">
                                        @foreach($alphabets as $alphabet)
                                        <div class="col-sm-3 col-3"><a onclick="addFrontBR('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontBR')"><img src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a></div>
                                        @endforeach
                                        </div>
                                      </div>
                                      <hr>
                                      <!----------------------------------------------------->
                                      <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile3" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz3" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i></span></a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile3">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha1','frontBR')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz3">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha2','frontBR')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="two-one" style="display:none;">
                                    <div class="row">
                                    @foreach($symbols as $symbol)
                                      <div class="col-sm-4 col-4"><a onclick="addFrontBR('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','frontBR')"><img src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a></div>
                                    @endforeach
                                    </div>
                                    <hr>
                                      <!----------------------------------------------------->
                                      <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile3-1" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz3-1" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i></span></a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile3-1">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','frontBR')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz3-1">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','frontBR')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="three-one" style="display:none;">
                                      <div class="row" style="padding-left: 10px; padding-right: 10px;">Text (Max 12 characters)</div>
                                      <div class="row" style="padding-left: 10px; padding-right: 10px;"><input type="text" id="editable_text1" class="col-12" onchange="addText(this.value,'frontBR','text-align: center;')" maxlength="12"></div>
                                      <div class="row" style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                        <select id="fonts" class="col-12">
                                          <option>Select Font</option>
                                          @foreach($fonts as $font)
                                          <option value="{!! $font->id !!}">{!! $font->name !!}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                      <div class="row">
                                    @foreach($elogos as $elogo)
                                      <div class="col-sm-4 col-4"><a onclick="addFrontBR('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','frontBR')"><img src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a></div>
                                    @endforeach
                                    </div>
                                    <hr>
                                      <!----------------------------------------------------->
                                      <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile3-2" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz3-2" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i></span></a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile3-2">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','frontBR')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz3-2">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','frontBR')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="four-one" style="display:none;">
                                      <h3>Select an image file</h3>
                                    <form class="form form-horizontal" id="custom3" method="post" action="{{ route('step1-addColor.uploade') }}" enctype="multipart/form-data">
                                      @csrf
                                      <div style="position:relative;">
                                        <a class='btn btn-primary' href='javascript:;'>
                                          Select file ...
                                          <input required id="apkfile" name="myfile" accept="image/x-svg" type="file"
                                              style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                              name="file_source" size="40">
                                        </a>
                                      </div>
                                      <br>
                                      <input class="form-control btn btn-warning" id="upload" type="submit" value="Upload file to server">
                                    </form>

                                    <div style="height: 50px;" class="progress">
                                      <div class="bar"></div>
                                      <div class="percent">0%</div>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div> 
                          <!---------------------------Front Bottom Left--------------------------->
                          <div class="row">
                            <div class="col-sm-6 col-12" style="cursor:pointer;">
                            <div class="modal1" id="myModal1-4" style="display:none; z-index: 9999;">
                              <div class="modal-dialog">
                                <div class="modal-content model-content-1">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                  <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                  <button type="button" id="close1-4" data-dismiss="modal">×</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body">
                                  <div class="row no-gutters">
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-1 btn-light float-left mr-1">A-Z</button>
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-2 btn-light float-left">Symbol</button>
                                    </div>
                                  </div>
                                  <div class="row no-gutters" style="margin-top:10px;">
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-3 btn-light float-left mr-1">Editable</button>
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-4 btn-light float-left">Upload</button>
                                    </div>
                                  </div>
                                  <hr>
                                    <div class="one-one" style="display:none;">
                                      <div class="container">
                                        <div class="row mt-3">
                                        @foreach($alphabets as $alphabet)
                                        <div class="col-sm-3 col-3"><a onclick="addFrontBL('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','frontBL')"><img src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a></div>
                                        @endforeach
                                        </div>
                                      </div>
                                      <hr>
                                      <!----------------------------------------------------->
                                      <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile4" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz4" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile4">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha1','frontBL')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz4">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha2','frontBL')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="two-one" style="display:none;">
                                    <div class="row">
                                    @foreach($symbols as $symbol)
                                      <div class="col-sm-4 col-4"><a onclick="addFrontBL('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','frontBL')"><img src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a></div>
                                    @endforeach
                                    </div>
                                    <hr>
                                      <!----------------------------------------------------->
                                      <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile4-1" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz4-1" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile4-1">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','frontBL')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz4-1">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','frontBL')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="three-one" style="display:none;">
                                      <div class="row" style="padding-left: 10px; padding-right: 10px;">Text (Max 12 characters)</div>
                                      <div class="row" style="padding-left: 10px; padding-right: 10px;"><input type="text" id="editable_text1" class="col-12" onchange="addText(this.value,'frontBL','text-align: center;')" maxlength="12"></div>
                                      <div class="row" style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                        <select id="fonts" class="col-12">
                                          <option>Select Font</option>
                                          @foreach($fonts as $font)
                                          <option value="{!! $font->id !!}">{!! $font->name !!}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                      <div class="row">
                                    @foreach($elogos as $elogo)
                                      <div class="col-sm-4 col-4"><a onclick="addFrontBL('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','frontBL')"><img src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a></div>
                                    @endforeach
                                    </div>
                                    <hr>
                                      <!----------------------------------------------------->
                                      <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile4-2" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz4-2" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                        </ul>

                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile4-2">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','frontBL')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz4-2">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','frontBL')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="four-one" style="display:none;">
                                      <h3>Select an image file</h3>
                                    <form class="form form-horizontal" id="custom4" method="post" action="{{ route('step1-addColor.uploade') }}" enctype="multipart/form-data">
                                      @csrf
                                      <div style="position:relative;">
                                        <a class='btn btn-primary' href='javascript:;'>
                                          Select file ...
                                          <input required id="apkfile" name="myfile" accept="image/x-svg" type="file"
                                              style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                              name="file_source" size="40">
                                        </a>
                                      </div>
                                      <br>
                                      <input class="form-control btn btn-warning" id="upload" type="submit" value="Upload file to server">
                                    </form>

                                    <div style="height: 50px;" class="progress">
                                      <div class="bar"></div>
                                      <div class="percent">0%</div>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div> 
                          <!---------------------------Back Center--------------------------->
                          <div class="row">
                            <div class="col-sm-6 col-12" style="cursor:pointer;">
                            <div class="modal1" id="myModal1-5" style="display:none; z-index: 9999;">
                              <div class="modal-dialog">
                                <div class="modal-content model-content-1">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                  <h6 class="modal-title modal-color">SELECT A BADGE</h6>
                                  <button type="button" id="close1-5" data-dismiss="modal">×</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body">
                                  <div class="row no-gutters">
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-1 btn-light float-left mr-1">A-Z</button>
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-2 btn-light float-left">Symbol</button>
                                    </div>
                                  </div>
                                  <div class="row no-gutters" style="margin-top:10px;">
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-3 btn-light float-left mr-1">Editable</button>
                                    </div>
                                    <div class="col-6">
                                    <button type="button" class="btn-model-design btn-4 btn-light float-left">Upload</button>
                                    </div>
                                  </div>
                                  <hr>
                                    <div class="one-one" style="display:none;">
                                      <div class="container">
                                        <div class="row mt-3">
                                        @foreach($alphabets as $alphabet)
                                        <div class="col-sm-3 col-3"><a onclick="addBackCenter('{{ asset("img/alphabets/") }}/{!! $alphabet->alphabet !!}','backCenter')"><img src="{{ asset('img/alphabets/') }}/{!! $alphabet->alphabet !!}"></a></div>
                                        @endforeach
                                        </div>
                                      </div>
                                      <hr>
                                        <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile5" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz5" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile5">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha1','backCenter')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz5">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','alpha2','backCenter')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="two-one" style="display:none;">
                                    <div class="row">
                                    @foreach($symbols as $symbol)
                                      <div class="col-sm-4 col-4"><a onclick="addBackCenter('{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}','backCenter')"><img src="{{ asset('img/symbols/') }}/{!! $symbol->symbol !!}"></a></div>
                                    @endforeach
                                    </div>
                                    <hr>
                                        <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile5-1" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz5-1" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile5-1">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','backCenter')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz5-1">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','backCenter')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="three-one" style="display:none;">
                                      <div class="row" style="padding-left: 10px; padding-right: 10px;">Text (Max 12 characters)</div>
                                      <div class="row" style="padding-left: 10px; padding-right: 10px;"><input type="text" id="editable_text1" class="col-12" onchange="addText(this.value,'backCenter','text-align: center;')" maxlength="12"></div>
                                      <div class="row" style="padding-left: 10px; padding-right: 10px; padding-bottom: 10px;">
                                        <select id="fonts" class="col-12">
                                          <option>Select Font</option>
                                          @foreach($fonts as $font)
                                          <option value="{!! $font->id !!}">{!! $font->name !!}</option>
                                          @endforeach
                                        </select>
                                      </div>
                                      <div class="row">
                                    @foreach($elogos as $elogo)
                                      <div class="col-sm-4 col-4"><a onclick="addBackCenter('{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}','backCenter')"><img src="{{ asset('img/editable_logos/') }}/{!! $elogo->logo !!}"></a></div>
                                    @endforeach
                                    </div>
                                    <hr>
                                        <ul class="nav nav-tabs" role="tablist">
                                          <li class="nav-item">
                                            <a class="nav-link active" href="#profile5-2" role="tab" data-toggle="tab" id="border1"><span style="color: #99999b;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                          <li class="nav-item">
                                            <a class="nav-link" href="#buzz5-2" role="tab" data-toggle="tab" id="border2"><span style="color: #040404;"><i class="fas fa-circle"></i><span></a>
                                          </li>
                                        </ul>
                                        <!-- Tab panes -->
                                        <div class="tab-content">
                                          <div role="tabpanel" class="tab-pane active demo3 mt-2" id="profile5-2">
                                              @foreach($colors as $color)
                                                <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S1','backCenter')">
                                                <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                                <?php $i++?>
                                              @endforeach                   
                                          </div>
                                          <div role="tabpanel" class="tab-pane fade demo3 mt-2" id="buzz5-2">
                                            @foreach($colors as $color)
                                              <input type="checkbox" id="checkbox-1-<?=$i?>" class="regular-checkbox big-checkbox" value="checkbox1" onclick="changeColor('{!! $color->color !!}','S2','backCenter')">
                                              <label for="checkbox-1-<?=$i?>" style="background-color: {!! $color->color !!} !important;"></label>
                                              <?php $i++?>
                                            @endforeach
                                          </div>
                                        </div>
                                    </div>
                                    <div class="four-one" style="display:none;">
                                      <h3>Select an image file</h3>
                                    <form class="form form-horizontal" id="custom5" method="post" action="{{ route('step1-addColor.uploade') }}" enctype="multipart/form-data">
                                      @csrf
                                      <div style="position:relative;">
                                        <a class='btn btn-primary' href='javascript:;'>
                                          Select file ...
                                          <input required id="apkfile" name="myfile" accept="image/x-svg" type="file"
                                              style='position:absolute;z-index:2;top:0;left:0;filter: alpha(opacity=0);-ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";opacity:0;background-color:transparent;color:transparent;'
                                              name="file_source" size="40">
                                        </a>
                                      </div>
                                      <br>
                                      <input class="form-control btn btn-warning" id="upload" type="submit" value="Upload file to server">
                                    </form>

                                    <div style="height: 50px;" class="progress">
                                      <div class="bar"></div>
                                      <div class="percent">0%</div>
                                    </div>
                                    </div>
                                  </div>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        
                          @for($i=1;$i<=count($layers);$i++)
                          <div class="mymodal7" id="myModal<?=$i?>" style="display: none">
                            <div class="modal-dialog">
                              <div class="modal-content model-content-7">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                 <h6 class="modal-title modal-color">{!! $layers[$i-1]->layer_title !!} Color</h6>
                                 <button type="button" id="close<?=$i?>" data-dismiss="modal">×</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body">
                                  <div class="one-one" style="">
                                    <div class="container">
                                      <div class="row mt-3">
                                        <div class="demo3 mt-2" id="checkboxes">
                                        <?php $j=5;?>
                                          @foreach($colors as $color)
                                            <input type="checkbox" id="checkbox-<?=$i+12?>-<?=$j?>" class="regular-checkbox big-checkbox" value="{!! $color->color !!}" onclick="changeS1Colors('{!! $color->color !!}','{!! $layers[$i-1]->layer_id !!}')">
                                            <label for="checkbox-<?=$i+12?>-<?=$j?>" style="background-color: {!! $color->color !!} !important;"></label>
                                            <?php $j++?>
                                          @endforeach
                                          <p>Fluoresent Colors</p>
                                          <input type="checkbox" id="checkbox-<?=$i+12?>-<?=$j?>" class="regular-checkbox big-checkbox" value="#e22687" onclick="changeS1Colors('#e22687','{!! $layers[$i-1]->layer_id !!}')">
                                            <label for="checkbox-<?=$i+12?>-<?=$j++?>" style="background-color: #e22687 !important;"></label>
                                          <input type="checkbox" id="checkbox-<?=$i+12?>-<?=$j?>" class="regular-checkbox big-checkbox" value="#e9e61f" onclick="changeS1Colors('#e9e61f','{!! $layers[$i-1]->layer_id !!}')">
                                            <label for="checkbox-<?=$i+12?>-<?=$j++?>" style="background-color: #e9e61f !important;"></label>
                                          <input type="checkbox" id="checkbox-<?=$i+12?>-<?=$j?>" class="regular-checkbox big-checkbox" value="#f59932" onclick="changeS1Colors('#f59932','{!! $layers[$i-1]->layer_id !!}')">
                                            <label for="checkbox-<?=$i+12?>-<?=$j++?>" style="background-color: #f59932 !important;"></label>
                                          <input type="checkbox" id="checkbox-<?=$i+12?>-<?=$j?>" class="regular-checkbox big-checkbox" value="#f06522" onclick="changeS1Colors('#f06522','{!! $layers[$i-1]->layer_id !!}')">
                                            <label for="checkbox-<?=$i+12?>-<?=$j++?>" style="background-color: #f06522 !important;"></label>
                                      </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          @endfor
                          <div class="mymodal7" id="myModal-1" style="display:none;" style="z-index:9999;">
                            <div class="modal-dialog">
                               <div class="modal-content model-content-7">
                                <!-- Modal Header -->
                                <div class="modal-header">
                                 <h6 class="modal-title modal-color">Embelishment Color</h6>
                                 <button type="button" id="close-1" data-dismiss="modal">×</button>
                                </div>
                                <!-- Modal body -->
                                <div class="modal-body">
                                 <div class="one-one" style="">
                                  <div class="container">
                                     <div class="row mt-3">
                                      <div class="demo4 mt-2" id="checkboxes">
                                       <?php $j=5?>
                                       @foreach($colors as $color)
                                        <input type="checkbox" id="checkbox-6-<?=$j?>" class="regular-checkbox big-checkbox" value="{!! $color->color !!}" onclick="changeEmbColor('{!! $color->color !!}')">
                                        <label for="checkbox-6-<?=$j?>" style="background-color: {!! $color->color !!} !important;"></label>
                                        <?php $j++?>
                                       @endforeach
                                       <p>Fluoresent Colors</p>
                                       <input type="checkbox" id="checkbox-6-<?=$j?>" class="regular-checkbox big-checkbox" value="#e22687" onclick="changeEmbColor('#e22687')">
                                          <label for="checkbox-6-<?=$j++?>" style="background-color: #e22687 !important;"></label>
                                        <input type="checkbox" id="checkbox-6-<?=$j?>" class="regular-checkbox big-checkbox" value="#e9e61f" onclick="changeEmbColor('#e9e61f')">
                                          <label for="checkbox-6-<?=$j++?>" style="background-color: #e9e61f !important;"></label>
                                        <input type="checkbox" id="checkbox-6-<?=$j?>" class="regular-checkbox big-checkbox" value="#f59932" onclick="changeEmbColor('#f59932')">
                                          <label for="checkbox-6-<?=$j++?>" style="background-color: #f59932 !important;"></label>
                                        <input type="checkbox" id="checkbox-6-<?=$j?>" class="regular-checkbox big-checkbox" value="#f06522" onclick="changeEmbColor('#f06522')">
                                          <label for="checkbox-6-<?=$j++?>" style="background-color: #f06522 !important;"></label>
                                      </div>
                                     </div>
                                  </div>
                                 </div>
                                </div>
                               </div>
                            </div>
                           </div>
                          <div class="row">
                            <?php if($embs != '')
                            {
                            ?>
                            <div class="col-12" style="z-index: 2; width: 97%; height: 90% !important; cursor:pointer; position:absolute;" id="embLayer"></div>
                            <?php
                            }?>
                            <?php /**/?><div class="img-fluid" style="z-index: 3; cursor:pointer; position:absolute;"><img src="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->base !!}_base.png" class='col-12'></div>
                                        <?php /**/?>
                            <div class="img-fluid" style="z-index: 4; cursor:pointer; position:absolute;"><img src="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->overlay !!}_overlay.png" class='col-12'></div>
                            <?php /**/?>
                            @if($collar!=0)
                            <div id='collarDiv' class="col-sm-11" style="z-index: 2; cursor:pointer; position: absolute; margin-top: -3px;">{!! $collars[0]->collar !!}</div>
                            @endif
                            <?php /**/?>
                            <div class="col-sm-12 col-12" id="svgDataDiv" style="z-index: 1; cursor:pointer;">{!! $products[0]->image !!}</div>
                            <div class="col-sm-12" style="z-index: 3; background: #ffffff; padding: 12px;">
                              <div class="col-sm-2 col-3 off-on" style="padding:0px;">
                                <div class="col-6 m-show active-1 off-style" onclick="addOutline()">ON</div>
                                <div class=" col-6 s-hide box-1 on-style" onclick="removeOutline()">Off</div>
                              </div>  
                            </div>
                            
                              <div class="test" style="z-index:2; position:absolute; top:125px; left:110px; height:80px; width:80px;" id='frontTL'></div>
                            <div class="test" style="z-index:2; position:absolute; top:205px; left:110px; height:12pt; width:80px; color: #ffffff;" id='frontTLText'></div>
                            <div class="test" style="z-index:2; position:absolute; top:125px; left:245px; height:80px; width:80px;" id='frontTR'></div>
                            <div class="test" style="z-index:2; position:absolute; top:185px; left:245px; height:12pt; width:80px; color: #ffffff;" id='frontTRText'></div>
                            <div  class="test"style="z-index:2; position:absolute; top:340px; left:100px; height:80px; width:80px;" id='frontBL'></div>
                            <div class="test"style="z-index:2; position:absolute; top:4200px; left:100px; height:12pt; width:80px; color: #ffffff;" id='frontBLText'></div>
                            <div  class="test"style="z-index:2; position:absolute; top:340px; left:257px; height:80px; width:80px;" id='frontBR'></div>
                            <div class="test"style="z-index:2; position:absolute; top:420px; left:257px; height:12pt; width:80px; color: #ffffff;" id='frontBRText'></div>
                            <div class="test"style="z-index:2; position:absolute; top:100px; left:536px; height:150px; width:150px;" id='backCenter'></div>
                            <div class="test"style="z-index:2; position:absolute; top:250px; left:536px; height:16pt; width:150px; color: #ffffff;" id='backCenterText'></div>
                          </div>
                          <div id="myModal-kit" class="modal" style="z-index: 999; margin-top: 50px;">
                            <div class="modal-content">
                              <span class="close-kit" style="float: right;">&times;</span>
                              <div class="col-12">
                                <div class="container">
                                <h3>Submit Design</h3>
                                <div class="row" style="margin-top: 7%;">
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtName">Name*</label>
                                      <input id="txtName" type='text'>
                                    </fieldset>
                                  </div>
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtlname">Last Name*</label>
                                      <input id="txtlname" type='text'>
                                    </fieldset>
                                  </div>
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtemail">Email*</label>
                                      <input id="txtemail" type='email'>
                                    </fieldset>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtnumber">Phone*</label>
                                      <input id="txtnumber" type='number'>
                                    </fieldset>
                                  </div>
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtorganization">Organization/School/Club</label>
                                      <input id="txtorganization" type='text'>
                                    </fieldset>
                                  </div>
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtqt">Quantity</label>
                                      <input id="txtqt" type='number'>
                                    </fieldset>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtcountry">Country</label>
                                      <input id="txtcountry" type='text'>
                                    </fieldset>
                                  </div>
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtcity">City</label>
                                      <input id="txtcity" type='text'>
                                    </fieldset>
                                  </div>
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtstate">State</label>
                                      <input id="txtstate" type='text'>
                                    </fieldset>
                                  </div>
                                  <div class="col-12 col-sm-4">
                                    <fieldset class='float-label-field'>
                                      <label for="txtPcode">Postal Code</label>
                                      <input id="txtPcode" type='text'>
                                    </fieldset>
                                  </div>
                                </div>
                                <div class="row">
                                  <div class="col-12 col-sm-12">
                                    <fieldset class='float-label-field'>
                                      <label for="txtcomment">Comments</label>
                                      <input id="txtcomment" type='text'>
                                    </fieldset>
                                  </div>
                                </div>
                                <br>
                                <div style="text-align: right;">
                                  <a class="btn submit-btn" href="">
                                    <p style="margin-bottom: 0px;">Submit</p>
                                  </a>
                                </div>
                                </div>
                              </div>
                            </div>
                          </div>
                      </div>

                      <div class="col-md-3 info" >
                          <div class="row ">
                                  <!---LEFT MENU START-->
                                  <ol class="css-accordion user-journey">
                                      <li class="accordion-item stage-1">
                                        <input type="radio" name="accordion-control active" id="stage-5-control" checked>
                                        <label class="menu-label" for="stage-5-control"><span class="add-color">Add Color</span></label>
                                        <div class="item-content-container">
                                          <div class="item-content">
                                              <ol id="layers" class="prefixed styled">
                                                  <?php $i=1;?>
                                                  @foreach($layers as $layer)
                                                  <li class="add-color-<?=$i?>"><span>&#9675</span>{!! $layer->layer_title !!}</li><?php $i++;?>
                                                  @endforeach
                                                  <?php if($embs != '')
                                                  {
                                                  ?>
                                                  <li class="add-color--1"><span>&#9675</span>Embelishment</li>
                                                  <?php
                                                  }?>
                                              </ol>
                                          </div>
                                        </div>
                                      </li>
                                      <li class="accordion-item stage-2">
                                        <input type="radio" name="accordion-control" id="stage-6-control">
                                        <label class="menu-label" for="stage-6-control"><span class="add-design">Add Design</span></label>
                                        <div class="item-content-container">
                                          <div class="item-content">
                                              <ol class="prefixed styled">
                                                  <li id="one-my-design"><span>&#9675</span>Front Top Right</li>
                                                  <li id="two-my-design"><span>&#9675</span>Front Top Left</li>
                                                  <li id="three-my-design"><span>&#9675</span>Front Bottom Right</li>
                                                  <li id="four-my-design"><span>&#9675</span>Front Bottom Left</li>
                                                  <li id="five-my-design"><span>&#9675</span>Back Center</li>
                                              </ol>
                                          </div>
                                        </div>
                                      </li>
                                      <li class="accordion-item stage-3">
                                        <input type="radio" name="accordion-control" id="stage-7-control">
                                        <label class="menu-label" for="stage-7-control"><span class="add-design" id="saveDesign1">Save Design</span></label>
                                      </li>
                                  </ol>
                                  <!-- -LEFT MENU END -->
                                  <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                  <input id="base" name="base" type="hidden" value="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->base !!}_base.png">
                                  <input id="overlay" name="overlay" type="hidden" value="{{ asset('img/t-shirts/') }}/{!! $cloth[0]->overlay !!}_overlay.png">
                                  <input id="svgData" name="svgData" type="hidden" value="{!! $_REQUEST['designID'] !!}">

                                  <input id="baseColor" name="baseColor" type="hidden">
                                  <input id="layer1Color" name="layer1Color" type="hidden">
                                  <input id="layer2Color" name="layer2Color" type="hidden">
                                  <input id="accentColor" name="accentColor" type="hidden">

                                  <input id="frontTRFile" name="frontTRFile" type="hidden">
                                  <input id="frontTLFile" name="frontTLFile" type="hidden">
                                  <input id="frontBRFile" name="frontBRFile" type="hidden">
                                  <input id="frontBLFile" name="frontBLFile" type="hidden">
                                  <input id="backCenterFile" name="backCenterFile" type="hidden">
                          </div>
                          <div class="row ">
                              <hr class="col-12">
                              <div class="col-12" style="text-align: justify;"><span><strong>Price:</strong> ${!! $products[0]->price !!}</span><br><span><strong>Description:</strong> {!! $products[0]->description !!}</span></div>  
                          </div>
                      </div>
                      
                     
                </div>
            </div>
         </section>
         

		 <script>
         var acc = document.getElementsByClassName("accordion");
         var i;

         for (i = 0; i < acc.length; i++) {
           acc[i].addEventListener("click", function() {
             this.classList.toggle("active");
             var panel = this.nextElementSibling;
             if (panel.style.maxHeight) {
               panel.style.maxHeight = null;
             } else {
               panel.style.maxHeight = panel.scrollHeight + "px";
             }
           });
         }
        $(document).ready(function () {
          <?php
          /**/
          for($i=1;$i<=count($layers);$i++)
          {
            ?>
          $(".add-color-<?=$i?>").click(function(){
            <?php
            for($j=1;$j<=count($layers);$j++)
            {
              if($j==$i)
              {
                ?>
                $("#myModal<?=$j?>").toggle();
                $("#myModal<?=$j?>").css("z-index",("9999"));
                <?php    
              }
              else{
                ?>
                $("#myModal<?=$j?>").hide();
                <?php
              }
            }
            ?>
            $("#myModal-1").hide();
            
          });
          $("#close<?=$i?>").click(function(){
            $("#myModal<?=$i?>").css("display","none");
          });
          <?php
          }
          ?>
          $(".add-color--1").click(function(){
            $("#myModal-1").toggle();
            <?php
            for($i=1;$i<=count($layers);$i++)
            {
            ?>
            $("#myModal<?=$j?>").hide();
            <?php
            }
            ?>
            $("#myModal-1").css("z-index",("9999"));
          });
          $("#close-1").click(function(){
            $("#myModal-1").css("display","none");
          });
          <?php /**/?>
          $(".m-show").click(function () {

            if ($(".m-show").hasClass("active-1")) {

            } else {
              $(".m-show").addClass("active-1");
              $(".s-hide").removeClass("active-1");
              $(".contact-box").show();
              $(".box-mrg").hide();
            }
          });

          $(".s-hide").click(function () {

            if ($(".s-hide").hasClass("active-1")) {

            } else {
              $(".s-hide").addClass("active-1");
              $(".m-show").removeClass("active-1");
              $(".contact-box").hide();
              $(".box-mrg").show();
            }
          });

        });
        $(document).ready(function(){
			$(".btn-1").click(function(){
			  $(".one-one").show();
			  $(".two-one").hide();
			  $(".three-one").hide();
			  $(".four-one").hide();
        $(".five-one").hide();
        $(".six-one").hide();
			  $(".one-one").css("z-index",("9999"));
			});
			$(".btn-2").click(function(){
			  $(".one-one").hide();
			  $(".two-one").show();
			  $(".three-one").hide();
			  $(".four-one").hide();
        $(".five-one").hide();
        $(".six-one").hide();
			  $(".two-one").css("z-index",("9999"));
			});
			$(".btn-3").click(function(){
			  $(".one-one").hide();
			  $(".two-one").hide();
			  $(".three-one").show();
			  $(".four-one").hide();
        $(".five-one").hide();
        $(".six-one").hide();
			  $(".three-one").css("z-index",("9999"));
			});
			$(".btn-4").click(function(){
			  $(".one-one").hide();
			  $(".two-one").hide();
			  $(".three-one").hide();
			  $(".four-one").show();
        $(".five-one").hide();
        $(".six-one").hide();
			  $(".four-one").css("z-index",("9999"));
			});
      $(".btn-5").click(function(){
			  $(".one-one").hide();
			  $(".two-one").hide();
			  $(".three-one").hide();
			  $(".four-one").hide();
        $(".five-one").show();
        $(".six-one").hide();
			  $(".five-one").css("z-index",("9999"));
			});
      $(".btn-6").click(function(){
			  $(".one-one").hide();
			  $(".two-one").hide();
			  $(".three-one").hide();
			  $(".four-one").hide();
        $(".five-one").hide();
        $(".six-one").show();
			  $(".six-one").css("z-index",("9999"));
			});
			$('#checkboxes').on('click', ':checkbox', function(e) {
			  $('#checkboxes :checkbox').each(function() {
				if (this != e.target)
				  $(this).prop('checked', false);
			  });
			});
			$('#one-my-design').click(function(){
				$('#myModal1-2').hide();
        $('#myModal1-3').hide();
        $('#myModal1-4').hide();
        $('#myModal1-5').hide();
        $('#myModal1-1').show();
				$(".one-one").show();
				$("#myModal1-1").css("z-index",("9999"));
			});
			$('#two-my-design').click(function(){
				$('#myModal1-1').hide();
        $('#myModal1-3').hide();
        $('#myModal1-4').hide();
        $('#myModal1-5').hide();
        $('#myModal1-2').show();
				$(".one-one").show();
				$("#myModal1-2").css("z-index",("9999"));
			});
			$('#three-my-design').click(function(){
				$('#myModal1-1').hide();
        $('#myModal1-2').hide();
        $('#myModal1-4').hide();
        $('#myModal1-5').hide();
        $('#myModal1-3').show();
				$(".one-one").show();
				$("#myModal1-3").css("z-index",("9999"));
			});
			$('#four-my-design').click(function(){
				$('#myModal1-1').hide();
        $('#myModal1-2').hide();
        $('#myModal1-3').hide();
        $('#myModal1-5').hide();
        $('#myModal1-4').show();
				$(".one-one").show();
				$("#myModal1-4").css("z-index",("9999"));
			});
			$('#five-my-design').click(function(){
				$('#myModal1-1').hide();
        $('#myModal1-2').hide();
        $('#myModal1-3').hide();
        $('#myModal1-4').hide();
        $('#myModal1-5').show();
				$(".one-one").show();
				$("#myModal1-5").css("z-index",("9999"));
			});
			jQuery('#close1-1').click(function(){
				jQuery('#myModal1-1') .hide();
			});
			jQuery('#close1-2').click(function(){
				jQuery('#myModal1-2') .hide();
			});
			jQuery('#close1-3').click(function(){
				jQuery('#myModal1-3') .hide();
			});
			jQuery('#close1-4').click(function(){
				jQuery('#myModal1-4') .hide();
			});
			jQuery('#close1-5').click(function(){
				jQuery('#myModal1-5') .hide();
			});
		});
		    $('.stage-2').click(function() {
            $('#step-2').addClass("complete");//.css('background-image', 'url(../../images/StepBox-Green.png) !important')
        });
        $('.stage-3').click(function() {
            $('#step-3').addClass("complete");
            //$('.current-bg-3').css('background-image', 'url(../../images/StepBox-Green-End.png) !important')
        });
            $('#saveDesign').click(function(e){
                $('#myModal-kit').show();
                var base = $('#base').val();
                var overlay = $('#overlay').val();
                var svgData = $('#svgData').val();
                var baseColor = $('#baseColor').val();
                var layer1Color = $('#layer1Color').val();
                var layer2Color = $('#layer2Color').val();
                var accentColor = $('#accentColor').val();
                var frontTRFile = $('#frontTRFile').val();
                var frontTLFile = $('#frontTLFile').val();
                var frontBRFile = $('#frontBRFile').val();
                var frontBLFile = $('#frontBLFile').val();
                var backCenterFile = $('#backCenterFile').val();
                //console.log(base+" - "+overlay+" - "+svgData+" - "+baseColor+" - "+layer1Color+" - "+layer2Color+" - "+accentColor+" - "+frontTRFile+" - "+frontTLFile+" - "+frontBRFile+" - "+frontBLFile+" - "+backCenterFile);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('step1-addColor.download-pdf') }}",
                    data: {
                        base: base,
                        overlay: overlay,
                        svgData: svgData,
                        overlay: overlay,
                        baseColor: baseColor,
                        layer1Color: layer1Color,
                        layer2Color: layer2Color,
                        accentColor: accentColor,
                        frontTRFile: frontTRFile,
                        frontTLFile: frontTLFile,
                        frontBRFile: frontBRFile,
                        frontBLFile: frontBLFile,
                        backCenterFile: backCenterFile
                    },
                	success: function(result){
                		window.open(result);
                		alert(result);
                	}
                });
            });
            $('#saveDesign1').click(function(e){
                $('#myModal-kit').show();
                var base = $('#base').val();
                var overlay = $('#overlay').val();
                var svgData = $('#svgData').val();
                var baseColor = $('#baseColor').val();
                var layer1Color = $('#layer1Color').val();
                var layer2Color = $('#layer2Color').val();
                var accentColor = $('#accentColor').val();
                var frontTRFile = $('#frontTRFile').val();
                var frontTLFile = $('#frontTLFile').val();
                var frontBRFile = $('#frontBRFile').val();
                var frontBLFile = $('#frontBLFile').val();
                var backCenterFile = $('#backCenterFile').val();
                //console.log(base+" - "+overlay+" - "+svgData+" - "+baseColor+" - "+layer1Color+" - "+layer2Color+" - "+accentColor+" - "+frontTRFile+" - "+frontTLFile+" - "+frontBRFile+" - "+frontBLFile+" - "+backCenterFile);
                $.ajax({
                    type: 'POST',
                    url: "{{ route('step1-addColor.download-pdf') }}",
                    data: {
                        base: base,
                        overlay: overlay,
                        svgData: svgData,
                        overlay: overlay,
                        baseColor: baseColor,
                        layer1Color: layer1Color,
                        layer2Color: layer2Color,
                        accentColor: accentColor,
                        frontTRFile: frontTRFile,
                        frontTLFile: frontTLFile,
                        frontBRFile: frontBRFile,
                        frontBLFile: frontBLFile,
                        backCenterFile: backCenterFile
                    },
                	success: function(result){
                		window.open(result);
                		alert(result);
                	}
                });
            });
            $('.close-kit').click(function(){
                $('#myModal-kit').css('display','none');
            });
            $('.on-style').click(function(){
                $('.on-style').css('background','#7bc143')
                $('.off-style').css('background','#e9e5e5')
            });
            $('.off-style').click(function(){
                $('.off-style').css('background','#7bc143')
                $('.on-style').css('background','#e9e5e5')
            });
		</script>
		<!-- End Content -->
		@include('layouts.front_footer')
	</body>
	<script>
    	document.addEventListener("DOMContentLoaded", function (event) {
            (function () {
                var bar = $('.bar');
                var percent = $('.percent');
                var status = $('#status');
                
                $('#custom1').ajaxForm({
                    beforeSend: function () {
                        status.empty();
                        var percentVal = '0%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    uploadProgress: function (event, position, total, percentComplete) {
                        var percentVal = percentComplete + '%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    success: function (data, textStatus, jqXHR, pos) {
                        $('.image').val(jqXHR.responseText);
                        $('.image').hide();
                        var percentVal = '100%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                        var position = 'frontTR';//$('#position').val();
                        console.log(position);
                        addFrontTR("{{ asset('img/custom_img/') }}/"+jqXHR.responseText,'frontTR');
                    },
                    complete: function (xhr) {
                    }
                });
                $('#custom2').ajaxForm({
                    beforeSend: function () {
                        status.empty();
                        var percentVal = '0%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    uploadProgress: function (event, position, total, percentComplete) {
                        var percentVal = percentComplete + '%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    success: function (data, textStatus, jqXHR, pos) {
                        $('.image').val(jqXHR.responseText);
                        $('.image').hide();
                        var percentVal = '100%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                        var position = 'frontTL';//$('#position').val();
                        console.log(position);
                        addFrontTL("{{ asset('img/custom_img/') }}/"+jqXHR.responseText,'frontTL');
                    },
                    complete: function (xhr) {
                    }
                });
                $('#custom3').ajaxForm({
                    beforeSend: function () {
                        status.empty();
                        var percentVal = '0%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    uploadProgress: function (event, position, total, percentComplete) {
                        var percentVal = percentComplete + '%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    success: function (data, textStatus, jqXHR, pos) {
                        $('.image').val(jqXHR.responseText);
                        $('.image').hide();
                        var percentVal = '100%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                        var position = 'frontBR';//$('#position').val();
                        console.log(position);
                        addFrontBR("{{ asset('img/custom_img/') }}/"+jqXHR.responseText,'frontBR');
                    },
                    complete: function (xhr) {
                    }
                });
                $('#custom4').ajaxForm({
                    beforeSend: function () {
                        status.empty();
                        var percentVal = '0%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    uploadProgress: function (event, position, total, percentComplete) {
                        var percentVal = percentComplete + '%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    success: function (data, textStatus, jqXHR, pos) {
                        $('.image').val(jqXHR.responseText);
                        $('.image').hide();
                        var percentVal = '100%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                        var position = 'frontBL';//$('#position').val();
                        console.log(position);
                        addFrontBL("{{ asset('img/custom_img/') }}/"+jqXHR.responseText,'frontBL');
                    },
                    complete: function (xhr) {
                    }
                });
                $('#custom5').ajaxForm({
                    beforeSend: function () {
                        status.empty();
                        var percentVal = '0%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    uploadProgress: function (event, position, total, percentComplete) {
                        var percentVal = percentComplete + '%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                    },
                    success: function (data, textStatus, jqXHR, pos) {
                        $('.image').val(jqXHR.responseText);
                        $('.image').hide();
                        var percentVal = '100%';
                        bar.width(percentVal)
                        percent.html(percentVal);
                        var position = 'backCenter';//$('#position').val();
                        console.log(position);
                        addBackCenter("{{ asset('img/custom_img/') }}/"+jqXHR.responseText,'backCenter');
                    },
                    complete: function (xhr) {
                    }
                });
            })();
        });
    	getDefaultColors();
    	addOutline();
        <?php
        if(isset($_REQUEST['base'])) {
            ?>
            changeBaseColor('#<?=$_REQUEST['base']?>');
        <?php
        }
        if(isset($_REQUEST['layer1'])) {
            ?>
            changeLayer1Color('#<?=$_REQUEST['layer1']?>');
        <?php
        }
        if(isset($_REQUEST['layer2'])) {
            ?>
            changeLayer2Color('#<?=$_REQUEST['layer2']?>');
        <?php
        }
        if(isset($_REQUEST['layer3'])) {
            ?>
            changeLayer3Color('#<?=$_REQUEST['layer3']?>');
        <?php
        }
        ?>
        /*var mySVG = document.getElementsByTagName('svg');

        for(i=0; i<mySVG.length;i++)
            mySVG[i].setAttribute("viewBox", "0 0 1200 1200");*/
    </script>
    <script>
    	var closemodal = document.getElementById('myModal2');
    	var closemodal2 = document.getElementById('myModal3');
    	var closemodal3 = document.getElementById('myModal4');
    	var closemodal4 = document.getElementById('myModal5');
      var closemodal10 = document.getElementById('myModal6');
    	var closemodal5 = document.getElementById('myModal1-1');
    	var closemodal6 = document.getElementById('myModal1-2');
    	var closemodal7 = document.getElementById('myModal1-3');
    	var closemodal8 = document.getElementById('myModal1-4');
    	var closemodal9 = document.getElementById('myModal1-5');
    	
        window.onclick = function(event) {
            if (event.target == closemodal) {
                closemodal.style.display = "none";
            }
            if (event.target == closemodal2) {
                closemodal2.style.display = "none";
        	}
        	if (event.target == closemodal3) {
                closemodal3.style.display = "none";
        	}
        	if (event.target == closemodal4) {
                closemodal4.style.display = "none";
        	}
        	if (event.target == closemodal5) {
                closemodal5.style.display = "none";
        	}
        	if (event.target == closemodal6) {
                closemodal6.style.display = "none";
        	}
        	if (event.target == closemodal7) {
                closemodal7.style.display = "none";
        	}
        	if (event.target == closemodal8) {
                closemodal8.style.display = "none";
        	}
        	if (event.target == closemodal9) {
                closemodal9.style.display = "none";
          }
          if (event.target == closemodal10) {
                closemodal10.style.display = "none";
          }
        }
    </script>
    <script>
        var $li = $('stage-2').click(function() {
            $li.removeClass('stage-2');
            $(this).addClass('current-step-1');
        });
    </script>
    <script>
        $('input[type="checkbox"]').on('change', function() {
           $('input[type="checkbox"]').not(this).prop('checked', false);
        });
    </script>
    <script>
        var allStates = $("svg > *");

        allStates.on("click", function() {
          var id = this.getAttribute('id');
          if(id=='l-1') {
            $("#myModal1").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal1").css("z-index",("9999"));
          }
          else if(id=='l-2') {
            $("#myModal2").show();
            $("#myModal1").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal2").css("z-index",("9999"));
          }
          else if(id=='l-3') {
            $("#myModal3").show();
            $("#myModal2").hide();
            $("#myModal1").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal3").css("z-index",("9999"));
          }
          else if(id=='l-4') {
            $("#myModal4").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal1").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal4").css("z-index",("9999"));
          }
          else if(id=='l-5') {
            $("#myModal5").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal1").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal5").css("z-index",("9999"));
          }
          else if(id=='l-6') {
            $("#myModal6").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal1").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal6").css("z-index",("9999"));
          }
          else if(id=='l-7') {
            $("#myModal7").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal1").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal7").css("z-index",("9999"));
          }
          else if(id=='l-8') {
            $("#myModal8").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal1").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal8").css("z-index",("9999"));
          }
          else if(id=='l-9') {
            $("#myModal9").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal1").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal9").css("z-index",("9999"));
          }
          else if(id=='l-10') {
            $("#myModal10").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal1").hide();
            $("#myModal11").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal10").css("z-index",("9999"));
          }
          else if(id=='l-11') {
            $("#myModal11").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal1").hide();
            $("#myModal12").hide();
            $("#myModal-1").hide();
            $("#myModal11").css("z-index",("9999"));
          }
          else if(id=='l-12') {
            $("#myModal12").show();
            $("#myModal2").hide();
            $("#myModal3").hide();
            $("#myModal4").hide();
            $("#myModal5").hide()
            $("#myModal6").hide();
            $("#myModal7").hide();
            $("#myModal8").hide();
            $("#myModal9").hide();
            $("#myModal10").hide();
            $("#myModal11").hide();
            $("#myModal1").hide();
            $("#myModal-1").hide();
            $("#myModal12").css("z-index",("9999"));
          }
          
          allStates.removeClass("on");
          $(this).addClass("on");
        });
    </script>
	<script>
	$(function() {
  $('#step-1').addClass('current-step-1');
});
	</script>
  <script>
    <?php
    if($embs != '')
    {
      $f = asset("img/embellishments/")."/".$embs[0]->file;
    ?>
    applyEmbStep1('{!! $f !!}');
    <?php
     }
    ?>
  </script>
    <script src="{{asset('js/download.js')}}"></script>
 <!----Model Script--->   
 <script src="{{asset('js/model.js')}}"></script>    
    <!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script> -->
    <!-- <script src="//cdnjs.cloudflare.com/ajax/libs/less.js/3.9.0/less.min.js"></script> -->
    <script>
      $('head').append('<link href="//fonts.googleapis.com/css?family=Open+Sans:300,400,600" rel="stylesheet" type="text/css">');

      $('input').focus(function(event) {
        $(this).closest('.float-label-field').addClass('float').addClass('focus');
      })

      $('input').blur(function() {
        $(this).closest('.float-label-field').removeClass('focus');
        if (!$(this).val()) {
          $(this).closest('.float-label-field').removeClass('float');
        }
      });
    </script>
    <script>
    var svgDataDiv = document.getElementById('svgDataDiv');
    console.log("Number of Layers: "+svgDataDiv.getElementsByTagName('path').length);
    for(var i=0; i<svgDataDiv.getElementsByTagName('path').length; i++) {
      //<li class="add-color-1"><span>&#9675</span>Body</li>
    }
    </script>
    <script>
    /**/
    var svgDataDiv = document.getElementById('collarDiv');
    var paths = svgDataDiv.getElementsByTagName('path');
    console.log("Number of Layers: "+paths.length);
    var li; 
    var ol = document.getElementById('layers');
    for(var i=0; i<paths.length; i++) {
      li = document.createElement("li");
      li.onclick="console.log('id:'"+paths[i].getAttribute('id')+")";
      li.setAttribute('id','l+'+(i+1))
      li.classList.add("add-color+"+(i+1));
      li.innerHTML = "<span>&#9675</span>"+paths[i].getAttribute('id');
      ol.appendChild(li);
    }
    /**/
    </script>
	@include('layouts.front_footerScript')
</html>>>