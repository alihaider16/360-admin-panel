@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Embellishments</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('embs.create') !!}">Add new</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-responsive" id="dataTable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>File</th>
                        <th>Thumb</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($embs as $emb)
                        <tr>
                            <td>{{$emb->name}}</td>
                            <td>
                                <img src="{{asset('img/embellishments/')}}/{!! $emb->file !!}" alt="img">
                            </td>
                            <td>
                                <img width="40%" class="cloth" src="{{asset('img/embellishments/thumbs/')}}/{!! $emb->thumb !!}" alt="img">
                            </td>
                            <td>
                                {!! Form::open(['route' => ['embs.destroy', $emb->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('embs.show', [$emb->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
                                    <a href="{!! route('embs.edit', [$emb->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @empty
                        <td>No embellishments added yet.</td>

                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

