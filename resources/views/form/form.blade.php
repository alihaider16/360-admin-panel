<form id="product-form" class="m-t-b" method="post" action="" role="form">

    <div class="messages"></div>

    <div class="controls">
        <h3>Add Form</h3>
        <hr>
        <div class="row">

            <div class="col-md-6">
                <div class="form-group">
                    <label for="name">Name</label>
                    <input id="name" type="text" name="name" class="form-control" placeholder="e.g Quote Form"
                           required="required" data-error="Position required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="distributor">Distributor</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="distributor" name="distributor">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input id="email" type="email" name="email" class="form-control"
                           placeholder="Please Valid email" required="required"
                           data-error="Email Rwquired.">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="email_subject">Email Subject</label>
                    <input id="email_subject" type="text" name="email_subject" class="form-control" placeholder=""  required="required" data-error="">
                    <!-- <div class="input-group">
                        <span class="input-group-addon"></span><input id="url_override" type="email"
                            name="url_override" class="form-control" placeholder="e.g #ffffff"
                            required="required" data-error="Valid email is required.">
                    </div> -->
                    <p>Set inside material color.Leave emplty to leave the to mirror the front material
                    </p>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_code">Form Code</label>
                    <input id="form_code" type="number" name="form_code" class="form-control"
                           placeholder="e.g 0.5" required="required" data-error="required">
                    <p>Use the same as your ecommerece unit</p>

                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="submit_button">Submit Button Text</label>
                    <input id="submit_button" type="text" name="submit_button" class="form-control"
                           placeholder="e.g 0.5" required="required" data-error="required">
                    <p>Use the same as your ecommerece unit</p>

                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->
        <!--input Fields-->
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="intro_text">intro Text</label>
                    <input id="intro_text" type="text" name="intro_text" class="form-control"
                           placeholder="e.g 0.5" required="required" data-error="required">
                    <p>Use the same as your ecommerece unit</p>

                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="footer_text">Footer Text</label>
                    <input id="footer_text" type="text" name="footer_text" class="form-control"
                           placeholder="e.g 0.5" required="required" data-error="required">
                    <p>Use the same as your ecommerece unit</p>

                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <!---End-->

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="completion_page">Completion Page</label>
                    <input id="completion_page" type="text" name="completion_page" class="form-control"
                           placeholder="e.g 0.5" required="required" data-error="required">
                    <p>Use the same as your ecommerece unit</p>

                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="form_type">From Type</label>
                    <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                    <select class="form-control" id="form_type" name="form_type">
                        <option>A</option>
                        <option>B</option>
                        <option>C</option>
                    </select>
                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="completion_page">Display On</label>
                    <input id="completion_page" type="checkbox" name="completion_page" class="form-control"
                           placeholder="e.g 0.5" required="required" data-error="required">
                    <input id="completion_page" type="checkbox" name="completion_page" class="form-control"
                           placeholder="e.g 0.5" required="required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="privious_button">Privious Button Text</label>
                    <input id="privious_button" type="text" name="privious_button" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required.">

                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="completion_page">Disable default builder from styling</label>
                    <input id="completion_page" type="checkbox" name="completion_page" class="form-control"
                           placeholder="e.g 0.5" required="required" data-error="required">
                    <input id="completion_page" type="checkbox" name="completion_page" class="form-control"
                           placeholder="e.g 0.5" required="required">
                    <div class="help-block with-errors"></div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="privious_button">Active</label>
                    <input id="privious_button" type="text" name="privious_button" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required.">

                    <div class="help-block with-errors"></div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-12">
                <input type="submit" class="btn btn-success btn-send" value="Save">
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <p class="text-muted"><strong>*</strong> These fields are required.</p>
            </div>
        </div>
    </div>

</form>