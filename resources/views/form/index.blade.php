@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Form</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container">

            <div class="m-t-b"><button id="product-btn" onclick="divToggle('product-form-toggle')"
                                       class="btn btn-primary pushme with-color">Add Form</button></div>

            <!--FormController Start-->
            <div class="d-none" id="product-form-toggle">
                @include('form.form')
            </div>
            <!--FormController End-->
            <!--Table-->
            @include('form.table')
            <!--End table-->

        </div>
    </div>
@endsection