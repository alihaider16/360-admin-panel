@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Product Categories</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('product_cat.create') !!}">Add</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-responsive" id="dataTable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($categories as $cat)
                        <tr>
                            <td>{{$cat->name}}</td>
                            <td>
                                <img width="40%" class="cloth" src="{{asset('storage/categories/'.$cat->image)}}" alt="img">
                            </td>
                            <td>
                                {!! Form::open(['route' => ['product_cat.destroy', $cat->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <?php /*?><a href="{!! route('product_cat.show', [$cat->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a><?php */?>
                                    <a href="{!! route('product_cat.edit', [$cat->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                        @empty
                        <td>No categories added yet.</td>

                    @endforelse

                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection





