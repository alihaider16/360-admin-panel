@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1 class="pull-left">Designs</h1>
        <h1 class="pull-right">
            <a class="btn btn-primary pull-right" style="margin-top: -10px;margin-bottom: 5px" href="{!! route('designs.create') !!}">Add new</a>
        </h1>
    </section>
    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>
        <div class="box box-primary">
            <div class="box-body">
                <table class="table table-responsive" id="dataTable">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Image</th>
                        <th>Price</th>
                        <th>Description</th>
                        <th>Actions</th>
                    </tr>
                    </thead>
                    <tbody>
                    @forelse($designs as $design)
                        <tr>
                            <td>{{$design->name}}</td>
                            <td>{!! $design->image !!}</td>
                            <td>$ {!! $design->price !!}</td>
                            <td>{!! $design->description !!}</td>
                            <td>
                                {!! Form::open(['route' => ['designs.destroy', $design->id], 'method' => 'delete']) !!}
                                <div class='btn-group'>
                                    <a href="{!! route('designs.show', [$design->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-tasks"></i></a>
                                    <a href="{!! route('designs.edit', [$design->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                                </div>
                                {!! Form::close() !!}
                            </td>
                        </tr>
                    @empty
                        <td>No designs added yet.</td>

                    @endforelse
                    </tbody>
                </table>
            </div>
        </div>
        <div class="text-center">

        </div>
    </div>
@endsection

