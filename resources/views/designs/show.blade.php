@extends('layouts.admin')

@section('content')
    <style>
        #layers {
            list-style-type: none !important;
        }
    </style>
    <section class="content-header">
        <h1>
            Design Edit
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                    {!! Form::model($design, ['route' => ['designs.addLayers'], 'method' => 'post']) !!}
                        <table class="form-group col-sm-12">
                            <tr>
                                <td class="col-sm-6"><strong>Name: </strong>{!! $design->name !!}</td>
                                <td class="col-sm-6" rowspan='2' id="svgDataDiv">{!! $design->image !!}</td>
                            </tr>
                            <tr>
                                <td class="col-sm-6">
                                    <script>
                                        var svgDataDiv = document.getElementById('svgDataDiv');
                                        var paths = svgDataDiv.getElementsByTagName('path');
                                        console.log("Number of Layers: "+paths.length);
                                        var layer_no = paths.length;
                                        <?php $layers = 'layer_no'; ?>
                                    </script>
                                    
                                    <strong>Layers: <input type='hidden' name='id' value='{!! $design->id !!}'><input type='hidden' name='layer_no' id='layers_no' value=''></strong>
                                    <div id="layers">
                                    <?php for($i=0;$i<20;$i++) { ?>
                                        <div class='form-group col-sm-12' id="div-<?=$i?>" style="display:none">
                                            <div class='col-sm-3' id='lbl-<?=$i?>'><i class='fa fa-circle'></i></div>
                                            <div class='col-sm-9'><input type='hidden' id='l-<?=$i?>' name='l-<?=$i?>' value=''><input type='text' id='t-<?=$i?>' name='t-<?=$i?>' placeholder='' value=''></div>
                                        </div>
                                    <?php }?>
                                    </div>
                                </td>
                            </tr>
                        </table>
                        <div class="form-group col-sm-12">
                            {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                            <a href="{!! route('designs.index') !!}" class="btn btn-default">Cancel</a>
                        </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
    <script>
    var svgDataDiv = document.getElementById('svgDataDiv');
    var paths = svgDataDiv.getElementsByTagName('path');
    console.log("Number of Layers: "+paths.length);
    document.getElementById('layers_no').value = paths.length;
    var id;
    for(var i=0; i<paths.length; i++) {
        id = paths[i].getAttribute('id')
        document.getElementById('div-'+i).style.display = '';
        document.getElementById('lbl-'+i).innerHTML = id;
        document.getElementById('l-'+i).value = id;
        document.getElementById('t-'+i).placeholder = id+" Title";
    }
    <?php
    for($j=0;$j<count($designLayers);$j++) {
        if($designLayers[$j]['layer_title']!='') {
            ?>
            document.getElementById('t-<?=$j?>').value = '<?=$designLayers[$j]['layer_title']?>';
            <?php
        }
    }
    ?>
    </script>
@endsection