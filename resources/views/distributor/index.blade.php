@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Distributor</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container">
            <!--FormController Start-->
            <div class="" id="product-form-toggle">
                @include('distributor.form')
            </div>
            <!--FormController End-->
            <!--Table-->
            @include('distributor.table')
            <!--End table-->

        </div>

    </div>
@endsection