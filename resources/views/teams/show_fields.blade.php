<!-- Id Field -->
<div class="form-group">
    {!! Form::label('id', 'ID:') !!}
    <p>{!! $teams->id !!}</p>
</div>

<!-- Heading Field -->
<div class="form-group">
    {!! Form::label('name', 'Name:') !!}
    <p>{!! $teams->name !!}</p>
</div>

<!-- Text Field -->
<div class="form-group">
    {!! Form::label('description', 'Description:') !!}
    <p>{!! $teams->description !!}</p>
</div>

<!-- Image Field -->
<div class="form-group">
    {!! Form::label('base', 'Base:') !!}
    <p><img class="cloth" src="{{ asset('img/t-shirts/') }}/{!! $clothes->base !!}_base.png"></p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', 'Updated:') !!}
    <p>{!! $teams->updated_at !!}</p>
</div>

