@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">File</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container">
            <h5>Bitmap Images e.g .png .jpg and .gif are publicly accessible. However .svg and 3D models are not.</h5>
            <hr>
            <form action="{{route("file.save")}}" enctype="multipart/form-data" method="post">
                @csrf
                <div class="row">
                    <div class="col-md-3"> <input id="name" type="file" name="image" class="form-control" placeholder="Enter Id For Download"
                                                  required="required" data-error="Setting Required">
                    <button class="btn btn-primary m-t-b" type="submit">Uploaded</button></div>
                </div>
            </form>
            <!--Table-->
            @include('file.table')
            <!--End table-->

        </div>
    </div>
@endsection