<style>
    canvas{
        width: 200px !important;
        height: 100px !important;
    }
    #gui_container{
        position: absolute;
        top: 0;
        right: 0;
    }
</style>
<table id="tbl-Product" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%"
       role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Name: activate to sort column ascending" style="width: 137px;">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 215px;">Name
        </th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 100px;">Preview
        </th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 215px;">Uploaded
        </th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th rowspan="1" colspan="1">ID</th>
        <th rowspan="1" colspan="1">Name</th>
        <th rowspan="1" colspan="1">preview</th>
        <th rowspan="1" colspan="1">uploaded</th>
    </tr>
    </tfoot>
    <tbody>
    @php($i = 1)
    @foreach($files as $file)
    <tr>
        <td>{{$i++}}</td>
        <td>{{$file->name}}</td>
        @if(pathinfo($file->image, PATHINFO_EXTENSION) == 'png' ||
            pathinfo($file->image, PATHINFO_EXTENSION) == 'jpg' ||
            pathinfo($file->image, PATHINFO_EXTENSION) == 'jpeg')
        <td>
            <img src="{{asset('/').'images/Files/'.$file->image}}" alt="image" height="100" width="100">
        </td>
        @elseif(pathinfo($file->image, PATHINFO_EXTENSION) == 'obj')
        <td id="append-{{$file->id}}">
                <script>
                    var scene = new THREE.Scene();

                    var camera = new THREE.PerspectiveCamera( 75, window.innerWidth/window.innerHeight, 0.1, 1000 );
                    camera.position.z = 200;

                    var renderer = new THREE.WebGLRenderer({antialias: true});
                    renderer.setClearColor("#eeeeee");
                    renderer.setSize( window.innerWidth, window.innerHeight );
                    document.getElementById("append-{{$file->id}}").appendChild( renderer.domElement );

                    window.addEventListener('resize', () => {
                        renderer.setSize( window.innerWidth, window.innerHeight );
                        camera.aspect = window.innerWidth / window.innerHeight ;
                        camera.updateProjectionMatrix();
                    });

                    var controls = new THREE.OrbitControls(camera, renderer.domElement);
                    controls.enableDamping = true;
                    controls.dampingFactor = 0.25;
                    controls.enableZoom = true;

                    var keyLight = new THREE.DirectionalLight(new THREE.Color('hsl(30, 100%, 75%)'), 1.0);
                    keyLight.position.set(-100, 0, 100);

                    var fillLight = new THREE.DirectionalLight(new THREE.Color('hsl(240, 100%, 75%)'), 0.75);
                    fillLight.position.set(100, 0, 100);

                    var backLight = new THREE.DirectionalLight(0xffffff, 1.0);
                    backLight.position.set(100, 0, -100).normalize();

                    scene.add(keyLight);
                    scene.add(fillLight);
                    scene.add(backLight);

                    var mtlLoader = new THREE.MTLLoader();
                    mtlLoader.setTexturePath('{{asset('/')}}images/Files/{{$file->name}}/');
                    mtlLoader.setPath('{{asset('/')}}images/Files/{{$file->name}}/');
                    mtlLoader.load('{{$file->mtl}}', function (materials) {

                        materials.preload();

                        var objLoader = new THREE.OBJLoader();
                        objLoader.setMaterials(materials);
                        objLoader.setPath('{{asset('/')}}images/Files/{{$file->name}}/');
                        objLoader.load('{{ $file->image }}', function (object) {

                            scene.add(object);
                            object.position.y -= 60;
                        });

                    });

                    var animate = function () {
                        requestAnimationFrame( animate );
                        controls.update();
                        renderer.render(scene, camera);
                    };

                    animate();
                </script>
        </td>
            @else
            <td>
                {!! $file->image !!}
            </td>
        @endif
        <td>{{date('d/m/yy',strtotime($file->created_at))}}</td>
    </tr>
    @endforeach
    </tbody>
</table>