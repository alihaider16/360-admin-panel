<style>
    .tt{
        width: 100px;
        max-width: 100px;
        max-height: 100px;
    }
</style>
<table class="table table-responsive" id="dataTable">
    <thead>
        <tr>
			<th>FAQs</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
    @foreach($textArr as $homeText)
        <tr>
            <td>
                {!! $homeText->question !!}
            </td>
			<td>
                {!! Form::open(['route' => ['faqs.destroy', $homeText->id], 'method' => 'delete']) !!}
                <div class='btn-group'>
                    <a href="{!! route('faqs.show', [$homeText->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-eye-open"></i></a>
					<a href="{!! route('faqs.edit', [$homeText->id]) !!}" class='btn btn-default btn-xs'><i class="glyphicon glyphicon-edit"></i></a>
                    {!! Form::button('<i class="glyphicon glyphicon-trash"></i>', ['type' => 'submit', 'class' => 'btn btn-danger btn-xs', 'onclick' => "return confirm('Are you sure?')"]) !!}
                </div>
                {!! Form::close() !!}
            </td>
        </tr>
    @endforeach
    </tbody>
</table>