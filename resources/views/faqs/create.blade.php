@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            FAQs
        </h1>
    </section>
    <div class="content">
        @include('adminlte-templates::common.errors')
        <div class="box box-primary">
            <div class="box-body">
                <div class="row">
                {!! Form::open(['route' => 'faqs.store']) !!}
					<!-- Name Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('question', 'Question:') !!}
                        {!! Form::text('question', null, ['class' => 'form-control']) !!}
                    </div>
					
					<!-- Name Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::label('answer', 'Answer:') !!}
                        {!! Form::textarea('answer', null, ['class' => 'form-control']) !!}
                    </div>

                    <!-- Submit Field -->
                    <div class="form-group col-sm-12">
                        {!! Form::submit('Save', ['class' => 'btn btn-primary']) !!}
                        <a href="{!! route('faqs.index') !!}" class="btn btn-default">Cancel</a>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
@endsection
