@extends('layouts.admin')

@section('content')
    <section class="content-header">
        <h1>
            FAQs
        </h1>
    </section>
    <div class="content">
        <div class="box box-primary">
            <div class="box-body">
                <div class="row" style="padding-left: 20px">
                    <!-- Id Field -->
					<div class="form-group">
						{!! Form::label('id', 'Id:') !!}
						<p>{!! $homeText->id !!}</p>
					</div>

					<!-- Heading Field -->
					<div class="form-group">
						{!! Form::label('question', 'Question:') !!}
						<p>{!! $homeText->question !!}</p>
					</div>

					<!-- Text Field -->
					<div class="form-group">
						{!! Form::label('answer', 'Answer:') !!}
						<p>{!! $homeText->answer !!}</p>
					</div>

					<!-- Updated At Field -->
					<div class="form-group">
						{!! Form::label('updated_at', 'Updated:') !!}
						<p>{!! $homeText->updated_at !!}</p>
					</div>
					
                    <a href="{!! route('faqs.index') !!}" class="btn btn-default">Back</a>
                </div>
            </div>
        </div>
    </div>
@endsection
