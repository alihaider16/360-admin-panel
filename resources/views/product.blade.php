<?php
$homeNav = '';
$productNav = ' active';
$designLabNav = '';
$teamNav = '';
$orderNav = '';
$faqsNav = '';
$contactNav = '';
?>
<!doctype html>
<html lang="en">

<head>
    @include('layouts.front_header')
</head>
<style>
/* #clip {
  position: absolute;
  clip: rect(0, 100px, 200px, 0);
  /* clip: shape(top, right, bottom, left); NB 'rect' is the only available option
} */
/*.product-sec .container{
	position:relative;
} */
/*Product style*/
.product_text {
    color: grey;
    border-bottom: 1px solid grey;
    padding-bottom: 10px;
    padding-top: 10px;
    cursor: pointer;
}

.product_text:hover {
    color: black;
    border-bottom: 1px solid black;
}

.menuTitle {
    margin-top: 10px;
}

.menu.filter ul {
    margin: 0;
    padding: 8px 0;
    list-style: none;
}

.menu.filter ul li {
    width: 100%;
    padding-top: 8px;
    padding-bottom: 8px;
    border-bottom: 1px solid rgba(0, 0, 0, 0.12);
}

.menu.filter ul li:hover,
.menu.filter ul li.actived {
    border-color: black;

}

.menu.filter ul li:hover *,
.menu.filter ul li.actived * {
    color: #000;
    fill: #000;
}

.menu.filter ul li a {
    display: block;
    width: 100%;
    height: 100%;
    color: grey;
    text-decoration: none;
    line-height: 26px;
}

.menu.filter ul li a:hover {
    text-decoration: none;
}

.menuTitle {
    display: block;
    font-size: 12px;
    text-transform: uppercase;
}

/*End*/
.side-crop {
    width: 100% !important;
    overflow: hidden !important;
    text-indent: 83px;
    object-fit: contain;
    margin-left: -40px;
}

.side-crop .prodicy-img.img {
    max-width: initial;
    /* Maybe optional. See note below */
}

.prodict-img {
    width: 100%;
    height: auto;
    overflow: hidden;
    margin-bottom: 20px;
}
</style>

<body>
    @include('layouts.front_nav')
    <!-- Content -->
    <div class="container">
        <!--our team -->
        <!-- Haeder -->
        <header class="header" id="header">
            <div class="row">
                <div class="col-12 col-sm-5">
                    <h1 class="mrg" style="margin-bottom: 0px;">KIT DESIGNER</h1>
                </div>
                <div class="col-7 d-none d-sm-block">
                    <ul class="process-step">
                        <li style="z-index:3;" class="">Step 1<span>Select Design</span></li>
                        <li style="z-index:2;" class="">Step 2<span>Edit Design</span></li>
                        <li style="z-index:1;" class="">Step 3<span>Completed Design</span></li>
                    </ul>
                </div>
            </div>
        </header>
        <!-- Haeder -->
        <div class="row">
            <!-- <div class="our-team">
               <div class="col-md-12">
                  <h2 class="text-center">Products</h2>
               </div>
            </div> -->
            <div class="container">
                <main style="margin-bottom:10%;">
                    <div class="row">
                        <!-- <div class="col-sm-3 col-12">
                            <nav class="menu filter">
                                <div>
                                    <p class="paragraph xs font black menuTitle">Categories</p>
                                    <ul>
                                        <li class="">
                                            <a href="#">All Prducts</a>
                                        </li>
                                        <li class="">
                                            <a href="#">Letterman Jackets</a>
                                        </li>
                                        <li class="">
                                            <a href="#">Bomber Jackets</a>
                                        </li>
                                        <li class="">
                                            <a href="#">Windbreaker Jackets</a>
                                        </li>
                                        <li class="">
                                            <a href="#">Letterman Sweaters</a>
                                        </li>
                                        <li class="">
                                            <a href="#">Hoodies</a>
                                        </li>
                                    </ul>
                                </div>
                            </nav>
                        </div> -->
                        <div class="col-12">
                            <div class="container"> <br>
                                <div class="row">
                                    <!-- <div class="col-xl-12 col-md-12 col-sm-12 col-12" style="text-align: right;">
          <h4 >2 SELECTED YOUR DESIGN:</h4>
        </div> -->
                                    <div class="row">
                                        @foreach($clothes as $cloth)
                                        <div class="col-sm-3 col-12">
                                            <a href="{!! route('configurator',['catID'=>$cloth->id]) !!}">
                                                <img class="kit-design prodict-img img-fluid"
                                                    src="{{ asset('img/t-shirts/') }}/{!! $cloth->thumb !!}_thumb.png">
                                                <button type="button" class="btn btn-light" style="width: 100%;">
                                                    <a href="{!! route('configurator',['catID'=>$cloth->id]) !!}"
                                                        style="color: black;text-decoration:none;">Customize Design <img
                                                            class="color_wheel"
                                                            src="{{ asset('images/') }}/color_wheel.png"></a>
                                                </button>
                                                <br>
                                                <h5>{!! $cloth->name !!}</h5>
                                                <h6 class="">From ${!!
                                                    \App\Http\Controllers\ClothesController::getMinPrice($cloth->id);
                                                    !!} each</h6>
                                            </a>
                                        </div>
                                        @endforeach
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </main>
                <?php /*<div class="row">
			   @foreach($clothes as $cloth)
                  <div class="col-md-2">
                     <div class="row product-sec">
                        <div class="col-12" style="overflow:hidden;">
                        	<a href="{!! route('configurator',['catID'=>$cloth->id]) !!}">
	                        	<div class="side-crop">
	                        		<img class="prodict-img" id="" src="{{ asset('img/t-shirts/') }}/{!! $cloth->base !!}_base.png" alt="{!! $cloth->name !!}" class="img-fluid circle-shape">
	                        	</div>
								<div class="our-team-sec" style="text-align:center;">
									<h5>{!! $cloth->name !!}</h5>
								</div>
							</a>
                        </div>
                     </div>
                  </div>
				@endforeach
               </div>*/ ?>
            </div>
        </div>
    </div>
    @include('layouts.front_footer')
</body>
@include('layouts.front_footerScript')

</html>