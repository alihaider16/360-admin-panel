<table id="example" class="table table-striped table-bordered dataTable" cellspacing="0" width="100%"
       role="grid" aria-describedby="example_info" style="width: 100%;">
    <thead>
    <tr role="row">
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Name: activate to sort column ascending" style="width: 137px;">ID</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Position: activate to sort column ascending" style="width: 215px;">Name</th>
        <th class="sorting" tabindex="0" aria-controls="example" rowspan="1" colspan="1"
            aria-label="Office: activate to sort column ascending" style="width: 100px;">Position</th>
    </tr>
    </thead>
    <tfoot>
    <tr>
        <th rowspan="1" colspan="1">ID</th>
        <th rowspan="1" colspan="1">Category ID</th>
        <th rowspan="1" colspan="1">Position</th>
    </tr>
    </tfoot>
    <tbody>
    <tr role="row" class="odd">
        <td class="">Ashton Cox</td>
        <td>Junior Technical Author</td>
        <td>San Francisco</td>
    </tr>
    <tr role="row" class="even">
        <td class="">Michael Silva</td>
        <td>Marketing Designer</td>
        <td>London</td>
    </tr>
    <tr role="row" class="odd">
        <td class="">Jackson Bradshaw</td>
        <td>Director</td>
        <td>New York</td>
    </tr>
    <tr role="row" class="even">
        <td class="">Olivia Liang</td>
        <td>Support Engineer</td>
        <td>Singapore</td>
    </tr>
    </tbody>
</table>