<div class="" id="p-step-form">
    <form class="m-t-b" method="post" action="" role="form">
        <div class="messages"></div>
        <div class="controls">
            <!--input Fields-->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Auto Show First Substep</label>
                        <input id="name" type="checkbox" name="name"  data-error="Position required">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Scroll Customise Control</label>
                        <input id="name" type="checkbox" name="name"  data-error="Position required">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Product Name/Price Heading</label>
                        <input id="name" type="checkbox" name="name"  data-error="Position required">
                        <p>Show product name and price heading above comtomise control</p>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="logo">Price Type</label>
                        <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                        <select class="form-control" id="logo" name="logo">
                            <option>A</option>
                            <option>B</option>
                            <option>C</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Replace Buy Button with "Request a Qoute"</label>
                        <input id="name" type="checkbox" name="name"  data-error="Position required">
                        <p>Require from with key 'QouteRequest'</p>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <!---End-->
            <div class="row">
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-send" value="Save Step">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="text-muted"><strong>*</strong> These fields are required.</p>
                </div>
            </div>
        </div>

    </form>
</div>