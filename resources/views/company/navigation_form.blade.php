<div class="" id="p-step-form">
    <form class="m-t-b" method="post" action="" role="form">
        <div class="messages"></div>
        <div class="controls">
            <!--input Fields-->
            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Auto Show First Substep</label>
                        <input id="name" type="checkbox" name="name"  data-error="Position required">
                        <p>Require from with key 'QouteRequest'</p>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="name">Scroll Customise Control</label>
                        <input id="name" type="checkbox" name="name"  data-error="Position required">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <hr>
            <!---End-->
            <div class="row">
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-send" value="Save Step">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="text-muted"><strong>*</strong> These fields are required.</p>
                </div>
            </div>
        </div>

    </form>
</div>