<div class="" id="category-form-toggle">
    <form id="category-form" class="m-t-b" method="post" action="contact.php" role="form">
        <div class="messages"></div>
        <div class="controls">
            <!--input Fields-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="add_social_share">Add This Social Share </label>
                        <input id="add_social_share" type="Area" name="add_social_share" class="form-control" placeholder="Description" required="required" data-error="Category required.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="add_pub">AddThis Pub ID</label>
                        <input id="add_pub" type="number" name="add_pub" class="form-control" placeholder="e.g 10"
                               required="required" data-error="Position required">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="add_social_share">Insert AddThis Script</label>
                        <input id="add_social_share" type="checkbox" name="add_social_share" placeholder="Description" required="required" data-error="Category required.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <!---End-->
            <div class="row">
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-send" value="Save Category Mapping">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="text-muted"><strong>*</strong> These fields are required.</p>
                </div>
            </div>
        </div>

    </form>
</div>