@extends('layouts.admin')
@section('content')
    <section class="content-header">
        <h1 class="pull-left">Company</h1>
    </section>

    <div class="content">
        <div class="clearfix"></div>

        @include('flash::message')

        <div class="clearfix"></div>

        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <!--Test Drive-->
                    <h3>Add Company</h3>
                    <h5>Edit Company</h5>
                    <hr>
                    <!--End-->
                    <!--Nav Tab-->
                    <ul class="nav nav-tabs" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" href="#product" role="tab" data-toggle="tab" aria-controls="Main">Main</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#category" role="tab" data-toggle="tab" aria-controls="Social Media">Social Media</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#product_step" role="tab" data-toggle="tab" aria-controls="Product Step">
                                Controls for Customise Page </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Product_field" role="tab" data-toggle="tab" aria-controls="Product field">Navigation
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="#Product_design" role="tab" data-toggle="tab"
                               aria-controls="Product Design">Cart Integration</a>
                        </li>
                    </ul>
                    <!--end-->
                    <!-- Tab panes -->
                    <div class="tab-content ">
                        <!--Product Tab Content-->
                        <div role="tabpanel" class="tab-pane fade in active" id="product">

                            <!--FormController Start-->
                            @include('company.product_tab_form')
                            <!--FormController End-->
                            <!--Table-->
                            @include('company.product_tab_table')
                            <!--End table-->

                        </div>
                        <!--End-->

                        <!--Category tab-contant-->
                        <div role="tabpanel" class="tab-pane fade" id="category">
                            <!--FormController Start-->
                        @include('company.social_media_form')
                        <!--FormController End-->
                            <!--Table-->
                        @include('company.social_media_table')
                            <!--End table-->

                        </div>
                        <!--end-->

                        <!--product_step tab-contant-->
                        <div role="tabpanel" class="tab-pane fade" id="product_step">
                            <!--FormController Start-->
                            @include('company.control_customize_form')
                            <!--FormController End-->
                            @include('company.control_customize_table')
                        </div>
                        <!--end-->

                        <!--product_step tab-contant-->
                        <div role="tabpanel" class="tab-pane fade" id="Product_field">

                            <!--FormController Start-->
                            @include('company.navigation_form')
                            <!--FormController End-->
                            @include('company.navigation_table')
                        </div>
                        <!--end-->


                        <!--product_Design tab-contant-->
                        <div role="tabpanel" class="tab-pane fade" id="Product_design">

                            <!--FormController Start-->
                            @include('company.cart_integration_form')
                            <!--FormController End-->
                            @include('company.cart_integration_table')
                        </div>
                        <!--end-->

                        <!-- End Main Tab Content-->
                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection