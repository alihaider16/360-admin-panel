<div class="" id="product-form-toggle">
    <form id="product-form" class="m-t-b" method="post" action="" role="form">

        <div class="messages"></div>

        <div class="controls">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input id="name" type="text" name="name" class="form-control"
                               placeholder="e.g Magnetize" required="required"
                               data-error="Firstname is required.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input id="email" type="email" name="email" class="form-control"
                               placeholder="e.g info@company.com" required="required" data-error="Email is required.">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <!--input Fields-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="logo">Logo</label>
                        <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                        <select class="form-control" id="logo" name="logo">
                            <option>A</option>
                            <option>B</option>
                            <option>C</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="setting">Setting</label>
                        <!-- <input id="category" type="text" name="category" class="form-control" placeholder="Please select your Category *" required="required" data-error="Category required."> -->
                        <select class="form-control" id="setting" name="setting">
                            <option>A</option>
                            <option>B</option>
                            <option>C</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <!---End-->
            <!--input Fields-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <input id="phone" type="number" name="phone" class="form-control" placeholder="e.g +923049018107"
                               required="required" data-error="">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="web_url">Web URL</label>
                        <input id="web_url" type="number" name="web_url" class="form-control" placeholder="e.g http://itswebsoft.com/"
                               required="required" data-error="required">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <!---End-->
            <!--input Fields-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="default">Default</label>
                        <!-- <input id="f_overlay" type="number" name="f_overlay" class="form-control" placeholder=""
                              required="required" data-error="">   -->
                        <select class="form-control" id="default" name="default">
                            <option>A</option>
                            <option>B</option>
                            <option>C</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="time_zone">Time Zone</label>
                        <select class="form-control" id="time_zone" name="time_zone">
                            <option>A</option>
                            <option>B</option>
                            <option>C</option>
                        </select>
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <!---End-->
            <!--input Fields-->
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="locale">Locale</label>
                        <select class="form-control" id="locale" name="locale ">
                            <option>Test</option>
                            <option>Test</option>
                            <option>Test</option>
                        </select>

                        <div class="help-block with-errors"></div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="active">Active</label>
                        <input id="active" type="checkbox" name="active">
                        <div class="help-block with-errors"></div>
                    </div>
                </div>
            </div>
            <!---End-->
            <div class="row">
                <div class="col-md-12">
                    <input type="submit" class="btn btn-success btn-send" value="Save">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p class="text-muted"><strong>*</strong> These fields are required.</p>
                </div>
            </div>
        </div>
    </form>
</div>