<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Design extends Model
{
    protected $fillable = [
        'name',
        'image',
        'prod_id',
        'price',
        'description'
    ];
}
