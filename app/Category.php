<?php

namespace App;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'image',
        'slug',
        'url_override',
        'position'
    ];
    public function products()
    {
        return $this->hasMany(Product::class);
    }
}
