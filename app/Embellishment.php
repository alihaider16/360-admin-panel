<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Embellishment extends Model
{
    protected $fillable=[

        'name',
        'file',
        'thumb'

    ];
}
