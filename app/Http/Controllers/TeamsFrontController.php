<?php

namespace App\Http\Controllers;

use App\Models\Teams;
use App\Repositories\TeamsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TeamsFrontController extends AppBaseController
{
    /** @var  PrintsRepository */
    private $teamsRepository;
	
    public function __construct(TeamsRepository $textRepo)
    {
        //$this->teamsRepository = $textRepo;
        //$this->middleware('auth');
    }

    /**
     * Display a listing of the Prints.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->teamsRepository->pushCriteria(new RequestCriteria($request));
        $text = $this->teamsRepository->all();

        return view('teams.index')
            ->with('textArr', $text);
    }
	
	public function indexHome(Request $request)
    {
        //$this->teamsRepository->pushCriteria(new RequestCriteria($request));
        $textArr = Teams::all();//$this->teamsRepository->all();

        return view('ourteam',compact('textArr'));
    }
    /**
     * Show the form for creating a new Prints.
     *
     * @return Response
     */
    public function create()
    {
        return view('teams.create');
    }

    /**
     * Store a newly created Prints in storage.
     *
     * @param CreatePrintsRequest $request
     *
     * @return Response
     */
    public function store(CreateTeamsRequest $request)
    {
        $input = $request->all();
        $team = $this->teamsRepository->create($input);

        $location = public_path() . '/tmp/' . $input["image"];
        rename($location, public_path() . '/img/team/' . $input["image"]);

        Flash::success('Team Member saved successfully.');

        return redirect(route('teams.index'));
    }

    /**
     * Display the specified Prints.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $teams = $this->teamsRepository->findWithoutFail($id);

        if (empty($teams)) {
            Flash::error('Data not found');

            return redirect(route('teams.index'));
        }

        return view('teams.show')->with('teams', $teams);
    }

    /**
     * Show the form for editing the specified Prints.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $text = $this->teamsRepository->findWithoutFail($id);

        if (empty($text)) {
            Flash::error('Data not found');

            return redirect(route('teams.index'));
        }

        return view('teams.edit')->with('homeText', $text);
    }

    /**
     * Update the specified Prints in storage.
     *
     * @param  int $id
     * @param UpdatePrintsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTeamsRequest $request)
    {
        $text = $this->teamsRepository->findWithoutFail($id);

        if (empty($text)) {
            Flash::error('Data not found');

            return redirect(route('teams.index'));
        }

        $prints = $this->teamsRepository->update($request->all(), $id);

        Flash::success('Data updated successfully.');

        return redirect(route('teams.index'));
    }
	
	 /**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prints = $this->teamsRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Team Member not found');

            return redirect(route('teams.index'));
        }

        $this->teamsRepository->delete($id);

        Flash::success('Team Member deleted successfully.');

        return redirect(route('teams.index'));
    }

    /**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    

    public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.png';
            $request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);
            return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }

    }
}
