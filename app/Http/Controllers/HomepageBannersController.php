<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateHomepageBannersRequest;
use App\Http\Requests\UpdateHomepageBannersRequest;
use App\Repositories\HomepageBannersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class HomepageBannersController extends AppBaseController
{
    /** @var  PrintsRepository */
    private $homepageBannersRepository;

    public function __construct(HomepageBannersRepository $homepageBannersRepo)
    {
        $this->homepageBannersRepository = $homepageBannersRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Prints.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
		/*echo "<pre>";
		print_r($request);
		echo "</pre>";
		return;*/
        $this->homepageBannersRepository->pushCriteria(new RequestCriteria($request));
        $homepageBanners = $this->homepageBannersRepository->all();

        return view('homepage_banners.index')
            ->with('homepage_banners', $homepageBanners);
    }

    /**
     * Show the form for creating a new Prints.
     *
     * @return Response
     */
    public function create()
    {
        return view('homepage_banners.create');
    }

    /**
     * Store a newly created Prints in storage.
     *
     * @param CreatePrintsRequest $request
     *
     * @return Response
     */
    public function store(CreateHomepageBannersRequest $request)
    {
        $input = $request->all();
        $homepageBanners = $this->homepageBannersRepository->create($input);

        $location = public_path() . '/tmp/' . $input["image"];
        rename($location, public_path() . '/img/banners/' . $input["image"]);

        Flash::success('Banner Saved successfully.');

        return redirect(route('homepage_banners.index'));
    }

    /**
     * Display the specified Prints.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $homepageBanners = $this->homepageBannersRepository->findWithoutFail($id);

        if (empty($homepageBanners)) {
            Flash::error('Banner not found');

            return redirect(route('homepage_banners.index'));
        }

        return view('homepage_banners.show')->with('homepage_banners', $homepageBanners);
    }

    /**
     * Show the form for editing the specified Prints.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $homepageBanners = $this->homepageBannersRepository->findWithoutFail($id);

        if (empty($homepageBanners)) {
            Flash::error('Banner not found');

            return redirect(route('homepage_banners.index'));
        }

        return view('homepage_banners.edit')->with('homepage_banners', $homepageBanners);
    }

    /**
     * Update the specified Prints in storage.
     *
     * @param  int $id
     * @param UpdatePrintsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateHomepageBannersRequest $request)
    {
        $homepageBanners = $this->homepageBannersRepository->findWithoutFail($id);

        if (empty($homepageBanners)) {
            Flash::error('Banner not found');

            return redirect(route('homepage_banners.index'));
        }

        $homepageBanners = $this->homepageBannersRepository->update($request->all(), $id);

        Flash::success('Banner updated successfully.');

        return redirect(route('homepage_banners.index'));
    }

    /**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $homepageBanners = $this->homepageBannersRepository->findWithoutFail($id);

        if (empty($homepageBanners)) {
            Flash::error('Banner not found');

            return redirect(route('homepage_banners.index'));
        }

        $this->homepageBannersRepository->delete($id);

        Flash::success('Banner deleted successfully.');

        return redirect(route('homepage_banners.index'));
    }

    public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.png';
            $request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);
            return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }
    }
}