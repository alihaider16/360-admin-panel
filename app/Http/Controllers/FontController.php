<?php

namespace App\Http\Controllers;

use App\Font;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class FontController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $fonts = Font::all();
        return view('fonts.index',compact('fonts'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('fonts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $font = Font::create($request->all());
        Flash::success($font->name." added successfully");
        return redirect(route('fonts.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $font = Font::findOrFail($id);
        return view('fonts.edit',compact('font'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $font = Font::findOrFail($id);
        $font->name = $request->name;
        $font->save();

        Flash::success($font->name." updated successfully");
        return redirect(route('fonts.index'));

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $font = Font::findOrFail($id);
        $font->delete();
        Flash::success($font->name.' deleted successfully');
        return redirect(route('fonts.index'));
    }
}
