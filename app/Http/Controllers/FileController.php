<?php

namespace App\Http\Controllers;

use App\Models\File;
use App\Repositories\UsersRepository;
use Chumper\Zipper\Zipper;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades;
use Laracasts\Flash\Flash;
use Illuminate\Support\Facades\DB;

class FileController extends Controller
{
    public function __construct(UsersRepository $usersRepo)
    {
        $this->middleware('auth');
    }
    public function index()
    {
        $files = DB::table('files')->get();
        return view('file.index',compact('files'));
    }
    public function create(Request $request)
    {
        $dbFile = new File();
        if ($request->has('image'))
        {
            $file = $request->file('image');
            $extention = $file->getClientOriginalExtension();
            if ($extention != 'svg') {
                if ($extention != 'zip') {
                    $fileName = time() . '.' . $extention;
                    $file->move('images/Files/', $fileName);
                    $orignalName = $file->getClientOriginalName();
                    $dbFile->image = $fileName;
                    $dbFile->name = $orignalName;
                    $dbFile->save();
                    Flash::success("File added successfully");
                    return redirect()->back();
                }
            }
            if ($extention == 'zip')
            {
                $zipper = new \Chumper\Zipper\Zipper;
                $orignalName = $file->getClientOriginalName();
                $name = basename($orignalName,'.zip');
                $fileName = rand(10000,50000).time().'.'.$extention;
                $move = $file->move('images/Files/', $fileName);
                $baseName = $move->getBasename();

                $zipper->make(public_path('images/Files/'.$baseName))->extractTo(public_path('images/Files/'.$name));
                $zipper->close();
                $path = public_path()."/images/Files/".$name;
                $files = Facades\File::allFiles($path);
                foreach ($files as $item)
                {
                    $content = $item->getFilename();
                    $getExt = pathinfo($content, PATHINFO_EXTENSION);
                    if ($getExt == 'mtl'){
                        $dbFile->mtl = $content;
                        $dbFile->save();
                    }
                    if ($getExt == 'obj') {
                        $id = DB::table('files')->orderBy('id','desc')->pluck('id')->first();
                        $selectDB = File::findOrfail($id);
                        $selectDB->name = $name;
                        $selectDB->image = $content;
                        $selectDB->update();
                        Flash::success("File is added");
                        return redirect()->back();
                    }
                }
            }
            else{
                try {
                    $location = public_path() . '/images/Files/';
                    if (!file_exists($location)) {
                        mkdir($location, 0777, true);
                    }
                    $orignalName = $file->getClientOriginalName();
                    $name = $orignalName;
                    $dbFile->name = $name;
                    $request->image->move($location, $name);
                    session(['filename' => $location . $name]);
                    $file = basename($location . $name);
                    $path = public_path() . '/images/Files/' . $name;
                    $svg_file = file_get_contents($path);
                    $find_string   = '<svg';
                    $position = strpos($svg_file, $find_string);
                    $svg_file_new = substr($svg_file, $position);
                    $dbFile->image = $svg_file_new;
                    $dbFile->save();
                } catch (Exception $e) {
                    return $e;
                }
                $dbFile->save();
                Flash::success("File added successfully");
                return redirect()->back();
            }
        } else{
            return redirect()->back()->withErrors(['message','Something goes wrong!!']);
        }
    }
}
