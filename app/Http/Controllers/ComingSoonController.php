<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateHomepageBannersRequest;
use App\Http\Requests\UpdateHomepageBannersRequest;
use App\Repositories\HomepageBannersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class ComingSoonController extends AppBaseController
{
    /** @var  PrintsRepository */
    //private $homepageBannersRepository;
	/*
    public function __construct()
    {
        //$this->homepageBannersRepository = $homepageBannersRepo;
        //$this->middleware('auth');
    }
*/
    /**
     * Display a listing of the Prints.
     *
     * @param Request $request
     * @return Response
     */
    public function index()
    {
		/*echo "<pre>";
		print_r($request);
		echo "</pre>";
		return;*/
        //$this->homepageBannersRepository->pushCriteria(new RequestCriteria($request));
        //$homepageBanners = $this->homepageBannersRepository->all();

        return view('coming_soon.index')
            ->with('title', $_REQUEST['title']);
    }
}