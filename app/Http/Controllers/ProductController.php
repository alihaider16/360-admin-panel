<?php

namespace App\Http\Controllers;

use App\Category;
use App\Models\Product;
use App\Models\ProductStep;
use App\Models\RelatedProduct;
use Dompdf\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Laracasts\Flash\Flash;
use OpenApiFixures\Hello;
use phpDocumentor\Reflection\DocBlock\Description;
use Svg\Tag\Image;

class ProductController extends Controller
{
    public function index()
    {
        $files = \App\Models\File::all();
        $categories = Category::all();
        $products = Product::get();
        $related_products = RelatedProduct::get();
        return view('product.index',compact('categories','products','related_products','files'));
    }
    public function store(Request $request)
    {
        $allReq = $request->all();
        $product = new Product();
        $product->name = $allReq['name'];
        $product->url_slug = $allReq['url_slug'];
        $product->cat_id = $allReq['category'];
        $product->cart_price = $allReq['cart_price'];
        $product->min_quantity = $allReq['minimum_quantity'];
        $product->prod_weight = $allReq['product_weight'];

        $product->font_overlay = $allReq['font_overly'];

        $product->front_view_name = $allReq['front_view_name'];

        $product->back_overlay = $allReq['back_overlay'];

        $product->back_view_name = $allReq['back_view'];

        $product->list_image = $allReq['list_image'];
        try {
            $location = public_path() . '/new/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $name = time() . '.svg';

            $request->threeD_modal->move($location, $name);
            session(['filename' => $location . $name]);
            $file = basename($location . $name);
            $path = public_path() . '/new/' . $name;
            $svg_file = file_get_contents($path);
            $find_string   = '<svg';
            $position = strpos($svg_file, $find_string);
            $svg_file_new = substr($svg_file, $position);
            $product->threeD_model = $svg_file_new;
        } catch (Exception $e) {
            return $e;
        }

        $product->back_material_color = $allReq['back_material'];
        $product->cameras = $allReq['camera'];
        $product->form_rply_cam_override = $allReq['camera_override'];
        $product->position_x = $allReq['position_offset_x'];
        $product->position_y = $allReq['position_offset_y'];
        $product->position_z = $allReq['position_offset_z'];
        $product->scale = $allReq['scale'];
        $product->light_brightness = $allReq['light_brightness'];
        $product->svg_dimention_x = $allReq['svg_dimention_1'];
        $product->svg_dimention_y = $allReq['svg_dimention_2'];
        $product->height_basket_page = $allReq['height_baskit'];
        $product->def_togler_color = $allReq['befault_colour'];

//        $product->pdf_overlay_img = $allReq['b_overlay'];

        $product->position = $allReq['position'];
        $product->save();
        return redirect()->back()->with('message','product insert successfully');
    }
    public function edit($id)
    {
        $product_steps = Product::findOrfail($id)->productstep;
        $files = \App\Models\File::all();
        $categories = Category::all();
        $editProduct = Product::findOrfail($id);
        return view('product.edit',compact('editProduct','categories','files','product_steps'));
    }
    public function update($id, Request $request)
    {
        $allReq = $request->all();
        $product = Product::findOrfail($id);
        $product->name = $allReq['name'];
        $product->url_slug = $allReq['url_slug'];
        $product->cat_id = $allReq['category'];
        $product->cart_price = $allReq['cart_price'];
        $product->min_quantity = $allReq['minimum_quantity'];
        $product->prod_weight = $allReq['product_weight'];

        //$product->font_overlay = $allReq['font_overly'];

        $product->front_view_name = $allReq['front_view_name'];

//        $product->back_overlay = $allReq['back_overlay'];

        $product->back_view_name = $allReq['back_view'];

//        $product->list_image = $allReq['list_image'];
        if ($request->has('threeD_modal')) {
            try {
                $location = public_path() . '/new/';

                if (!file_exists($location)) {
                    mkdir($location, 0777, true);
                }

                $name = time() . '.svg';

                $request->threeD_modal->move($location, $name);
                session(['filename' => $location . $name]);
                $file = basename($location . $name);
                $path = public_path() . '/new/' . $name;
                $svg_file = file_get_contents($path);
                $find_string = '<svg';
                $position = strpos($svg_file, $find_string);
                $svg_file_new = substr($svg_file, $position);
                $product->threeD_model = $svg_file_new;
            } catch (Exception $e) {
                return $e;
            }
        }

        $product->back_material_color = $allReq['back_material'];
        $product->cameras = $allReq['camera'];
        $product->form_rply_cam_override = $allReq['camera_override'];
        $product->position_x = $allReq['position_offset_x'];
        $product->position_y = $allReq['position_offset_y'];
        $product->position_z = $allReq['position_offset_z'];
        $product->scale = $allReq['scale'];
        $product->light_brightness = $allReq['light_brightness'];
        $product->svg_dimention_x = $allReq['svg_dimention_1'];
        $product->svg_dimention_y = $allReq['svg_dimention_2'];
        $product->height_basket_page = $allReq['height_baskit'];
        $product->def_togler_color = $allReq['befault_colour'];

//        $product->pdf_overlay_img = $allReq['b_overlay'];

        $product->position = $allReq['position'];
        $product->update();
        Flash::success("Product is updated!");
        return redirect()->route('product.index');
    }
    public function cat_store(Request $request)
    {
        if ($request->has('image')) {
            $image = $request->file('image');
            $extention = $image->getClientOriginalExtension();
            $fileName = time().'.'.$extention;
            $image->move('images/categories/',$fileName);
            $form_data = [
                'image'        => $fileName,
                'name'         => $request->name,
                'slug'         => $request->url_slug,
                'url_override' => $request->url_override,
                'position'     => $request->position,
            ];
            $category = Category::create($form_data);
            if ($category)
            {
                Flash::success('Category is Submitted');
                return redirect()->back();
            }
        }
        else{
            $form_data = [
                'image'        => '',
                'name'         => $request->name,
                'slug'         => $request->slug,
                'url_override' => $request->url_override,
                'position'     => $request->position,
            ];
            $category = Category::create($form_data);
            if ($category)
            {
                Flash::success('Category is Submitted');
                return redirect()->back();
            }
        }
    }
    public function cat_edit($id)
    {
        $categories = Category::all();
        $cat_edit = Category::findOrfail($id);
        return view('product.category_edit_form',compact('cat_edit','categories'));
    }
    public function cat_update(Request $request, $id)
    {
        $category = Category::findOrfail($id);
        if ($request->has('image')) {
            $image = $request->file('image');
            $extention = $image->getClientOriginalExtension();
            $fileName = time() . '.' . $extention;
            $image->move('images/categories/', $fileName);
            $category->image = $fileName;
            $category->name = $request->name;
            $category->slug = $request->url_slug;
            $category->url_override = $request->url_override;
            $category->position     = $request->position;
            $category->update();
            Flash::success('Category is Updated!!');
            return redirect()->back();
        }else {
            $category->name = $request->name;
            $category->slug = $request->url_slug;
            $category->url_override = $request->url_override;
            $category->position     = $request->position;
            $category->update();
            Flash::success('Category is Updated!!');
            return redirect()->route('product.index');
        }
    }
    public function product_related(Request $request)
    {
        $allReq = $request->all();
        $validate = $request->validate([
            'parent_product'  => 'required',
            'related_product' => 'required'
        ]);
        if (!$validate){
            Flash::error("Something goes wrong!!");
            return redirect()->back();
        }
        else {
            $product = new RelatedProduct();
            $product->parent_product = $allReq['parent_product'];
            $product->related_product = $allReq['related_product'];
            $product->save();
            Flash::success("Product is related successfully!!");
            return redirect()->back();
        }
    }
    public static function getRelatedName($id)
    {
        $related = Product::where('id',$id)->first();
        if ($related)
        {
            return $related->name;
        }
        else{
            return;
        }
    }
    public static function getRelatedImage($id)
    {
        $related = Product::where('id',$id)->first();
        if ($related)
        {
            return $related->threeD_model;
        }
        else{
            return;
        }
    }
}
