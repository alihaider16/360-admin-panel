<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateFaqRequest;
use App\Http\Requests\UpdateFaqRequest;
use App\Repositories\FaqsRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class FaqsController extends AppBaseController
{
    /** @var  PrintsRepository */
    private $faqRepository;
	
    public function __construct(FaqsRepository $faqRepo)
    {
        $this->faqRepository = $faqRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Prints.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->faqRepository->pushCriteria(new RequestCriteria($request));
        $faq = $this->faqRepository->all();

        return view('faqs.index')
            ->with('textArr', $faq);
    }
	
	public function indexHome(Request $request)
    {
        $this->faqRepository->pushCriteria(new RequestCriteria($request));
        $faq = $this->faqRepository->all();
		//return view('welcome',compact('banners','text','team'));
        return view('faqs',compact('faq'));
    }

    /**
     * Show the form for creating a new Prints.
     *
     * @return Response
     */
    public function create()
    {
        return view('faqs.create');
    }

    /**
     * Store a newly created Prints in storage.
     *
     * @param CreatePrintsRequest $request
     *
     * @return Response
     */
    public function store(CreateFaqRequest $request)
    {
        $input = $request->all();
        $faq = $this->faqRepository->create($input);

        Flash::success('FAQ saved successfully.');

        return redirect(route('faqs.index'));
    }

    /**
     * Display the specified Prints.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $text = $this->faqRepository->findWithoutFail($id);

        if (empty($text)) {
            Flash::error('Data not found');

            return redirect(route('faqs.index'));
        }

        return view('faqs.show')->with('homeText', $text);
    }

    /**
     * Show the form for editing the specified Prints.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $text = $this->faqRepository->findWithoutFail($id);

        if (empty($text)) {
            Flash::error('FAQ not found');

            return redirect(route('faqs.index'));
        }

        return view('faqs.edit')->with('homeText', $text);
    }

    /**
     * Update the specified Prints in storage.
     *
     * @param  int $id
     * @param UpdatePrintsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateFaqRequest $request)
    {
        $text = $this->faqRepository->findWithoutFail($id);

        if (empty($text)) {
            Flash::error('Data not found');

            return redirect(route('faqs.index'));
        }

        $prints = $this->faqRepository->update($request->all(), $id);

        Flash::success('FAQ updated successfully.');

        return redirect(route('faqs.index'));
    }
	/**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prints = $this->faqRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('FAQ not found');

            return redirect(route('teams.index'));
        }

        $this->faqRepository->delete($id);

        Flash::success('FAQ deleted successfully.');

        return redirect(route('teams.index'));
    }
    /**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    

    public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.png';
            $request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);
            return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }

    }
}
