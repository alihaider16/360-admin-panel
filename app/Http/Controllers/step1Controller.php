<?php

namespace App\Http\Controllers;

/*use App\Http\Requests\CreateHomepageBannersRequest;
use App\Http\Requests\UpdateHomepageBannersRequest;
use App\Repositories\HomepageBannersRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;
*/
use PDF;
use App\Design;
use App\Font;
use App\Models\DesignLayers;
use App\Models\Clothes;
use App\Models\Collars;
use App\Models\Alphabets;
use App\Models\Symbols;
use App\Models\EditableLogos;
use App\Models\Color;
use App\Embellishment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class step1Controller extends AppBaseController
{
    /** @var  PrintsRepository */
    //private $homepageBannersRepository;
	/*
    public function __construct()
    {
        //$this->homepageBannersRepository = $homepageBannersRepo;
        //$this->middleware('auth');
    }
*/
    /**
     * Display a listing of the Prints.
     *
     * @param Request $request
     * @return Response
     */
    public function index()
    {
		$productID = $_REQUEST['designID'];
        $embs = '';
        if(isset($_REQUEST['emb']))
        {
            $embs = Embellishment::where('id',$_REQUEST['emb'])->get();
		}
        $colors = Color::all();
        $fonts = Font::all();
		$alphabets = Alphabets::all();
		$symbols = Symbols::all();
		$elogos = EditableLogos::all();
		$products = Design::where('id',$productID)->get();
        $cloth = Clothes::where('id',$products[0]->prod_id)->get();
        $collars = Collars::where('category',$products[0]->prod_id)->get();
        $collar = count($collars);
        $layers = DesignLayers::where([['design_id','=',$productID],['layer_title','!=', '']])->get();
        return view('step1-addColor',compact('colors','cloth','products','alphabets','symbols','elogos','fonts','collar','collars','embs','layers'));
    }
	
	public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.png';
            $request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            rename($location . $name, public_path().'/img/custom_img/'. $name);
            $file = basename($location . $name);
            return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }
    }

    public function downloadPDF(Request $request)
    {   
        $input = $request->all();
        $product = Design::where('id',$input['svgData'])->get();
        $pdf = PDF::loadView('pdfView',compact('input','product'));
        return $pdf->download('design.pdf');
        //return $pdf->open('design.pdf');
        //return $pdf->stream();
    }
}