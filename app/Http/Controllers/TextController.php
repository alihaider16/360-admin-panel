<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateTextRequest;
use App\Http\Requests\UpdateTextRequest;
use App\Repositories\TextRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class TextController extends AppBaseController
{
    /** @var  PrintsRepository */
    private $textRepository;
	
    public function __construct(TextRepository $textRepo)
    {
        $this->textRepository = $textRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the Prints.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->textRepository->pushCriteria(new RequestCriteria($request));
        $text = $this->textRepository->all();

        return view('text.index')
            ->with('textArr', $text);
    }

    /**
     * Show the form for creating a new Prints.
     *
     * @return Response
     */
    public function create()
    {
        return view('text.create');
    }

    /**
     * Store a newly created Prints in storage.
     *
     * @param CreatePrintsRequest $request
     *
     * @return Response
     */
    public function store(CreateTextRequest $request)
    {
        $input = $request->all();
        //$prints = $this->printsRepository->create($input);

        $location = public_path() . '/tmp/' . $input["image"];
        rename($location, public_path() . '/img/templates/' . $input["image"]);

        Flash::success('Prints saved successfully.');

        return redirect(route('text.index'));
    }

    /**
     * Display the specified Prints.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $text = $this->textRepository->findWithoutFail($id);

        if (empty($text)) {
            Flash::error('Data not found');

            return redirect(route('text.index'));
        }

        return view('text.show')->with('homeText', $text);
    }

    /**
     * Show the form for editing the specified Prints.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $text = $this->textRepository->findWithoutFail($id);

        if (empty($text)) {
            Flash::error('Data not found');

            return redirect(route('text.index'));
        }

        return view('text.edit')->with('homeText', $text);
    }

    /**
     * Update the specified Prints in storage.
     *
     * @param  int $id
     * @param UpdatePrintsRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateTextRequest $request)
    {
        $text = $this->textRepository->findWithoutFail($id);

        if (empty($text)) {
            Flash::error('Data not found');

            return redirect(route('text.index'));
        }

        $prints = $this->textRepository->update($request->all(), $id);

        Flash::success('Data updated successfully.');

        return redirect(route('text.index'));
    }

    /**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    

    public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.png';
            $request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);
            return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }

    }
}
