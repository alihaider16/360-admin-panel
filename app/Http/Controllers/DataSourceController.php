<?php

namespace App\Http\Controllers;

use App\Models\Color;
use App\Models\DataSource;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class DataSourceController extends Controller
{
    public function index()
    {
        $dataSrc = DataSource::all();
        return view('data_source.index',compact('dataSrc'));
    }
    public function store(Request $request)
    {
        $allReq = $request->all();
        $dataSrc = new DataSource();
        $dataSrc->name = $allReq['name'];
        $dataSrc->type = $allReq['type'];
        $save = $dataSrc->save();
        if ($save){
            Flash::success('DataSource is added successfully');
            return redirect()->back();
        }
        else{
            Flash::error("Something goes wrong!!");
            return redirect()->back();
        }
    }
    public function edit($id)
    {
        $colours = Color::where('datasource_id',$id)->orderBy('id','desc')->get();
        $dataSrc = DataSource::findOrfail($id);
        return view('data_source.edit',compact('dataSrc','colours'));
    }
    public function update($id, Request $request)
    {
        $dataSrc = DataSource::findOrfail($id);
        $dataSrc->name = $request->name;
        $dataSrc->type = $request->type;
        $dataSrc->update();
        Flash::success("DataSource is updated!");
        return redirect()->back();
    }
    public function storeColour(Request $request)
    {
        $colours = new Color();
        $colours->name = $request->text;
        $colours->color = $request->value;
        $colours->datasource_id = $request->id;
        $colours->save();
        Flash::success("colour is added successfully");
        return redirect()->back();
    }
}
