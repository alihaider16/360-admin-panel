<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateEditableLogosRequest;
use App\Http\Requests\UpdateEditableLogosRequest;
use App\Repositories\EditableLogosRepository;
use App\Http\Controllers\AppBaseController;
use Illuminate\Http\Request;
use Flash;
use Prettus\Repository\Criteria\RequestCriteria;
use Response;

class EditableLogosController extends AppBaseController
{
    /** @var  EditableLogosRepository */
    private $EditableLogosRepository;

    public function __construct(EditableLogosRepository $printsRepo)
    {
        $this->EditableLogosRepository = $printsRepo;
        $this->middleware('auth');
    }

    /**
     * Display a listing of the editable_logos.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $this->EditableLogosRepository->pushCriteria(new RequestCriteria($request));
        $prints = $this->EditableLogosRepository->all();

        return view('editable_logos.index')
            ->with('prints', $prints);
    }

    /**
     * Show the form for creating a new editable_logos.
     *
     * @return Response
     */
    public function create()
    {
        return view('editable_logos.create');
    }

    /**
     * Store a newly created Prints in storage.
     *
     * @param CreateEditableLogosRequest $request
     *
     * @return Response
     */
    public function store(CreateEditableLogosRequest $request)
    {
        $input = $request->all();
        $prints = $this->EditableLogosRepository->create($input);

        $location = public_path() . '/tmp/' . $input["logo"];
        rename($location, public_path() . '/img/editable_logos/' . $input["logo"]);

        Flash::success('Editable Logo saved successfully.');

        return redirect(route('editable_logos.index'));
    }

    /**
     * Display the specified editable_logos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $prints = $this->EditableLogosRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Editable Logo not found');

            return redirect(route('editable_logos.index'));
        }

        return view('editable_logos.show')->with('prints', $prints);
    }

    /**
     * Show the form for editing the specified editable_logos.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $prints = $this->EditableLogosRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Editable Logo not found');

            return redirect(route('editable_logos.index'));
        }

        return view('editable_logos.edit')->with('prints', $prints);
    }

    /**
     * Update the specified Prints in storage.
     *
     * @param  int $id
     * @param UpdateEditableLogosRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateEditableLogosRequest $request)
    {
        $prints = $this->EditableLogosRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Editable Logo not found');

            return redirect(route('editable_logos.index'));
        }

        $prints = $this->EditableLogosRepository->update($request->all(), $id);

        Flash::success('Editable Logo updated successfully.');

        return redirect(route('editable_logos.index'));
    }

    /**
     * Remove the specified Prints from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $prints = $this->EditableLogosRepository->findWithoutFail($id);

        if (empty($prints)) {
            Flash::error('Editable Logo not found');

            return redirect(route('editable_logos.index'));
        }

        $this->EditableLogosRepository->delete($id);

        Flash::success('Editable Logo deleted successfully.');

        return redirect(route('editable_logos.index'));
    }

    public function uploade(Request $request)
    {
        try {
            $location = public_path() . '/tmp/';

            if (!file_exists($location)) {
                mkdir($location, 0777, true);
            }

            $files = glob($location . '*');
            foreach ($files as $file) {
                if (is_file($file))
                    unlink($file);
            }
            $name = time() . '.svg';
            $request->myfile->move($location, $name);
            session(['filename' => $location . $name]);
            // rename($location . $name, public_path().'/images/'. $name);
            $file = basename($location . $name);
            return "" . $file;
            // return "TEsted";
        } catch (Exception $e) {
            return $e;
        }
    }
}
