<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class NewCategoryController extends Controller
{
    public function index()
    {
        $categories = Category::all();
        return view('new_category.index',compact('categories'));
    }
    public function create(Request $request)
    {
        if ($request->has('image')) {
            $image = $request->file('image');
            $extention = $image->getClientOriginalExtension();
            $fileName = time().'.'.$extention;
            $image->move('images/categories/',$fileName);
            $form_data = [
                'image'        => $fileName,
                'name'         => $request->name,
                'slug'         => $request->url_slug,
                'url_override' => $request->url_override,
                'position'     => $request->position,
            ];
            $category = Category::create($form_data);
            if ($category)
            {
                Flash::success('Category is Submitted');
                return redirect()->back();
            }
        }
        else{
            $form_data = [
                'image'        => '',
                'name'         => $request->name,
                'slug'         => $request->slug,
                'url_override' => $request->url_override,
                'position'     => $request->position,
            ];
            $category = Category::create($form_data);
            if ($category)
            {
                Flash::success('Category is Submitted');
                return redirect()->back();
            }
        }
    }
}
