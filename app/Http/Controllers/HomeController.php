<?php

namespace App\Http\Controllers;

use App\Colors;
use App\Models\Clothes;
use App\Models\Teams;
use App\Models\Text;
use App\Models\HomepageBanners;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$path = public_path() . '\img\banners';
        $team = Teams::all();
		$text = Text::all();
		$banners = HomepageBanners::all();
		$products = Clothes::all();
        return view('welcome',compact('banners','text','team','products'));
    }
    
    public function productsHome()
	{
		$clothes = Clothes::all();
        return view('product',compact('clothes'));
	}

    public function orderHome()
	{
		return view('order');
    }
    
    public function contactHome()
	{
		return view('contact');
    }
    
    private static function getFiles($path)
    {
        if (is_dir($path)) {
            $files = array_diff(scandir($path), array('.', '..'));
        } else {
            $files = [];
        }
        return $files;
    }
}