<?php

namespace App\Http\Controllers;

use App\Models\ProductStep;
use Illuminate\Http\Request;
use Laracasts\Flash\Flash;

class ProductStepController extends Controller
{
    public function store(Request $request)
    {
        $reqAll = $request->all();
        $step = new ProductStep();

        $step->parent_id = $reqAll['parent_step'];
        $step->name = $reqAll['name'];
        $step->product_id = $reqAll['product_id'];
        $step->position = $reqAll['position'];
        $step->save();
        Flash::success("Product step is added");
        return redirect()->back();
    }
    public function edit($id)
    {
        $editField = ProductStep::findOrfail($id);
        return view('product.product_edit_field_step',compact('editField'));
    }
    public function update($id, Request $request)
    {
        $reqAll = $request->all();
        $editField = ProductStep::findOrfail($id);
        $editField->parent_id = $reqAll['parent_step'];
        $editField->name = $reqAll['name'];
        $editField->position = $reqAll['position'];
        $editField->update();
        Flash::success("Product step is updated");
        return redirect()->back();
    }
    public function showProductField($id)
    {
        $parent_id = ProductStep::where('id',$id)->first();
        return view('product.add_product_field_step',compact('parent_id'));
    }
    public function storeProductField(Request $request)
    {
        $reqAll = $request->all();
        $step = new ProductStep();

        $step->parent_id = $reqAll['parent_step'];
        $step->name = $reqAll['name'];
        $step->product_id = $reqAll['product_id'];
        $step->position = $reqAll['position'];
        $step->save();
        Flash::success("Product step is added");
        return redirect()->back();
    }
    public function editProductField($id)
    {
        $editStepField = ProductStep::findOrfail($id);
        return view('product.edit_product_field_step',compact('editStepField'));
    }
    public function updateProductField($id, Request $request)
    {
        $reqAll = $request->all();
        $editField = ProductStep::findOrfail($id);
        $editField->parent_id = $reqAll['parent_step'];
        $editField->name = $reqAll['name'];
        $editField->position = $reqAll['position'];
        $editField->update();
        Flash::success("Product step is updated");
        return redirect()->back();
    }
    public static function getParentName($id)
    {
        $name = ProductStep::findOrfail($id);
        if (!empty($name))
        {
            return $name->name;
        }
        else{
            return;
        }
    }
}
