<?php


namespace App\Models;


use App\Category;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = "products";
    public function category()
    {
        return $this->belongsTo(Category::class,'cat_id');
    }
    public function relatedproducts()
    {
        return $this->hasMany(RelatedProduct::class);
    }
    public function productstep()
    {
        return $this->hasMany(ProductStep::class);
    }
}