<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ProductStep extends Model
{
    protected $table = "product_step";

    public function products()
    {
        return $this->belongsTo(Product::class,'product_id');
    }
}
