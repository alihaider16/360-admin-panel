<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RelatedProduct extends Model
{
    protected $table = "related_products";
    public function product()
    {
        return $this->belongsTo(Product::class,'parent_product','id');
    }
}
