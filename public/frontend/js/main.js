jQuery( document ).on("ready", function( event ) {
"use strict"
	event.preventDefault();
     // ---------- Banner Slider ---------- // 
    jQuery('#tp-banner').show().revolution({
        dottedOverlay:"none",
        delay:10000,
        startwidth:1170,
        startheight:570,
        navigationType:"false",
        navigationArrows:"solo",
        navigationStyle:"preview4",                                             
        keyboardNavigation:"on",                        
        shadow:0,
        fullWidth:"off",
        fullScreen:"off",
        shuffle:"off",                      
        autoHeight:"off",                       
        forceFullWidth:"off"  
    });

    // ---------- Banner Slider ---------- // 
    jQuery('#tp-banner2').show().revolution({
        dottedOverlay:"none",
        delay:10000,
        startwidth:1170,
        startheight:570,
        navigationType:"false",
        navigationArrows:"solo",
        navigationStyle:"preview4",                                             
        keyboardNavigation:"on",                        
        shadow:0,
        fullWidth:"off",
        fullScreen:"on",
        shuffle:"off",                      
        autoHeight:"on",                       
        forceFullWidth:"off"  
    });
   // ---------- slide menu ---------- //
    $('.menu-link').bigSlide();

    // ---------- sticky navbar ---------- //
    $("#header .nav-holder").sticky({
        topSpacing: 0
    });

    // ---------- skill bar ---------- //
    try {
        $('#our-skill').appear(function() {
            jQuery('.skill-holder').each(function() {
                jQuery(this).find('.skill-bar').animate({
                    width: jQuery(this).attr('data-percent')
                }, 2500);
            });
        });
    } catch (err) {}

    // ------- autoheight function ------- //
    var setElementHeight = function() {
    var height = $(window).height();
      $('.fullscreen').css('height', (height));
    }
    $(window).on("resize", function(event) {
		event.preventDefault();
      setElementHeight();
    }).resize();

    // ---------- blog vertical slider ---------- //
    $('#blog-vertical-slider').bxSlider({
        mode: 'vertical',
        slideMargin: 5,
        minSlides: 3,
        maxSlides: 4,
        auto: true,
        autoControls: true,
        pause: 8000
    });

    // ------- testimonial Slider ------- //
    $('#testimonial-slider').owlCarousel({
        loop:true,
        items: 1,
        dots: false,
        nav:false,
        autoplay:true
   
    });

    // ------- testimonial Slider 2------- //
    $('#test-2-slider').owlCarousel({
        loop:true,
        items: 1,
        dots: false,
        autoplay:true
    });
    // ------- project Slider ------- //
    $('#project-slider').owlCarousel({
        loop:true,
        items: 1,
        dots: true,
        nav:true,
        autoplay:true
    });

    // ------- team Slider ------- //
    $('#team-slider').owlCarousel({
        loop:true,
        items: 2,
        dots: false,
        nav:true,
        responsiveClass: true,
        responsive: {
            0: {
                items: 1
            },
            320: {
                items: 1
            },
            480: {
                items: 2
            },
        }

    })

    // ------- Counter ------- //
    try {
        $('#cr-counters').appear(function () {
            $('.cr-timer').countTo()
        });
    } catch (err) {}    
   

    // ------- comming soon countdown ------- //
        var clock;

        clock = $('.clock').FlipClock({
            clockFace: 'DailyCounter',
            autoStart: false,
            callbacks: {
                stop: function() {
                    $('.message').html('The clock has stopped!')
                }
            }
        });
                
        clock.setTime(220880);
        clock.setCountdown(true);
        clock.start();


    // ---------- Wow Animation ---------- //
    new WOW().init();

    // ------- Prety Photo ------- //
    $("a[data-rel]").each(function () {
        $(this).attr("rel", $(this).data("rel"));
    });
    $("a[data-rel^='prettyPhoto']").prettyPhoto({
        animation_speed: 'normal',
        theme: 'dark_square',
        slideshow: 3000,
        autoplay_slideshow: false,
        social_tools: false
    });

    /*radio holder*/
    $('.radio-box li input:radio').change(function(){
        $('label.highlight').removeClass('highlight');
        $(this).closest('label').addClass('highlight');
    });
    /*radio holder*/
      
    $('#search').on("click", function (event) {
        $('.search').toggleClass('expanded');
    });


    // ------- Vertical slider ------- //
    $("#vertical-text-slider").owlCarousel({
        loop: true,
        autoplay: true,
        items: 1,
        nav: true,
        animateOut: 'slideOutUp',
        animateIn: 'slideInUp'
    });
    // ------- Vertical slider ------- //

    // ------- Sticky Smooth Scroll ------- //
    $('a[rel="smooth-top"]').on("click", function (event){
        $('html, body').animate({
            scrollTop: $( $.attr(this, 'href') ).offset().top
        }, 1000);
        return false;
    }); 


    // ------- PrettyPhoto Video Popup ------- //
    $("a[rel^='prettyPhoto']").prettyPhoto();

    // ------- Mesonary ------- //
    var $container = $('.project-masonary');
    var $optionSets = $('.option-set');
    var $optionLinks = $optionSets.find('a');
    function doIsotopeFilter() {
        if ($().isotope) {
            var isotopeFilter = '';
            $optionLinks.each(function () {
                var selector = $(this).attr('data-filter');
                var link = window.location.href;
                var firstIndex = link.indexOf('filter=');
                if (firstIndex > 0) {
                    var id = link.substring(firstIndex + 7, link.length);
                    if ('.' + id == selector) {
                        isotopeFilter = '.' + id;
                    }
                }
            });
            $container.isotope({
                itemSelector: '.masonry-grid',
                filter: isotopeFilter
            });
            $optionLinks.each(function () {
                var $this = $(this);
                var selector = $this.attr('data-filter');
                if (selector == isotopeFilter) {
                    if (!$this.hasClass('selected')) {
                        var $optionSet = $this.parents('.option-set');
                        $optionSet.find('.selected').removeClass('selected');
                        $this.addClass('selected');
                    }
                }
            });
            $optionLinks.on('click', function (event) {
				event.preventDefault();
                var $this = $(this);
                var selector = $this.attr('data-filter');
                $container.isotope({itemSelector: '.masonry-grid', filter: selector});
                if (!$this.hasClass('selected')) {
                    var $optionSet = $this.parents('.option-set');
                    $optionSet.find('.selected').removeClass('selected');
                    $this.addClass('selected');
                }
                return false;
            });
        }
    }
    var isotopeTimer = window.setTimeout(function () {
        window.clearTimeout(isotopeTimer);
        doIsotopeFilter();
    }, 1000);
    var selected = $('#cr-filterbale-nav > li > a');
    var $this = $(this);
    selected.on('click', function (event) {
		event.preventDefault();
        if (selected.hasClass('selected')) {
            $(this).parent().addClass('current-menu-item').siblings().removeClass('current-menu-item');
        }
    });
    // ------- Mesonary ------- // 

});
